# STYLER (Web App)

## Link
https://styler.link/

## Overview
STYLERは"つながり"でファッションを楽しくするサービスです。あなたの生活に新しい価値を提供します。

## Requirement
Ruby 2.2.2p95  
Rails 4.2.3

## Wiki
https://github.com/STYLER-Inc/styler/wiki

## Licence
STYLER株式会社

## Author
[Yuji Goto](https://github.com/yuji1129xxx)
[Kojima Kazuto](https://github.com/kojimakazuto)
[Yi Gu](https://github.com/ghawkgu)
[Naoki Ioroi](https://github.com/naoki1061)
[Shota Toguchi](https://github.com/Dooor) 
