# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161101113723) do

  create_table "activities", force: :cascade do |t|
    t.string "category", limit: 255
    t.string "name",     limit: 255
  end

  create_table "app_sessions", force: :cascade do |t|
    t.integer  "user_id",      limit: 4
    t.string   "token",        limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.date     "expired_date"
    t.string   "version",      limit: 255
    t.string   "device_token", limit: 255
  end

  create_table "article_tags", force: :cascade do |t|
    t.integer  "article_id", limit: 4
    t.integer  "tag_id",     limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "articles", force: :cascade do |t|
    t.integer  "wp_post_id",  limit: 4
    t.integer  "category_id", limit: 4
    t.integer  "author_id",   limit: 4
    t.text     "title",       limit: 65535
    t.text     "description", limit: 65535
    t.text     "keywords",    limit: 65535
    t.text     "content",     limit: 65535
    t.text     "thumbnail",   limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "wp_url",      limit: 65535
  end

  create_table "authors", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "profile",    limit: 65535
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "child_name", limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "type",       limit: 255
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.text     "text",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "post_id",    limit: 4
    t.integer  "item_id",    limit: 4
  end

  create_table "conversation_users", force: :cascade do |t|
    t.integer  "conversation_id", limit: 4
    t.integer  "user_id",         limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "conversations", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "newest_time"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "developers", force: :cascade do |t|
    t.string   "email",               limit: 255, default: "", null: false
    t.string   "encrypted_password",  limit: 255, default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",  limit: 255
    t.string   "last_sign_in_ip",     limit: 255
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "developers", ["email"], name: "index_developers_on_email", unique: true, using: :btree

  create_table "events", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.integer  "shop_id",     limit: 4
    t.text     "title",       limit: 65535
    t.text     "text",        limit: 65535
    t.date     "start_date"
    t.date     "finish_date"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "location",    limit: 255
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "reply_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reply_id",   limit: 4
    t.integer  "rate",       limit: 4
  end

  create_table "follows", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "staff_id",   limit: 4
  end

  create_table "images", force: :cascade do |t|
    t.string   "type",               limit: 255
    t.integer  "generic_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 8
    t.datetime "photo_updated_at"
    t.string   "random_string",      limit: 255
  end

  create_table "interests", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "event_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "items", force: :cascade do |t|
    t.integer  "shop_id",     limit: 4
    t.string   "brand",       limit: 255
    t.string   "name",        limit: 255
    t.integer  "price",       limit: 8
    t.text     "text",        limit: 65535
    t.boolean  "complete",                  default: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "category_id", limit: 4,     default: 1
    t.integer  "likes_count", limit: 4,     default: 0,     null: false
  end

  create_table "kpis", force: :cascade do |t|
    t.integer  "total_customers", limit: 8, default: 0
    t.integer  "total_shops",     limit: 8, default: 0
    t.integer  "total_replies",   limit: 8, default: 0
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "total_users",     limit: 8, default: 0
    t.integer  "total_staffs",    limit: 8, default: 0
    t.integer  "total_posts",     limit: 8, default: 0
    t.integer  "total_watches",   limit: 8, default: 0
    t.integer  "total_events",    limit: 8, default: 0
    t.integer  "total_matches",   limit: 8, default: 0
  end

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "item_id",    limit: 4
  end

  create_table "logs", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.datetime "created_at"
    t.integer  "activity_id", limit: 4
    t.integer  "platform_id", limit: 4
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "conversation_id", limit: 4
    t.text     "text",            limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "confirmed",                     default: false
    t.string   "type",            limit: 255
    t.text     "objects",         limit: 65535
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "type",       limit: 255
    t.integer  "generic_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status",     limit: 4,   default: 0
    t.boolean  "send_mail",              default: false
  end

  create_table "platforms", force: :cascade do |t|
    t.string "category", limit: 255
    t.string "name",     limit: 255
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.text     "text",        limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "newest_time"
    t.integer  "category_id", limit: 4,     default: 1
  end

  create_table "push_notifications", id: false, force: :cascade do |t|
    t.string  "identification_id", limit: 255
    t.integer "user_id",           limit: 4
    t.string  "message",           limit: 512
    t.integer "reservation_time",  limit: 4
    t.integer "registered_at",     limit: 4
    t.integer "page_id",           limit: 4,   default: 0, null: false
    t.integer "push_type",         limit: 4,               null: false
  end

  create_table "replies", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "post_id",    limit: 4
    t.text     "text",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "shop_id",    limit: 4
    t.string   "item_brand", limit: 255
    t.string   "item_name",  limit: 255
    t.string   "item_price", limit: 255
    t.text     "comment",    limit: 65535
    t.integer  "item_id",    limit: 4
    t.boolean  "copied",                   default: false
  end

  create_table "reports", force: :cascade do |t|
    t.text     "text",       limit: 65535
    t.integer  "user_id",    limit: 4
    t.boolean  "complete",                 default: false
    t.string   "object",     limit: 255
    t.integer  "generic_id", limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "shop_registrations", force: :cascade do |t|
    t.string   "shop_name",      limit: 255
    t.text     "shop_profile",   limit: 65535
    t.text     "shop_website",   limit: 65535
    t.string   "staff_name",     limit: 255
    t.string   "staff_email",    limit: 255
    t.string   "staff_password", limit: 255
    t.boolean  "confirmed",                    default: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "shop_users", force: :cascade do |t|
    t.integer  "shop_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.text     "profile",             limit: 65535
    t.text     "address",             limit: 65535
    t.string   "tel",                 limit: 255
    t.text     "website",             limit: 65535
    t.string   "business_hours",      limit: 255
    t.string   "authenticate_params", limit: 255
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.boolean  "closed",                            default: false
    t.datetime "newest_time"
    t.integer  "location",            limit: 4,     default: 0
    t.string   "station",             limit: 255
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "",    null: false
    t.string   "encrypted_password",     limit: 255,   default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   limit: 255
    t.string   "uid",                    limit: 255,   default: "",    null: false
    t.string   "provider",               limit: 255,   default: "",    null: false
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.date     "last_login_date"
    t.integer  "login_count",            limit: 4,     default: 0
    t.boolean  "admin_flag",                           default: false
    t.boolean  "mail_notification",                    default: true,  null: false
    t.boolean  "mail_magazine",                        default: true,  null: false
    t.text     "profile",                limit: 65535
    t.string   "location",               limit: 255
    t.date     "birthday"
    t.integer  "gender",                 limit: 4
    t.text     "education",              limit: 65535
    t.text     "work",                   limit: 65535
    t.integer  "status",                 limit: 4,     default: 0,     null: false
    t.boolean  "push_notification",                    default: false, null: false
    t.text     "fb_details",             limit: 65535
    t.integer  "likes_count",            limit: 4,     default: 0,     null: false
    t.integer  "interests_count",        limit: 4,     default: 0,     null: false
    t.integer  "posts_count",            limit: 4,     default: 0,     null: false
    t.integer  "watches_count",          limit: 4,     default: 0,     null: false
    t.integer  "follows_count",          limit: 4,     default: 0,     null: false
    t.integer  "replies_count",          limit: 4,     default: 0,     null: false
    t.integer  "followers_count",        limit: 4,     default: 0,     null: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree

  create_table "watches", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "post_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

end
