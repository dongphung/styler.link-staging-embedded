class AddNewestTimeToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :newest_time, :datetime
  end
end
