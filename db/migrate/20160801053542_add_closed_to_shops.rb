class AddClosedToShops < ActiveRecord::Migration
  def change
    add_column :shops, :closed, :boolean, :default => false
  end
end
