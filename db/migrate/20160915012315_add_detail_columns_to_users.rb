class AddDetailColumnsToUsers < ActiveRecord::Migration
  def up
    User.find_each do |user|
      if user[:gender] == 'male'
        user.update_columns(gender: 0)
      elsif user[:gender] == 'female'
        user.update_columns(gender: 1)
      else
        user.update_columns(gender: nil)
      end
    end

    add_column :users, :push_notification, :boolean, default: true, null: false
    add_column :users, :fb_details, :text
    remove_column :users, :height
    remove_column :users, :weight
    change_column :users, :gender, :integer
  end

  def down
    remove_column :users, :push_notification
    remove_column :users, :fb_details
    add_column :users, :height, :integer
    add_column :users, :weight, :integer
    change_column :users, :gender, :string
  end
end
