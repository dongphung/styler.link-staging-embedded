class AddSendMailToNotification < ActiveRecord::Migration
  def up
    add_column :notifications, :send_mail, :boolean, default: false

    Notification.all.update_all(send_mail: true)
  end

  def down
    remove_column :notifications, :send_mail
  end
end
