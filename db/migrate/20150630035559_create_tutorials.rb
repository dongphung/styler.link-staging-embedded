class CreateTutorials < ActiveRecord::Migration
  def change
    create_table :tutorials do |t|
      t.integer        :user_id
      t.integer        :tutorial_number
      t.timestamps null: false
    end
  end
end
