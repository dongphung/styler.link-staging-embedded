class AddCountsToUsers < ActiveRecord::Migration
  def up
    add_column :users, :likes_count, :integer, null: false, default: 0
    add_column :users, :interests_count, :integer, null: false, default: 0
    add_column :users, :posts_count, :integer, null: false, default: 0
    add_column :users, :watches_count, :integer, null: false, default: 0
    add_column :users, :follows_count, :integer, null: false, default: 0
    add_column :users, :replies_count, :integer, null: false, default: 0
    add_column :users, :followers_count, :integer, null: false, default: 0

    Like.skip_callback(:create, :after, :create_notification)
    Like.skip_callback(:create, :after, :create_push_notification)
    Favorite.all.each do |favorite|
      item_id = favorite.reply.item_id
      Like.where(item_id: item_id, user_id: favorite.user_id).first_or_create if item_id
    end
    Like.set_callback(:create, :after, :create_notification)
    Like.set_callback(:create, :after, :create_push_notification)

    Like.counter_culture_fix_counts
    Interest.counter_culture_fix_counts
    Post.counter_culture_fix_counts
    Watch.counter_culture_fix_counts
    Reply.counter_culture_fix_counts

    Follow.all.each do |follow|
      user = User.find(follow.user_id)
      user.update_columns(follows_count: user.follows_count + 1)
      staff = User.find(follow.staff_id)
      staff.update_columns(followers_count: staff.followers_count + 1)
    end
  end

  def down
    remove_column :users, :likes_count
    remove_column :users, :interests_count
    remove_column :users, :posts_count
    remove_column :users, :watches_count
    remove_column :users, :follows_count
    remove_column :users, :replies_count
    remove_column :users, :followers_count
  end
end
