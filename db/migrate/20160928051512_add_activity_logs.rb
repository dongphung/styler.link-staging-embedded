class AddActivityLogs < ActiveRecord::Migration
  def up
    create_table(:activities) do |t|
      t.string :category
      t.string :name
    end

    create_table(:platforms) do |t|
      t.string :category
      t.string :name
    end

    create_table(:logs) do |t|
      t.integer   :user_id
      t.timestamp :created_at
      t.integer   :activity_id
      t.integer   :platform_id
    end

    Activity.create(id:1,  category:"Launch",        name:"Launch app")
    Activity.create(id:10, category:"Auth",          name:"Transition to login modal")
    Activity.create(id:11, category:"Auth",          name:"Transition to login prompt")
    Activity.create(id:12, category:"Auth",          name:"Transition to registration prompt")
    Activity.create(id:13, category:"Auth",          name:"Transition to password reset email")
    Activity.create(id:14, category:"Auth",          name:"Transition to password reset")
    Activity.create(id:15, category:"Home",          name:"Transition to next popular posts")
    Activity.create(id:16, category:"Home",          name:"Transition to next popular posts")
    Activity.create(id:17, category:"Home",          name:"Transition to new posts")
    Activity.create(id:18, category:"Home",          name:"Transition to next new posts")
    Activity.create(id:19, category:"Home",          name:"Transition to popular items")
    Activity.create(id:20, category:"Home",          name:"Transition to popular items")
    Activity.create(id:21, category:"Home",          name:"Transition to shops")
    Activity.create(id:22, category:"Home",          name:"Transition to events")
    Activity.create(id:23, category:"Home",          name:"Transition to next new events")
    Activity.create(id:24, category:"Post",          name:"Transition to post")
    Activity.create(id:25, category:"Post",          name:"Transition to post comment")
    Activity.create(id:26, category:"Reply",         name:"Transition to reply")
    Activity.create(id:27, category:"Item",          name:"Transition to item")
    Activity.create(id:28, category:"Message",       name:"Transition to :message_id")
    Activity.create(id:29, category:"Shop",          name:"Transition to new shop list by location")
    Activity.create(id:30, category:"Shop",          name:"Transition to a-z shop list by location")
    Activity.create(id:31, category:"Event",         name:"Transition to ongoing events")
    Activity.create(id:32, category:"Event",         name:"Transition to :event_id")
    Activity.create(id:33, category:"Event",         name:"Transition to going list")
    Activity.create(id:34, category:"Article",       name:"Transition to new articles")
    Activity.create(id:35, category:"Article",       name:"Transition to keywords list")
    Activity.create(id:36, category:"Article",       name:"Transition to articles by keyword")
    Activity.create(id:37, category:"Article",       name:"Transition to categories list")
    Activity.create(id:38, category:"Article",       name:"Transition to :artice_id")
    Activity.create(id:39, category:"Post Edit",     name:"Transition to post new")
    Activity.create(id:40, category:"Post Edit",     name:"Transitino to post edit")
    Activity.create(id:41, category:"Create",        name:"Transition to reply/event")
    Activity.create(id:42, category:"Reply Edit",    name:"Transition to category selection")
    Activity.create(id:43, category:"Reply Edit",    name:"Transition to post list by :category_id")
    Activity.create(id:44, category:"Reply Edit",    name:"Transition to item list by :category_id")
    Activity.create(id:45, category:"Reply Edit",    name:"Transitino to Item list by all")
    Activity.create(id:46, category:"Reply Edit",    name:"Transition to reply new")
    Activity.create(id:47, category:"Reply Edit",    name:"Transition to reply edit")
    Activity.create(id:48, category:"Item Edit",     name:"Transition to item edit")
    Activity.create(id:49, category:"Notification",  name:"Transition to acitivities")
    Activity.create(id:50, category:"Notification",  name:"Transition to messages")
    Activity.create(id:51, category:"My Page",       name:"Transition to posts")
    Activity.create(id:52, category:"My Page",       name:"Transition to watches")
    Activity.create(id:53, category:"My Page",       name:"Transition to likes")
    Activity.create(id:54, category:"My Page",       name:"Transition to going")
    Activity.create(id:55, category:"My Page",       name:"Transition to following")
    Activity.create(id:56, category:"My Page",       name:"Transition to replies")
    Activity.create(id:57, category:"My Page",       name:"Transition to followers")
    Activity.create(id:58, category:"My Page",       name:"Transition to account settings")
    Activity.create(id:59, category:"My Page",       name:"Transition to profile setting")
    Activity.create(id:60, category:"My Page",       name:"Transition to login setting")
    Activity.create(id:61, category:"My Page",       name:"Transition to tos")
    Activity.create(id:62, category:"My Page",       name:"Transition to privacy policy")
    Activity.create(id:63, category:"My Page",       name:"Transition to help")
    Activity.create(id:64, category:"My Page",       name:"Transition to company info")
    Activity.create(id:65, category:"My Page",       name:"Transition to license")
    Activity.create(id:66, category:"Customer Page", name:"Transition to posts")
    Activity.create(id:67, category:"Customer Page", name:"Transition to watches")
    Activity.create(id:68, category:"Customer Page", name:"Transition to likes")
    Activity.create(id:69, category:"Customer Page", name:"Transition to going")
    Activity.create(id:70, category:"Customer Page", name:"Transition to following")
    Activity.create(id:71, category:"Staff Page",    name:"Transition to replies")
    Activity.create(id:72, category:"Staff Page",    name:"Transition to likes")
    Activity.create(id:73, category:"Staff Page",    name:"Transition to going")
    Activity.create(id:74, category:"Staff Page",    name:"Transition to followers")
    Activity.create(id:75, category:"Shop Page",     name:"Transition to items")
    Activity.create(id:76, category:"Shop Page",     name:"Transition to events")
    Activity.create(id:77, category:"Shop Page",     name:"Transition to staffs")
    Activity.create(id:78, category:"Shop Page",     name:"Transition to about")
    Activity.create(id:79, category:"Shop Page",     name:"Transition to map")
    Activity.create(id:80, category:"Shop Page",     name:"Transition to profile setting")

    Platform.create(id:11, category:"Web", name:"iOS")
    Platform.create(id:12, category:"Web", name:"Android")
    Platform.create(id:13, category:"Web", name:"Other")
    Platform.create(id:21, category:"App", name:"iOS")
    Platform.create(id:22, category:"App", name:"Android")
  end

  def down
    drop_table :logs
    drop_table :activities
    drop_table :platforms
  end
end
