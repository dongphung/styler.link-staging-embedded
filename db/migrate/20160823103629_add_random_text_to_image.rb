class AddRandomTextToImage < ActiveRecord::Migration
  def change
    add_column :images, :random_string, :string
  end
end
