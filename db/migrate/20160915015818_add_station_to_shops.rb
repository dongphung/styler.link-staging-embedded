class AddStationToShops < ActiveRecord::Migration
  def up
    add_column :shops, :station, :string
  end

  def down
    remove_column :shops, :station
  end
end
