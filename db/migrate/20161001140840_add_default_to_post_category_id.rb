class AddDefaultToPostCategoryId < ActiveRecord::Migration
  def up
    change_column :posts, :category_id, :integer, default: 1

    Post.where(category_id: nil).update_all(category_id: 1)
  end

  def down
    change_column :posts, :category_id, :integer
  end
end
