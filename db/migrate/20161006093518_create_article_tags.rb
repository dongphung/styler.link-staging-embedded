class CreateArticleTags < ActiveRecord::Migration
  def up
    create_table :article_tags do |t|
      t.integer        :article_id
      t.integer        :tag_id
      t.timestamps null: false
    end
  end

  def down
    drop_table :article_tags
  end
end
