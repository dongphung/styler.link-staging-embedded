class AddConfirmedToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :confirmed, :boolean, :default => false
    add_column :messages, :partner_id, :integer
  end
end
