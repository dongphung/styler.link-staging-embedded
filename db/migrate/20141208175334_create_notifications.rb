class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer         :user_id
      t.string          :type
      t.integer         :author_id
      t.integer         :generic_id
      t.boolean         :confirmed, :default => false
      t.boolean         :invalidated, :default => false
      t.timestamps
    end
  end
end
