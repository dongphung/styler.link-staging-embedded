class AddRatingToShops < ActiveRecord::Migration
  def change
    add_column :shops, :rating, :integer, null: false, default: 0
  end
end
