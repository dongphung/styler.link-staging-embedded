class AddColumnsToReplies < ActiveRecord::Migration
  def change
    add_column :replies, :item_brand, :string
    add_column :replies, :item_name, :string
    add_column :replies, :item_price, :string
  end
end
