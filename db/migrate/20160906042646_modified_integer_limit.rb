class ModifiedIntegerLimit < ActiveRecord::Migration
  def change
    change_column :app_sessions, :device_token, :string, limit: 255
    change_column :images, :photo_file_size, :integer, limit: 8
    change_column :items, :price, :integer, limit: 8
    change_column :kpis, :total_customers, :integer, limit: 8
    change_column :kpis, :total_shops, :integer, limit: 8
    change_column :kpis, :total_replies, :integer, limit: 8
    change_column :kpis, :total_users, :integer, limit: 8
    change_column :kpis, :total_staffs, :integer, limit: 8
    change_column :kpis, :total_posts, :integer, limit: 8
    change_column :kpis, :total_watches, :integer, limit: 8
    change_column :kpis, :total_events, :integer, limit: 8
    change_column :kpis, :total_matches, :integer, limit: 8
    change_column :push_notifications, :identification_id, :string, limit: 255
  end
end
