class CreateShopUsers < ActiveRecord::Migration
  def change
    create_table :shop_users do |t|
      t.integer :shop_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
