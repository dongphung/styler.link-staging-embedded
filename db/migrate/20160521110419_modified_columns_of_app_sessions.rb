class ModifiedColumnsOfAppSessions < ActiveRecord::Migration
  def change
    add_column :app_sessions, :expired_date, :date
    remove_column :app_sessions, :expired_at
  end
end
