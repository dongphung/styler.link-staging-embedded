class RemoveConsentFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :consent
  end
end
