class RemoveDetailsFromDetails < ActiveRecord::Migration
  def change
    remove_column :details, :age
    remove_column :details, :style1
    remove_column :details, :style2
    remove_column :details, :style3
    remove_column :details, :style4
  end
end
