class AddReplyIdToFeedback < ActiveRecord::Migration
  def change
    add_column :feedbacks, :reply_id, :integer
  end
end
