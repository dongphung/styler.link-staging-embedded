class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.integer        :user_id
      t.text           :profile
      t.integer        :height
      t.integer        :weight
      t.integer        :age
      t.integer        :style1
      t.integer        :style2
      t.integer        :style3
      t.integer        :style4
      t.timestamps
    end
  end
end
