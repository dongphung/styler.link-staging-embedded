class AddCompleteToFeedbacks < ActiveRecord::Migration
  def change
    add_column :feedbacks, :complete, :boolean, default: false
  end
end
