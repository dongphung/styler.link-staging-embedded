class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer        :shop_id
      t.string         :brand
      t.string         :name
      t.integer        :price
      t.text           :text
      t.boolean        :complete, default: false
      t.timestamps null: false
    end

    add_column :replies, :comment, :text
    add_column :replies, :item_id, :integer
    add_column :replies, :copied, :boolean, default: false
  end
end
