class RemoveTimelineFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :timeline
  end
end
