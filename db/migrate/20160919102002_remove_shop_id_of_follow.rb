class RemoveShopIdOfFollow < ActiveRecord::Migration
  def up
    remove_column :follows, :shop_id
  end

  def down
    add_column :follows, :shop_id, :integer
  end
end
