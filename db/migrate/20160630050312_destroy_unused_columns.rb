class DestroyUnusedColumns < ActiveRecord::Migration
  def change
    drop_table :details
    remove_column :kpis, :total_customer_posts
    remove_column :kpis, :total_match_count
    remove_column :kpis, :total_watchs
    remove_column :notifications, :author_id
    remove_column :posts, :type
    remove_column :users, :status
  end
end
