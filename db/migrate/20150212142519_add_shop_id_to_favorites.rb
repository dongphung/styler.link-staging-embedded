class AddShopIdToFavorites < ActiveRecord::Migration
  def change
    add_column :favorites, :shop_id, :integer
  end
end
