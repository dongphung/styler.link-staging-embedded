class ModifiedPushTypeOfPushNotifications < ActiveRecord::Migration
  def change
    remove_column :push_notifications, :push_type
    add_column :push_notifications, :push_type, :integer, null: false
  end
end
