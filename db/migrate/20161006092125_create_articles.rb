class CreateArticles < ActiveRecord::Migration
  def up
    create_table :articles do |t|
      t.integer        :wp_post_id
      t.integer        :category_id
      t.integer        :author_id
      t.text           :title
      t.text           :description
      t.text           :keywords
      t.text           :content
      t.text           :thumbnail

      t.timestamps null: false
    end
  end

  def down
    drop_table :articles
  end
end
