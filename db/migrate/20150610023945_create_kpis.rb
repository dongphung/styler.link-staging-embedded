class CreateKpis < ActiveRecord::Migration
  def change
    create_table :kpis do |t|
      t.integer :total_customers
      t.integer :total_customer_posts
      t.integer :total_shops
      t.integer :total_shop_posts
      t.integer :total_replies
      t.date :date
      t.timestamps
    end
  end
end
