class AddNewestTimeToShop < ActiveRecord::Migration
  def change
    add_column :shops, :newest_time, :datetime
    remove_column :shops, :rating
  end
end
