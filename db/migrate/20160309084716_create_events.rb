class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer        :user_id
      t.integer        :shop_id
      t.text           :title
      t.text           :text
      t.date           :start_date
      t.date           :finish_date

      t.timestamps null: false
    end
  end
end
