class ModifiedDefaultPushNotificationOfUser < ActiveRecord::Migration
  def up
    change_column :users, :push_notification, :boolean, default: false
  end

  def down
    change_column :users, :push_notification, :boolean, default: true
  end
end
