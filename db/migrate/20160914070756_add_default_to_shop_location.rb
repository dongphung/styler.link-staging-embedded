class AddDefaultToShopLocation < ActiveRecord::Migration
  def up
    change_column :shops, :location, :integer, default: 0
  end

  def down
    change_column :shops, :location, :integer
  end
end
