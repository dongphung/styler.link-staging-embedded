class AddVersionNumToAppSessions < ActiveRecord::Migration
  def change
    add_column :app_sessions, :version, :string
  end
end
