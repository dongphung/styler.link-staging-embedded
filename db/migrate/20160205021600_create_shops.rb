class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string          :name
      t.text            :profile
      t.text            :address
      t.string          :tel
      t.text            :website
      t.string          :business_hours
      t.string          :authenticate_params
      
      t.timestamps null: false
    end
  end
end
