class AddContToLike < ActiveRecord::Migration
  def up
    add_column :items, :likes_count, :integer, null: false, default: 0
  end

  def down
    remove_column :items, :likes_count
  end
end
