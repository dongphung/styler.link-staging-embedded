class AddColumnToAppSessions < ActiveRecord::Migration
  def change
    add_column :app_sessions, :expired_at, :datetime
  end
end
