class AddDefaultToItemCategoryId < ActiveRecord::Migration
  def up
    change_column :items, :category_id, :integer, default: 1

    Item.where(category_id: nil).update_all(category_id: 1)
  end

  def down
    change_column :items, :category_id, :integer
  end
end
