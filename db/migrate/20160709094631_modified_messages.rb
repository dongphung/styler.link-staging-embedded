class ModifiedMessages < ActiveRecord::Migration
  def change
    add_column :messages, :type, :string
    add_column :messages, :objects, :text
  end
end
