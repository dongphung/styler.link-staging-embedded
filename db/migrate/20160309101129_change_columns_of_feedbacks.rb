class ChangeColumnsOfFeedbacks < ActiveRecord::Migration
  def change
    add_column :feedbacks, :generic_id, :integer
    add_column :feedbacks, :object, :string
    remove_column :feedbacks, :post_id
    remove_column :feedbacks, :reply_id
  end
end
