class AddNewestTimeToConversations < ActiveRecord::Migration
  def change
    add_column :conversations, :newest_time, :datetime
  end
end
