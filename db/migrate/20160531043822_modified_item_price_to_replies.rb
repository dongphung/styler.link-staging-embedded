class ModifiedItemPriceToReplies < ActiveRecord::Migration
  def change
    remove_column :replies, :item_price
    add_column :replies, :item_price, :integer
  end
end
