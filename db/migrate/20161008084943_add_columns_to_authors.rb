class AddColumnsToAuthors < ActiveRecord::Migration
  def up
    add_column :authors, :profile, :text
  end

  def down
    remove_column :authors, :profile
  end
end
