class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :status, :integer
    add_column :users, :tutorial, :integer
  end
end
