class AddLocationToShop < ActiveRecord::Migration
  def up
    add_column :shops, :location, :integer

    Shop.where(location: nil).update_all(location: 0)
  end

  def down
    remove_column :shops, :location
  end
end
