class AddColumnsToComments < ActiveRecord::Migration
  def change
    add_column :comments, :post_id, :integer
    add_column :comments, :reply_comment_id, :integer

    remove_column :comments, :generic_id
  end
end
