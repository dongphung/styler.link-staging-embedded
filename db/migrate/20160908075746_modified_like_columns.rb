class ModifiedLikeColumns < ActiveRecord::Migration
  def up
    Like.delete_all
    LikeNotification.delete_all
    FavoriteNotification.delete_all

    remove_column :likes, :comment_id
    add_column :likes, :item_id, :integer
  end

  def down
    add_column :likes, :comment_id, :integer
    remove_column :likes, :item_id
  end
end
