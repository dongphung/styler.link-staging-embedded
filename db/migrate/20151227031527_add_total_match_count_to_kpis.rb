class AddTotalMatchCountToKpis < ActiveRecord::Migration
  def change
    add_column :kpis, :total_match_count, :integer
  end
end
