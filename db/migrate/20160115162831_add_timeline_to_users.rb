class AddTimelineToUsers < ActiveRecord::Migration
  def change
    add_column :users, :timeline, :integer, default: 0, null: false
  end
end
