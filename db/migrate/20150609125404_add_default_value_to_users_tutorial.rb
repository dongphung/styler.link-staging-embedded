class AddDefaultValueToUsersTutorial < ActiveRecord::Migration
  def change
    change_column :users, :tutorial, :integer, default: 0
  end
end
