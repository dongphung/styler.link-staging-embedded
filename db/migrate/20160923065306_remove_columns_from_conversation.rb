class RemoveColumnsFromConversation < ActiveRecord::Migration
  def up
    remove_column :conversations, :user_id
    remove_column :conversations, :reply_id
  end

  def down
    add_column :conversations, :user_id, :integer
    add_column :conversations, :reply_id, :integer
  end
end
