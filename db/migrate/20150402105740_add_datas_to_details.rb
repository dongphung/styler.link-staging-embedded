class AddDatasToDetails < ActiveRecord::Migration
  def change
    add_column :details, :location, :string
    add_column :details, :birthday, :date
    add_column :details, :neck, :integer
    add_column :details, :shoulder, :integer
    add_column :details, :arm, :integer
    add_column :details, :waist, :integer
    add_column :details, :hip, :integer
    add_column :details, :leg, :integer
  end
end
