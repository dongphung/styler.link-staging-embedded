class AddInfoToDetails < ActiveRecord::Migration
  def change
    add_column :details, :gender, :string
    add_column :details, :education, :text
    add_column :details, :work, :text
  end
end
