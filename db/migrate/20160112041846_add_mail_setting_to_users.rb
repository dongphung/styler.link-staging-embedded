class AddMailSettingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :mail_notification, :boolean, default: true, null: false
    add_column :users, :mail_magazine, :boolean, default: true, null: false
  end
end
