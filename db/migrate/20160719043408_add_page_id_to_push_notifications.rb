class AddPageIdToPushNotifications < ActiveRecord::Migration
  def change
  	add_column :push_notifications, :page_id, :integer, null: false, default: 0
  end
end
