class CreateShopRegistrations < ActiveRecord::Migration
  def change
    create_table :shop_registrations do |t|
      t.string         :shop_name
      t.text           :shop_profile
      t.text           :shop_website
      t.string         :staff_name
      t.string         :staff_email
      t.string         :staff_password
      t.boolean        :confirmed, default: false

      t.timestamps null: false
    end
  end
end
