class AddStaffIdToFollows < ActiveRecord::Migration
  def change
    add_column :follows, :staff_id, :integer
  end
end
