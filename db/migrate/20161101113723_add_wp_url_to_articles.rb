class AddWpUrlToArticles < ActiveRecord::Migration
  def up
    add_column :articles, :wp_url, :text
  end

  def down
    remove_column :articles, :wp_url
  end
end
