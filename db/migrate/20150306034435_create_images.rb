class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string          :type
      t.integer         :generic_id
      t.timestamps
    end
  end
end
