class AddDetailsToDetails < ActiveRecord::Migration
  def change
    add_column :details, :address, :text
    add_column :details, :tel, :string
    add_column :details, :site, :text
  end
end
