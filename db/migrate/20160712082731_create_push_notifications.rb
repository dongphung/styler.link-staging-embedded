class CreatePushNotifications < ActiveRecord::Migration
  def change
    create_table :push_notifications, id: false, timestamps: false do |t|
      t.string         :identification_id, :limit => 128
      t.column         :push_type, :tinyint
      t.integer        :user_id
      t.string         :message, :limit => 512
      t.integer        :reservation_time
      t.integer        :registered_at
    end
  end
end
