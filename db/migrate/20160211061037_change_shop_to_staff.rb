class ChangeShopToStaff < ActiveRecord::Migration
  def change
    add_column :conversations, :reply_id, :integer
    remove_column :conversations, :partner_id

    remove_column :favorites, :shop_id
    remove_column :favorites, :post_id

    remove_column :notifications, :confirmed
    remove_column :notifications, :invalidated

    remove_column :posts, :closed

    remove_column :messages, :partner_id

    add_column :replies, :shop_id, :integer

    drop_table :tutorials
    drop_table :keywords

    add_column :users, :profile, :text
    add_column :users, :height, :integer
    add_column :users, :weight, :integer
    add_column :users, :location, :string
    add_column :users, :birthday, :date
    add_column :users, :gender, :string
    add_column :users, :education, :text
    add_column :users, :work, :text
    remove_column :users, :tutorial
  end
end
