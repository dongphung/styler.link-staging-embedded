class CreateReports < ActiveRecord::Migration
  def up
    create_table :reports do |t|
      t.text           :text
      t.integer        :user_id
      t.boolean        :complete, default: false
      t.string         :object
      t.integer        :generic_id
      t.timestamps null: false
    end
  end

  def down
    drop_table :reports
  end
end
