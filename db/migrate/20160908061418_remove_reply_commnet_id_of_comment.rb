class RemoveReplyCommnetIdOfComment < ActiveRecord::Migration
  def up
    remove_column :comments, :reply_comment_id
  end

  def down
    add_column :comments, :reply_comment_id, :integer
  end
end
