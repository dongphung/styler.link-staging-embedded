class ModifiedFeedbacks < ActiveRecord::Migration
  def up
    Feedback.delete_all
    remove_column :feedbacks, :complete
    remove_column :feedbacks, :generic_id
    remove_column :feedbacks, :object
    remove_column :feedbacks, :text
    add_column :feedbacks, :reply_id, :integer
    add_column :feedbacks, :rate, :integer
  end

  def down
    remove_column :feedbacks, :reply_id
    remove_column :feedbacks, :rate
    add_column :feedbacks, :complete, :boolean, default: false
    add_column :feedbacks, :generic_id, :integer
    add_column :feedbacks, :object, :string
    add_column :feedbacks, :text, :text
  end
end
