class DropTaskModel < ActiveRecord::Migration
  def up
    drop_table :tasks
  end

  def down
    create_table :tasks do |t|
      t.integer        :user_id
      t.integer        :shop_id
      t.integer        :post_id
      t.timestamps null: false
    end
  end
end
