class AddDeviceTokenToAppSessions < ActiveRecord::Migration
  def change
  	add_column :app_sessions, :device_token, :string, :limit => 256
  end
end
