class AddColumnsToKpis < ActiveRecord::Migration
  def change
    add_column :kpis, :total_users, :integer, default: 0
    add_column :kpis, :total_staffs, :integer, default: 0
    add_column :kpis, :total_posts, :integer, default: 0
    add_column :kpis, :total_watches, :integer, default: 0
    add_column :kpis, :total_events, :integer, default: 0
    add_column :kpis, :total_matches, :integer, default: 0

    remove_column :kpis, :total_shop_posts

    change_column :kpis, :total_customers, :integer, default: 0
    change_column :kpis, :total_shops, :integer, default: 0
    change_column :kpis, :total_replies, :integer, default: 0
  end
end
