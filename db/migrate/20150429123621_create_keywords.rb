class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.string          :word
      t.integer         :category
      t.integer         :generic_id
      t.timestamps
    end
  end
end
