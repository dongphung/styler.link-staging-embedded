# coding: utf-8

CSV.foreach('db/seeds/categories.csv') do |row|
  Category.create(
    id: row[0].to_i,
    name: row[1].to_s,
    child_name: row[2].to_s,
    type: row[3].to_s
  )
end
