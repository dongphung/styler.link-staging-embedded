module ApplicationHelper
  def get_json(location, limit=10)
    raise ArgumentError, 'too many HTTP redirects' if limit == 0
    uri = URI.parse(location)
    begin
      response = Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
        http.open_timeout = 5
        http.read_timeout = 10
        http.get(uri.request_uri)
      end
      case response
      when Net::HTTPSuccess
        json = response.body
        JSON.parse(json)
      when Net::HTTPRedirection
        location = response['location']
        warn "redirected to #{location}"
        get_json(location, limit - 1)
      else
        puts [uri.to_s, response.value].join(" : ")
        nil
      end
    rescue => e
      puts [uri.to_s, e.class, e].join(" : ")
      nil
    end
  end

  def check_route(controller_name, action_name)
    if controller_name && action_name
      controller.controller_name == controller_name && controller.action_name == action_name
    elsif controller_name
      controller.controller_name == controller_name
    elsif action_name
      controller.action_name == action_name
    end
  end

  def confirmed?(notification)
    'confirmed' if notification.confirmed?
  end

  def role(object=nil)
    if current_user && current_user.id == object.try(:user_id)
      'mypost'
    elsif current_user.try(:customer?)
      'customer'
    elsif current_user.try(:staff?)
      'staff'
    else
      'guest'
    end
  end

  # 2回以上連続した改行文字と、普通の改行文字を区別して変換
  def change_br(str)
    str.present? ? "<p>#{h(str).gsub(/(\r\n?){2,}|(\n){2,}/, "</p></br><p>").gsub(/(\r\n?)|(\n)/, "</p><p>")}</p>".html_safe : ''
  end

  # 文字列の改行を削除
  def delete_br(str)
    str.present? ? str.gsub(/(\s)/,'') : ''
  end

  def object_image(object, size='medium')
    cloudfront_url = YAML.load_file("#{Rails.root}/config/cloudfront.yml")[Rails.env]['url']
    image = object.image
    if image
      extension = image.photo_content_type.gsub('image/','')
      "#{cloudfront_url}/photos/#{image.id}/#{size}_#{image.random_string}.#{extension}"
    else
      'user_default.png'
    end
  end

  def user_age(birthday)
    jdate(Date.today).to_i - jdate(birthday).to_i if birthday.present?
  end

  def login_rate(user)
    if user.last_login_date
      login_days = user.last_login_date - Date.parse(user.created_at.to_s) + 1
      (user.login_count.fdiv(login_days.to_i)*100).to_i
    else
      0
    end
  end

  def gender(gender)
    if gender == 'male'
      '男性'
    elsif gender == 'female'
      '女性'
    end
  end

  # for userAgent check
  def mobile_agent?
    request.user_agent =~ /Mobile|webOS/
  end

  # for json
  def jtime(time)
    time.try(:strftime, '%Y-%m-%d %H:%M:%S')
  end

  # for json
  def jdate(date)
    date.try(:strftime, '%Y-%m-%d')
  end

  # for json
  def junix(time)
    Time.parse(time.to_s).to_i if time.present?
  end
end
