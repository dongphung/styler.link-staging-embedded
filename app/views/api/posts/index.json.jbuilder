json.last_object_id     @posts.last.try(:id)
json.set! :posts do
  json.array! @posts do |post|
    json.partial! 'api/common/post', post: post
    json.set! :user do
      json.partial! 'api/common/user', user: post.user
    end
    json.set! :items do
      json.array! post.reply_items.limit(4) do |item|
        json.partial! 'api/common/item', item: item
      end
    end
  end
end
