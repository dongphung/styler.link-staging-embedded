json.set! :item do
  item = @item
  json.partial! 'api/common/item', item: item
  json.set! :shop do
    json.partial! 'api/common/shop', shop: item.shop
  end
  json.set! :replies do
    replies = item.replies
    json.array! replies do |reply|
      json.partial! 'api/common/reply', reply: reply
      json.set! :user do
        json.partial! 'api/common/user', user: reply.user
      end
      json.set! :shop do
        json.partial! 'api/common/shop', shop: reply.item.shop
      end
    end
  end
end
