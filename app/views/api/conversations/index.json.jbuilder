json.last_object_id     @conversations.last.try(:id)
json.set! :conversations do
  json.array! @conversations do |conversation|
    json.partial! 'api/common/conversation', conversation: conversation
    json.set! :messages do
      json.array! conversation.messages.limit(10).recent do |message|
        json.partial! 'api/common/message', message: message
      end
    end
  end
end
