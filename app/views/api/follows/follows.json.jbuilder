json.last_object_id   @follows.last.try(:id)
json.set! :follows do
  json.array! @follows do |follow|
    json.partial! 'api/common/follow', follow: follow
    json.set! :staff do
      json.partial! 'api/common/user', user: follow.staff
    end
    json.set! :shop do
      json.partial! 'api/common/shop', shop: follow.staff.shop
    end
  end
end
