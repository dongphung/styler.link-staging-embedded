json.last_object_id   @followers.last.try(:id)
json.set! :followers do
  json.array! @followers do |follower|
    json.partial! 'api/common/follow', follow: follower
    json.set! :user do
      json.partial! 'api/common/user', user: follower.user
    end
  end
end
