json.id                       conversation.id
json.created_at               jtime(conversation.created_at)
json.updated_at               jtime(conversation.updated_at)
json.newest_time              jtime(conversation.newest_time)
json.newest_text              conversation.newest_text
destination = conversation.destination(@current_user.id)
json.destination_id           destination.id
json.destination_name         destination.name
json.destination_image_url    destination.image.try(:url)
json.unread_flag              conversation.has_unread_message?(@current_user.id)
