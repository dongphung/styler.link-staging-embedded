json.id            category.id
json.name          category.name
json.child_name    category.child_name.presence
json.type          category.type
