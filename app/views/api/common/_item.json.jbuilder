json.id             item.id
json.shop_id        item.shop_id
json.brand          item.brand
json.name           item.name
json.price          item.price
json.text           item.text
json.category_id    item.category_id
json.created_at     jtime(item.created_at)
json.updated_at     jtime(item.updated_at)
json.likes_count    item.likes_count
json.like_id        item.json_like_id(@current_user)
json.set! :images_url do
  images_url = item.images.map{|image| image.try(:url, :normal)}
  json.image_url_1    images_url[0]
  json.image_url_2    images_url[1]
  json.image_url_3    images_url[2]
  json.image_url_4    images_url[3]
end
