json.id             follow.id
json.user_id        follow.user_id
json.staff_id       follow.staff_id
json.created_at     jtime(follow.created_at)
json.updated_at     jtime(follow.updated_at)
