json.id             watch.id
json.user_id        watch.user_id
json.post_id        watch.post_id
json.created_at     jtime(watch.created_at)
json.updated_at     jtime(watch.updated_at)
