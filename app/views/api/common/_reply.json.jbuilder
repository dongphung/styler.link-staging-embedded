json.id                 reply.id
json.post_id            reply.post_id
json.comment            reply.comment
json.created_at         jtime(reply.created_at)
json.updated_at         jtime(reply.updated_at)
json.conversation_id    reply.json_conversation_id(@current_user)
json.feedback_flag      reply.json_feedback_flag(@current_user)
