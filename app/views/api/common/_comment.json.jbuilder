json.id            comment.id
json.post_id       comment.post_id
json.text          comment.text.presence
json.item_id       comment.item_id.presence
json.created_at    jtime(comment.created_at)
json.updated_at    jtime(comment.updated_at)
