json.set! :shop do
  json.partial! 'api/common/shop', shop: @shop
end
