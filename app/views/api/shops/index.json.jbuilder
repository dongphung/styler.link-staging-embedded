if @locations.present?
  json.set! :locations do
    json.array! @locations do |location|
      id = location[0].to_s.to_i
      json.id             id
      json.name           location[1]
      json.count          Shop.by_location(id).count
      json.new_shop_flag  Shop.has_new_shop?(id)
    end
  end
else
  json.last_object_id       @shops.last.try(:id)
  json.set! :shops do
    json.array! @shops do |shop|
      json.partial! 'api/common/shop', shop: shop
    end
  end
end
