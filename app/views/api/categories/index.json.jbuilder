json.set! :categories do
  json.array! @categories do |category|
    json.partial! 'api/common/category', category: category
  end
end
