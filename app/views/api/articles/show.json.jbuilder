json.set! :article do
  article = @article
  json.partial! 'api/common/article', article: article
  json.set! :tags do
    json.array! article.tags do |tag|
      json.partial! 'api/common/tag', tag: tag
    end
  end
end
