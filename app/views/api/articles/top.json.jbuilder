json.set! :features do
  json.array! @features do |article|
    json.partial! 'api/common/article', article: article
    json.set! :tags do
      json.array! article.tags do |tag|
        json.partial! 'api/common/tag', tag: tag
      end
    end
  end
end
json.set! :articles do
  json.array! @articles do |article|
    json.partial! 'api/common/article', article: article
    json.set! :tags do
      json.array! article.tags do |tag|
        json.partial! 'api/common/tag', tag: tag
      end
    end
  end
end
json.set! :tags do
  json.array! @tags do |tag|
    json.partial! 'api/common/tag', tag: tag
  end
end
