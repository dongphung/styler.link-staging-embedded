json.last_object_id       @articles.last.try(:id)
json.set! :articles do
  json.array! @articles do |article|
    json.partial! 'api/common/article', article: article
    json.set! :tags do
      json.array! article.tags do |tag|
        json.partial! 'api/common/tag', tag: tag
      end
    end
  end
end
