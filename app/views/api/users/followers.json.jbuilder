json.last_object_id       @followers.last.try(:id)
json.set! :followers do
  json.array! @followers do |user|
    json.set! :user do
      json.partial! 'api/common/user', user: user
      json.conversation_id      Conversation.search(@current_user.id, user.id).try(:id)
    end
  end
end
