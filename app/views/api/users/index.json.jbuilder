json.last_object_id   @users.last.try(:id)
json.set! :users do
  json.array! @users do |user|
    json.partial! 'api/common/user', user: user
    if user.staff?
      json.set! :shop do
        json.partial! 'api/common/shop', shop: user.shop
      end
    end
  end
end
