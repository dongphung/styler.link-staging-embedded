json.set! :kpi do
  json.user_accounts      @user_accounts
  json.staff_accounts     @staff_accounts
  json.shop_accounts      @shop_accounts
  json.active_users       @active_users
  json.active_staffs      @active_staffs
  json.posts              @posts
  json.replies            @replies
  json.messages           @messages
end

