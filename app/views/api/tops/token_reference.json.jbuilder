json.token    @token
json.set! :user do
  json.partial! 'api/common/user', user: @user
end
if @user.staff?
  json.set! :shop do
    json.partial! 'api/common/shop', shop: @user.shop
  end
end
