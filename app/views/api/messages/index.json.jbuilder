json.last_object_id            @messages.last.try(:id)
json.set! :messages do
  json.array! @messages.reverse do |message|
    json.partial! 'api/common/message', message: message
  end
end
