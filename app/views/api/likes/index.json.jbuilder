json.last_object_id     @likes.last.try(:id)
json.set! :likes do
  json.array! @likes do |like|
    json.partial! 'api/common/like', like: like
    json.set! :item do
      json.partial! 'api/common/item', item: like.item
    end
    json.set! :shop do
      json.partial! 'api/common/shop', shop: like.item.shop
    end
    json.set! :replies do
      replies = like.item.replies
      json.array! replies do |reply|
        json.partial! 'api/common/reply', reply: reply
        json.set! :user do
          json.partial! 'api/common/user', user: reply.user
        end
        json.set! :shop do
          json.partial! 'api/common/shop', shop: reply.item.shop
        end
      end
    end

  end
end
