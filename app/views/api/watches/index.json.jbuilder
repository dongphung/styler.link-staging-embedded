json.last_object_id     @watches.last.try(:id)
json.set! :watches do
  json.array! @watches do |watch|
    json.partial! 'api/common/watch', watch: watch
    json.set! :post do
      json.partial! 'api/common/post', post: watch.post
    end
    json.set! :user do
      json.partial! 'api/common/user', user: watch.post.user
    end
    json.set! :items do
      json.array! watch.post.reply_items.limit(4) do |item|
        json.partial! 'api/common/item', item: item
      end
    end
  end
end
