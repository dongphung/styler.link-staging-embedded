json.last_object_id       @tags.last.try(:id)
json.set! :tags do
  json.array! @tags do |tag|
    json.partial! 'api/common/tag', tag: tag
  end
end
