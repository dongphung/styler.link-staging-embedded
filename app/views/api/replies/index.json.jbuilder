json.last_object_id       @replies.last.try(:id)
json.set! :replies do
  json.array! @replies do |reply|
    json.partial! 'api/common/reply', reply: reply
    json.set! :item do
      json.partial! 'api/common/item', item: reply.item
    end
    json.set! :user do
      json.partial! 'api/common/user', user: reply.user
    end
    json.set! :shop do
      json.partial! 'api/common/shop', shop: reply.item.shop
    end
  end
end
