json.set! :event do
  json.partial! 'api/common/event', event: @event
  json.set! :shop do
    json.partial! 'api/common/shop', shop: @event.shop
  end
  json.set! :interest_users do
    json.array! @interest_users do |interest_user|
      json.partial! 'api/common/user', user: interest_user
    end
  end
end
