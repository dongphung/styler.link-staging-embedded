json.last_object_id       @events.last.try(:id)
json.set! :events do
  json.array! @events do |event|
    json.partial! 'api/common/event', event: event
    json.set! :shop do
      json.partial! 'api/common/shop', shop: event.shop
    end
  end
end
