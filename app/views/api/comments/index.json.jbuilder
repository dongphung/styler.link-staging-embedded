json.last_object_id       @comments.last.try(:id)
json.set! :comments do
  json.array! @comments do |comment|
    json.partial! 'api/common/comment', comment: comment
    json.set! :user do
      json.partial! 'api/common/user', user: comment.user
    end
    if comment.item
      json.set! :item do
        json.partial! 'api/common/item', item: comment.item
      end
    end
  end
end
