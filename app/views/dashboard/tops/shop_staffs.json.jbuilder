json.set! :shop_staffs do
  json.array! @shops do |shop|
    json.id     shop.id
    json.set! :staffs do
      json.array! shop.users do |staff|
        json.id                staff.id
        json.name              staff.name
        json.replies_count     staff.replies.where(created_at: @range).count
        json.image_url         staff.image.try(:url)
      end
    end
  end
end
