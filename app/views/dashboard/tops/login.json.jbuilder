json.id            @user.id
json.image_url     @user.image.try(:url)
json.shop_id       @user.shop.id
json.admin_flag    @user.admin_flag
