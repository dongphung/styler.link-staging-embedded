json.set! :overall_stats do
  json.shops              @shops
  json.posts              @posts
  json.replies            @replies
  json.messages           @messages
end
