json.set! :shop_replies do
  json.array! @shops do |shop|
    json.id     shop.id
    json.set! :replies do
      json.array! shop.replies.where(created_at: @range) do |reply|
        json.user_id            reply.user_id
        json.likes_count        reply.item.likes_count
        json.sns_share          9999
        json.item_brand         reply.item.brand
        json.item_name          reply.item.name
        json.item_price         reply.item.price
        json.item_image_url     reply.item.image.try(:url)
      end
    end
  end
end
