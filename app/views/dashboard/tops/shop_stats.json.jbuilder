json.set! :shop_stats do
  json.array! @shops do |shop|
    json.shop_id            shop.id
    json.replies_count      shop.stats_replies_count(@range)
    json.likes_count        shop.stats_likes_count(@range)
    json.messages_count     shop.stats_messages_count(@range)
    json.sns_share          9999
  end
end
