$(document).ready(function () {
  Styler.Events.init();
});

var Styler = Styler || {};
Styler.Events = {
  init : function () {
    this.customeEvents();
  },

  customeEvents : function () {
    // Sign up with email
    $('.c-form-auth__signup input[type=submit]').on('click', function() {
      ga('send', 'event', 'sign_up', 'submit', 'email');
    });

    // Sign in with email
    $('.c-form-auth__login input[type=submit]').on('click', function() {
      ga('send', 'event', 'sign_in', 'submit', 'email');
    });

    // Sign in with Facebook
    $('.c-form-auth__facebook input[type=submit]').on('click', function() {
      ga('send', 'event', 'sign_in', 'submit', 'facebook');
    });

    // Passward reset
    $('.c-form-auth__password-reset input[type=submit]').on('click', function() {
      ga('send', 'event', 'sign_in', 'submit', 'email');
    });

    // Tabs switch
    $('.p-header-tab .c-header-tab').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'tab_switch', 'click', location.href);
    });

    // Notification transition
    $('.c-icon-bell').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'notification_transition', 'click', location.href);
    });
    $('.p-article-notification-item').on('click', function() {
      ga('send', 'event', 'notification_item', 'click', 'view');
    });
    $('.confirmed .p-article-notification-item').on('click', function() {
      ga('send', 'event', 'notification_item', 'click', 'view_more_than_twice');
    });

    // Post creation
    $('.c-button__create').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'post_new_transition', 'click', location.href);
    });
    $('.c-button__post-new').on('click', function() {
      ga('send', 'event', 'post_new', 'submit', 'create');
    });
    $('.c-button__post-update').on('click', function() {
      ga('send', 'event', 'post_edit', 'submit', 'update');
    });

    // Reply creation
    $('.c-button__reply').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'reply_new_transition', 'click', location.href);
    });
    $('.c-button__reply-new').on('click', function() {
      ga('send', 'event', 'reply_new', 'submit', 'create');
    });
    $('.c-button__reply-update').on('click', function() {
      ga('send', 'event', 'reply_update', 'submit', 'update');
    });

    // Watch
    $('button.u-watch-effect').on('click', function() {
      ga('send', 'event', 'watch_add', 'submit', $(this).val());
    });
    $('button.u-watch-effect.is-active').on('click', function() {
      ga('send', 'event', 'watch_remove', 'submit', $(this).val());
    });

    // Comment
    $('.c-button__comment-new').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'comment_new', 'submit', location.href);
    });
    $('.c-button__comment-reply').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'comment_reply', 'submit', location.href);
    });

    // Conversation
    $('.c-button__conversation').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'conversation_new_transition', 'click', location.href);
    });
    $('.c-button__conversation-new').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'conversation_new', 'submit', location.href);
    });

    // Like (Reply)
    $('.p-article-cta .c-button__like').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'liek_reply_add', 'submit', location.href);
    });
    $('.p-article-cta .c-button__like.is-active').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'liek_reply_remove', 'submit', location.href);
    });

    // Like (Comment)
    $('.c-comment-contents .c-button__like').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'liek_comment_add', 'submit', location.href);
    });
    $('.c-comment-contents .c-button__like.is-active').on('click', function() {
      var location = window.location;
      ga('send', 'event', 'liek_comment_remove', 'submit', location.href);
    });
  }
}
