// initialize site-wide javascript
$(document).ready(function () {
  Styler.Frontend.init();
});

var Styler = Styler || {};
Styler.Frontend = {
  init : function () {
    this.onPageLoading();
    this.hamburgerIconAnimation();
    this.tabSwitchAnimation();
    this.watchAnimation();
    this.readMore();
    this.fileUploadPreview();
    this.preventLinkClicking();
    this.letterCountValidation();
    this.scrolledAnimation();
    this.selectWindowLocation();
    this.getDatepicker();
    this.infiniteScroll();
    this.gridArrayLayout();
  },

  _clickEvent : function () {
    return this._userAgentCheck() ? 'touchstart' : 'click';
  },

  _userAgentCheck : function () { 
    if ( navigator.userAgent.match(/Android/i)
         || navigator.userAgent.match(/webOS/i)
         || navigator.userAgent.match(/iPhone/i)
         || navigator.userAgent.match(/iPad/i)
         || navigator.userAgent.match(/iPod/i)
         || navigator.userAgent.match(/BlackBerry/i)
         || navigator.userAgent.match(/Windows Phone/i)
    ){
      return true;
    } else {
      return false;
    }
  },

  _onCssAnimaionCallback : function ( el, state, callback ) {
    var prefix = ["webkit", "moz", "MS", "o", ""];

    for (var i = 0; i < prefix.length; i++) {
      if (!prefix[i])
      state = state.toLowerCase();
      el.on(prefix[i]+state, callback);
    }
  },

  onPageLoading : function () {
    Turbolinks.enableProgressBar();
  },

  preventLinkClicking : function () {
    var $unclickable = $('a[disabled=disabled]');

    $unclickable.on(this._clickEvent(), function ( event ) {
      event.preventDefault();
    });
  },

  hamburgerIconAnimation : function () {
    var $hamburger = $('#hamburger'),
        $nav = $('#nav'),
        clickEvent = this._clickEvent();

    $hamburger.on(clickEvent, function () {
      if ($hamburger.hasClass('is-active')) {
        $hamburger.removeClass('is-active');
        $nav.removeClass('is-animated');
        if (clickEvent == 'touchstart') {
          $(window).off('.noScroll');
        }
      } else {
        $hamburger.addClass('is-active');
        $nav.addClass('is-animated');
        if (clickEvent == 'touchstart') {
          $(window).on('touchmove.noScroll', function ( event ) {
            event.preventDefault();
          });
        }
      }
      return false;
    });
    $(window).off('.noScroll');
  },

  tabSwitchAnimation: function () {
    var $headerTab = $('.c-header-tab');

    $headerTab.on(this._clickEvent(), function () {
      if ($(this).hasClass('is-current')) {
        return false;
      } else {
        $active = $(this).parent().find('.is-current');
        $active.removeClass('is-current');
        $(this).addClass('is-current');
      }
    });
  },

  watchAnimation: function () {
    var $watchButton,
        self = this;

    $('.c-icon-button.u-watch-effect').on(this._clickEvent(), function () {
      $watchButton = $(this);
      if ($watchButton.hasClass('is-active')) {
        $watchButton.removeClass('is-active');
      } else {
        $watchButton.addClass('is-animated is-active');
        self._onCssAnimaionCallback($watchButton, 'AnimationEnd', function () {
          $watchButton.removeClass('is-animated');
        });
      }
    });
  },

  readMore : function () {
    var closeHeight = '8em';
    var overflow = 'hidden';
    var moreText  = 'Read More';
    var lessText  = 'Read Less';
    var duration  = '1000';
    var easing = 'linear';

    $('.p-article-profile .c-user-name h4').each(function() {
      
      var current = $(this).children('i');
      var current_height = current.height();

      if ( current_height > 100 ) {
        current.data('fullHeight', current.height()).css({
          'height': closeHeight,
          'overflow': overflow
        });

        current.after('<a href="javascript:void(0);" class="read-more closed">' + moreText + '</a>');
      }
      

    });

    var openSlider = function() {
      link = $(this);
      var openHeight = link.prev('i').data('fullHeight') + 'px';
      link.prev('i').animate({'height': openHeight}, {duration: duration }, easing);
      link.text(lessText).addClass('open').removeClass('closed');
      link.unbind('click', openSlider);
      link.bind('click', closeSlider);
    }

    var closeSlider = function() {
      link = $(this);
      link.prev('i').animate({'height': closeHeight}, {duration: duration }, easing);
      link.text(moreText).addClass('closed').removeClass('open');
      link.unbind('click');
      link.bind('click', openSlider);
    }

    $('.read-more').bind('click', openSlider);
  },

  fileUploadPreview: function () {
    var preview = $(".c-photo-uploader img");
    
    $(".u-photo-uploader").change(function(event){
       var input = $(event.currentTarget);
       var file = input[0].files[0];
       var reader = new FileReader();
       reader.onload = function(e){
           image_base = e.target.result;
           preview.attr("src", image_base);
       };
       reader.readAsDataURL(file);
    });
  },

  letterCountValidation: function () {
    var $textarea = $('#textarea'),
        $submit = $('.c-button__submit'),
        $replyImg = $('#photo');

    $textarea.on('keydown keyup keypress change paste cut', function() {
      if ($textarea.length > 0 && $textarea.val().length == 0) {
        $submit.prop('disabled', true);
      }

      if ($replyImg.length > 0 && $replyImg.val().length > 0 && $(this).val().length > 0) {
        $submit.prop('disabled', false);
      }
      
      if ($(this).val().length > 100) {
        $submit.prop('disabled', true);
        $('#textnum').css('color','red');
      }
      else {
        $submit.prop('disabled', false);
        $('#textnum').css('color','#666666');
      }
    });
  },

  scrolledAnimation: function () {
    var scrolled;
    var lastScrollTop = 0;
    var delta = 5;

    $(window).scroll(function (event) {
        scrolled = true;
    });

    setInterval(function() {
        if (scrolled) {
          hasScrolled();
          scrolled = false;
        }
    }, 250);

    function hasScrolled () {
      var scrollNav = $(this).scrollTop();

      if(Math.abs(lastScrollTop - scrollNav) <= delta)
        return;

      if (scrollNav > lastScrollTop){
        // Scroll Down
        $('.u-scrolled-animation').addClass('is-scrolled');
      } else {
        // Scroll Up
        if(scrollNav + $(window).height() < $(document).height()) {
            $('.u-scrolled-animation').removeClass('is-scrolled');
        }
      }

      lastScrollTop = scrollNav;
    }
  },

  selectWindowLocation : function () {
    $('#page_select').on('change', function() {
        // 遷移先URL取得
        var url = $(this).val();
        // URLが取得できていればページ遷移
        if(url != '') {
            location.href = url;
        }
    })
  },

  getDatepicker : function () {
    if ( $('[type="date"]').prop('type') != 'date' ) {
      $('[type="date"]').datepicker({
        dateFormat: "yy-mm-dd"
      });
    }
  },

  infiniteScroll: function () {
    $('#home-feed, #notification-feed, #user-content, #shop-content, #shop-list').infinitescroll({
      navSelector: "#pagination",
      nextSelector: "#pagination a",
      itemSelector: '.p-article-post, .p-article-notification, .p-article-staff, .p-article-shop-list',
      loading: {
        finished: undefined,
        finishedMsg: "",
        img: "",
        msg: null,
        msgText: "<span></span><span></span><span></span>",
        selector: null,
        speed: "fast",
        start: undefined
      },
      bufferPx: 150
    });
  },

  gridArrayLayout: function () {
    $(window).on('load page:load', function () {
      var $gridItem = $('.p-article-img__reply');

      colWidth = $gridItem.outerWidth() + offsetX * 2;
      gridArray = [];

      for (var i = 0; i < numOfCol; i++) {
        pushGridArray(i, 0, 1, -offsetY);
      }
      $gridItem.each(function (index) {
        setPosition($(this));
        getContainerHeight($(this));
      });
    });

    var gridArray = [], 
        colWidth, 
        offsetX = 5, 
        offsetY = 5, 
        numOfCol = 2;

    var pushGridArray = function (x, y, size, height) {
      for (var i = 0; i < size; i++) {
        var grid = [];
        grid.x = x + i;
        grid.endY = y + height + offsetY * 2;
        gridArray.push(grid);
      }
    }

    var removeGridArray = function (x, size) {
      for (var i = 0; i < size; i++) {
        var idx = getGridIndex(x + i);
        gridArray.splice(idx, 1);
      }
    }

    var getHeightArray = function (x, size) {
      var heightArray = [];
      var temps = [];
      for (var i = 0; i < size; i++) {
        var idx = getGridIndex(x + i);
        temps.push(gridArray[idx].endY);
      }
      heightArray.min = Math.min.apply(Math, temps);
      heightArray.max = Math.max.apply(Math, temps);
      heightArray.x = temps.indexOf(heightArray.min);

      return heightArray;
    }

    var getGridIndex = function (x) {
      for (var i = 0; i < gridArray.length; i++) {
        var obj = gridArray[i];
        if (obj.x === x) {
          return i;
        }
      }
    }

    var getContainerHeight = function (grid) {
      for (var i = 0; i < grid.length; i++) {
        var container_heights = [], pos = [];
        $('.p-article-post-detail .p-article-img').find(grid[i]).each(function () {
          var tempHeight = getHeightArray(0, gridArray.length);
          pos += tempHeight.max;
        });
        container_heights[i] = pos;
      }
      max_height = Math.max.apply(Math, container_heights);
      $('.p-article-post-detail .p-article-img').css('height', max_height - offsetY *2 );
    }

    var setPosition = function (grid) {
      if (!grid.data('size') || grid.data('size') < 0) {
        grid.data('size', 1);
      }
      var pos = [];
      var tempHeight = getHeightArray(0, gridArray.length);
      pos.x = tempHeight.x;
      pos.y = tempHeight.min;
      var gridWidth = colWidth - (grid.outerWidth() - grid.width());
      grid.css({
        'display': 'block',
        'left': pos.x * colWidth,
        'top': pos.y,
        'position': 'absolute'
      });
      removeGridArray(pos.x, grid.data('size'));
      pushGridArray(pos.x, pos.y, grid.data('size'), grid.outerHeight());
    }
 
    if (!Array.prototype.indexOf) {
      Array.prototype.indexOf = function(elt /*, from*/) {
        var len = this.length >>> 0;
        var from = Number(arguments[1]) || 0;
        from = (from < 0) ? Math.ceil(from) : Math.floor(from);
        if (from < 0) {
          from += len;
        } 
        for (; from < len; from++) {
          if (from in this && this[from] === elt) {
            return from;
          }
        }
        return -1;
      };
    }
  }

}
