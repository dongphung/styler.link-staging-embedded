class StaffMailer < ApplicationMailer
  def approval_mail(registration)
    if send_mail?
      @registration = registration
      @staff = User.find_by(email: registration.staff_email)
      @shop = @staff.shop
      mail to: registration.staff_email, subject: 'ショップ登録申請承認完了のお知らせ' do |format|
        format.html { render 'approval_mail' }
        format.text { render 'approval_mail' }
      end
    end
  end

  def staff_invite_mail(staff, password=nil)
    if send_mail?
      @staff = staff
      @password = password
      mail to: staff.email, subject: 'スタッフアカウント作成のお知らせ' do |format|
        format.html { render 'staff_invite_mail' }
        format.text { render 'staff_invite_mail' }
      end
    end
  end

  def article_contact_mail(email, shop_name, links)
    if send_mail?
      @shop_name = shop_name
      @links = links.values.reject(&:blank?)
      mail to: email, subject: 'STYLER MAG 記事掲載のご連絡' do |format|
        format.text { render 'article_contact_mail' }
      end
    end
  end

  def reprint_contact_mail(email, shop_name, links)
    if send_mail?
      @shop_name = shop_name
      @links = links.values.reject(&:blank?)
      mail to: email, subject: '記事転載のご連絡' do |format|
        format.text { render 'reprint_contact_mail' }
      end
    end
  end
end
