class AdministratorMailer < ApplicationMailer
  def registration_notification(registration)
    @registration = registration
    mail to: 'info@styler.link', subject: '新規ショップ登録申請通知'
  end

  def non_image_notification(ids)
    @ids = ids
    mail to: 'info@styler.link', subject: 'Item移行時の画像チェックに関する通知メール'
  end

  def rescue_notification(error, method)
    @method = method
    @error_message = error
    mail to: 'info@styler.link', subject: '例外処理通知'
  end
end
