class ApplicationMailer < ActionMailer::Base
  # setting
  default from: '"STYLER" <info@styler.link>'
  layout 'mailer'

  # module
  helper ApplicationHelper

  # before action
  before_action :set_current_domain

  def send_mail?(user=nil)
    correct_environment? && send_mail_user?(user)
  end

  def correct_environment?
    Rails.env.development? || Rails.env.production?
  end

  def send_mail_user?(user=nil)
    if user
      if user.mail_notification
        if user.app_sessions.present?
          if user.push_notification
            false
          else
            true
          end
        else
          true
        end
      else
        false
      end
    else
      true
    end
  end

  private
  def set_current_domain
    if Rails.env.development?
      @domain = 'http://localhost:3000'
    elsif Rails.env.staging?
      @domain = 'https://staging.styler.link'
    else
      @domain = 'https://styler.link'
    end
  end
end
