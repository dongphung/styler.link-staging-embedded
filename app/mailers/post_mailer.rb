class PostMailer < ApplicationMailer
  def welcome_mail(user)
    if send_mail?(user)
      @user = user
      mail to: user.email, subject: 'STYLERへようこそ' do |format|
        format.html { render 'welcome_mail' }
        format.text { render 'welcome_mail' }
      end
    end
  end

  def comment_mail(comment, destination)
    if send_mail?(destination)
      @comment = comment
      mail to: destination.email, subject: "#{comment.user.name}さんが新しいCommentを投稿しました" do |format|
        format.html { render 'comment_mail' }
        format.text { render 'comment_mail' }
      end
    end
  end

  def feedback_mail(feedback, destination)
    if send_mail?(destination)
      @feedback = feedback
      mail to: destination.email, subject: "#{feedback.user.name}さんがあなたのReplyにフィードバックしました" do |format|
        format.html { render 'feedback_mail' }
        format.text { render 'feedback_mail' }
      end
    end
  end

  def message_mail(message, destination)
    if send_mail?(destination)
      @message = message
      mail to: destination.email, subject: "#{@message.user.name}さんからメッセージが届いています" do |format|
        format.html { render 'message_mail' }
        format.text { render 'message_mail' }
      end
    end
  end

  def reply_mail(reply, destination)
    if send_mail?(destination)
      @reply = reply
      mail to: destination.email, subject: "#{reply.user.shop.name}の#{reply.user.name}さんが新しいReplyを投稿しました" do |format|
        format.html { render 'reply_mail' }
        format.text { render 'reply_mail' }
      end
    end
  end

  def start_events_mail(event, destination)
    if send_mail?(destination)
      @event = event
      mail to: destination.email, subject: "「#{event.title}」の開始日になりました" do |format|
        format.html { render 'start_events_mail' }
        format.text { render 'start_events_mail' }
      end
    end
  end
end
