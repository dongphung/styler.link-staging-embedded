class ArticlesController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    @features = Tag.find_by(name: 'feature').articles.categorized.sample(4)
    @articles = Article.categorized.recent.limit(9)
    @snaps = Category.find_by(name: 'SNAP').articles.categorized
    @tags = Tag.weekly_tags.sample(8)
  end

  def show
    @article = Article.find_by(id: id_params[:id])
    redirect_to root_path, alert: '該当する記事が見つかりません' unless @article
  end

  def wp_show
    article = Article.find_by(wp_post_id: id_params[:id])
    if article
      redirect_to article.url, status: 301
    else
      redirect_to root_path, alert: '該当する記事が見つかりません'
    end
  end

  private
  def id_params
    params.permit(:id)
  end
end
