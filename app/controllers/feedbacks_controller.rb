class FeedbacksController < ApplicationController
  def create
    @reply = Reply.find_by(id: create_params[:reply_id])
    if @reply && current_user.customer?
      @feedback = Feedback.where(create_params).first_or_create
      render
    else
      render js: 'window.location = "/"'
    end
  end

  private
  def create_params
    params.permit(:rate, :reply_id).merge(user_id: current_user.id)
  end
end
