class TopsController < ApplicationController
  skip_before_action :authenticate_user!
  before_action :add_top_to_body, only: %i(pending)

  def privacy_policy
  end

  def use_policy
  end

  def company
  end

  def help
  end

  def pending
  end
end
