class ShopUsersController < ApplicationController
  def new
    if current_user.customer?
      if authenticate_params[:authenticate_params]
        @shop = Shop.find_by(authenticate_params: authenticate_params[:authenticate_params])
        redirect_to new_shop_user_path, alert: 'ショップが見つかりません' unless @shop
      end
    else
      redirect_to root_path, alert: 'ショップスタッフへの変更権限がありません'
    end
  end

  def create
    if current_user.customer?
      shop = Shop.find_by(authenticate_params: authenticate_params[:authenticate_params])
      if shop
        shop.users << current_user
        StaffMailer.staff_invite_mail(current_user).deliver_now
        redirect_to shop_path(shop)
      else
        redirect_to new_shop_user_path, alert: '不正なパラメーターです'
      end
    else
      redirect_to root_path, alert: 'ショップスタッフへの変更権限がありません'
    end
  end

  private
  def authenticate_params
    params.permit(:authenticate_params)
  end
end
