class ShopRegistrationsController < ApplicationController
  skip_before_action :authenticate_user!

  def new
  end

  def create
    registration = ShopRegistration.create_with_photo(registration_params, image_params)
    redirect_to new_shop_registration_path, alert: '入力内容に不備があります' unless registration.try(:persisted?)
  end

  private
  def registration_params
    params.permit(:shop_name, :shop_profile, :shop_website, :staff_name, :staff_email, :staff_password, :staff_password_confirmation)
  end

  def image_params
    params.permit(:photo)
  end
end
