class Api::NotificationsController < Api::ApiController
  def index
    Notification.browse!(@current_user.id)
    @notifications = @current_user.notifications.api_index(last_notification)
    render template: 'api/notifications/index'
  end

  private
  def last_notification
    Notification.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_params
    params.permit(:last_object_id)
  end
end
