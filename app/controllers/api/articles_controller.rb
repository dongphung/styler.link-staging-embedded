class Api::ArticlesController < Api::ApiController
  skip_before_action :check_token, only: %i(top feature index show)

  def top
    @features = Tag.find_by(name: 'feature').articles.categorized.recent.limit(4)
    @articles = Article.categorized.recent.limit(4)
    @tags = Tag.weekly_tags.sample(8)
    render template: 'api/articles/top'
  end

  def feature
    @articles = Tag.find_by(name: 'feature').articles.api_index(last_article)
    render template: 'api/articles/index'
  end

  def index
    @articles = Article.categorized.api_index(last_article)
    render template: 'api/articles/index'
  end

  def show
    @article = Article.categorized.find_by(id: id_params[:id])
    if @article
      render template: 'api/articles/show'
    else
      render json: {message: '記事が見つかりません'}, status: 404
    end
  end

  private
  def last_article
    Article.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_params
    params.permit(:last_object_id)
  end

  def id_params
    params.permit(:id)
  end
end
