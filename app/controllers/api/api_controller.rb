class Api::ApiController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  # before action
  before_action :set_request_user_detail
  before_action :basic_authentication
  before_action :check_token
  before_action :update_login_count

  # module
  include ApplicationHelper

  def set_request_user_detail
    @current_version = request.headers['X-STYLER-APP-VERSION']
    @current_session = AppSession.find_by(token: request.headers['X-STYLER-TOKEN'])
    @current_user = @current_session.try(:user)
  end

  private
  def basic_authentication
    if Rails.env.staging?
      authenticate_or_request_with_http_basic do |id, pass|
        setting = Settings.basic_authentication
        id == setting.id && pass == setting.pass
      end
    end
  end

  def check_token
    render json: {message: 'X-STYLER-TOKEN is not correct'}, status: 400 unless @current_user
  end

  def update_login_count
    today = Date.today
    if @current_user && @current_user.last_login_date != today
      @current_user.login_count += 1
      @current_user.update(last_login_date: today)
    end
  end
end
