class Api::ShopsController < Api::ApiController
  skip_before_action :check_token, except: %i(update)

  def index
    if location_params[:location]
      if order_params[:order] == 'name'
        @shops = Shop.api_index_alphabetical(last_shop).by_location(location_params[:location])
      else
        @shops = Shop.api_index(last_shop).by_location(location_params[:location])
      end
    else
      @locations = Settings.prefecture
    end
    render template: 'api/shops/index'
  end

  def show
    @shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if @shop
      render template: 'api/shops/show'
    else
      render json: {message: 'ショップが見つかりません'}, status: 404
    end
  end

  def items
    shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if shop
      if category_params[:category_id]
        category = Category.find_by(id: category_params[:category_id])
        if category
          @items = shop.items.by_category(category.id).api_index(last_item)
          render template: 'api/items/index'
        else
          render json: {message: 'カテゴリーが見つかりません'}, status: 404
        end
      else
        @items = shop.items.api_index(last_item)
        render template: 'api/items/index'
      end
    else
      render json: {message: 'ショップが見つかりません'}, status: 404
    end
  end

  def events
    shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if shop
      @events = shop.events.api_index(last_event)
      render template: 'api/events/index'
    else
      render json: {message: 'ショップが見つかりません'}, status: 404
    end
  end

  def staffs
    shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if shop
      @users = shop.users.api_index(last_user)
      render template: 'api/users/index'
    else
      render json: {message: 'ショップが見つかりません'}, status: 404
    end
  end

  def update
    @shop = Shop.find_by(id: id_params[:id])
    if @shop
      if @shop == @current_user.try(:shop)
        @shop.update(name_params) if name_params[:name].present?
        @shop.update(detail_params)
        Image.create_by_object(@shop, image_params[:photo])
        render template: 'api/shops/show'
      else
        render json: {message: 'ショップの編集権限がありませんr'}, status: 401
      end
    else
      render json: {message: 'ショップが見つかりません'}, status: 404
    end
  end

  private
  def last_shop
    Shop.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_item
    Item.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_event
    Event.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_user
    User.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_params
    params.permit(:last_object_id)
  end

  def category_params
    params.permit(:category_id)
  end

  def detail_params
    params.permit(:profile, :address, :tel, :website, :business_hours, :location, :station)
  end

  def id_params
    params.permit(:id)
  end

  def image_params
    params.permit(:photo)
  end

  def location_params
    params.permit(:location)
  end

  def name_params
    params.permit(:name)
  end

  def order_params
    params.permit(:order)
  end
end
