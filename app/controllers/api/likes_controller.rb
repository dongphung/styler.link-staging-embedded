class Api::LikesController < Api::ApiController
  def create
    item = Item.find_by(id: like_params[:item_id])
    if item
      if item.shop_id != @current_user.shop.try(:id)
        @like = Like.where(like_params).first_or_create
        Favorite.where(favorite_params).first_or_create if favorite_params[:reply_id]
        if @like.try(:persisted?)
          render template: 'api/likes/show'
        else
          render json: {message: '入力内容に不備があります'}, status: 400
        end
      else
        render json: {message: 'Likeの作成権限がありません'}, status: 401
      end
    else
      render json: {message: 'アイテムが見つかりません'}, status: 404
    end
  end

  def destroy
    like = Like.find_by(id: id_params[:id])
    if like
      if like.user_id == @current_user.id
        like.destroy
        render json: {message: 'Likeを削除しました'}, status: 200
      else
        render json: {message: 'Likeの削除権限がありません'}, status: 401
      end
    else
      render json: {message: 'Likeが見つかりません'}, status: 404
    end
  end

  private
  def favorite_params
    params.permit(:reply_id).merge(user_id: @current_user.id)
  end

  def id_params
    params.permit(:id)
  end

  def like_params
    params.permit(:item_id).merge(user_id: @current_user.id)
  end
end
