class Api::ReportsController < Api::ApiController
  def create
    report = Report.create(create_params)
    if report.try(:persisted?)
      render json: {id: report.id}, status: 201
    else
      render json: {message: '入力内容に不備があります'}, status: 400
    end
  end

  private
  def create_params
    params.permit(:text, :object, :generic_id).merge(user_id: @current_user.id)
  end
end
