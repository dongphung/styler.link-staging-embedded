class Api::UsersController < Api::ApiController
  skip_before_action :check_token, except: %i(profile setting email password)

  def show
    @user = User.find_by(id: id_params[:id])
    if @user
      render template: 'api/users/show'
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def likes
    user = User.find_by(id: id_params[:id])
    if user
      @likes = user.likes.api_index(last_like)
      render template: 'api/likes/index'
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def interests
    user = User.find_by(id: id_params[:id])
    if user
      @events = user.interest_events.api_index(last_event)
      render template: 'api/events/index'
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def posts
    user = User.find_by(id: id_params[:id])
    if user.try(:customer?)
      @posts = user.posts.api_index(last_post)
      render template: 'api/posts/index'
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def watches
    user = User.find_by(id: id_params[:id])
    if user.try(:customer?)
      @watches = user.watches.api_index(last_watch)
      render template: 'api/watches/index'
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def follows
    user = User.find_by(id: id_params[:id])
    if user.try(:customer?)
      @follows = user.follows.api_index(last_follow)
      render template: 'api/follows/follows'
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def replies
    user = User.find_by(id: id_params[:id])
    if user.try(:staff?)
      @replies = user.replies.api_index(last_reply)
      render template: 'api/replies/index'
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def followers
    user = User.find_by(id: id_params[:id])
    if user.try(:staff?)
      @followers = user.followers.api_index(last_follow)
      render template: 'api/follows/followers'
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def profile
    @current_user.update(profile_params)
    Image.create_by_object(@current_user, image_params[:photo])
    @user = @current_user
    render template: 'api/users/show'
  end

  def setting
    @current_user.update(setting_params)
    @user = @current_user
    render template: 'api/users/show'
  end

  def email
    if @current_user.update(email_params)
      render json: {message: 'メールアドレスを更新しました'}, status: 200
    else
      render json: {message: 'メールアドレスの更新に失敗しました'}, status: 400
    end
  end

  def password
    result = @current_user.update_with_password(
        password: password_params[:password],
        password_confirmation: password_params[:password_confirmation],
        current_password: password_params[:current_password])
    if result
      render json: {message: 'パスワードを更新しました'}, status: 200
    else
      render json: {message: 'パスワードの更新に失敗しました'}, status: 400
    end
  end

  private
  def last_event
    Event.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_follow
    Follow.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_item
    Item.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_like
    Like.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_post
    Post.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_reply
    Reply.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_user
    User.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_watch
    Watch.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def email_params
    params.permit(:email)
  end

  def id_params
    params.permit(:id)
  end

  def image_params
    params.permit(:photo)
  end

  def last_params
    params.permit(:last_object_id)
  end

  def password_params
    params.permit(:password, :password_confirmation, :current_password)
  end

  def profile_params
    params.permit(:name, :location, :birthday, :gender, :profile)
  end

  def setting_params
    params.permit(:push_notification, :mail_notification, :mail_magazine)
  end
end
