class Api::EventsController < Api::ApiController
  skip_before_action :check_token, only: %i(index show)

  def index
    case type_params[:type]
    when 'future'
      @events = Event.future.api_index(last_event)
    when 'ongoing'
      @events = Event.ongoing.api_index(last_event)
    else
      @events = Event.api_index(last_event)
    end
    render template: 'api/events/index'
  end

  def future
    @events = Event.future.api_index(last_event)
    render template: 'api/events/index'
  end

  def ongoing
    @events = Event.ongoing.api_index(last_event)
    render template: 'api/events/index'
  end

  def create
    if @current_user.staff?
      @event = Event.create_with_photo(event_params, image_params)
      if @event.try(:persisted?)
        render template: 'api/events/show'
      else
        render json: {message: '入力内容に不備があります'}, status: 400
      end
    else
      render json: {message: 'イベントの作成権限がありません'}, status: 401
    end
  end

  def show
    @event = Event.find_by(id: id_params[:id])
    if @event
      @interest_users = @event.interest_users.includes(:image).recent
      render template: 'api/events/show'
    else
      render json: {message: 'イベントが見つかりません'}, status: 404
    end
  end

  def update
    @event = Event.find_by(id: id_params[:id])
    if @event
      if @event.shop_id == @current_user.shop.try(:id)
        @event.update(event_params)
        Image.create_by_object(@event, image_params[:photo])
        render template: 'api/events/show'
      else
        render json: {message: 'イベントの編集権限がありません'}, status: 401
      end
    else
      render json: {message: 'イベントが見つかりません'}, status: 404
    end
  end

  def destroy
    event = Event.find_by(id: id_params[:id])
    if event
      if event.shop_id == @current_user.shop.try(:id)
        event.destroy
        render json: {message: 'イベントを削除しました'}, status: 200
      else
        render json: {message: 'イベントの削除権限がありません'}, status: 401
      end
    else
      render json: {message: 'イベントが見つかりません'}, status: 404
    end
  end

  private
  def last_event
    Event.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_params
    params.permit(:last_object_id)
  end

  def event_params
    params.permit(:title, :text, :start_date, :finish_date, :location).merge(user_id: @current_user.id, shop_id: @current_user.shop.id)
  end

  def id_params
    params.permit(:id)
  end

  def image_params
    params.permit(:photo)
  end

  def type_params
    params.permit(:type)
  end
end
