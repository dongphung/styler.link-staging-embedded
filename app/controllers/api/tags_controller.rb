class Api::TagsController < Api::ApiController
  skip_before_action :check_token, only: %i(index)

  def index
    @tags = Tag.api_index(last_tag)
    render template: 'api/tags/index'
  end

  def articles
    tag = Tag.find_by(id: id_params[:id])
    if tag
      @articles = tag.articles.categorized.api_index(last_article)
      render template: 'api/articles/index'
    else
      render json: {message: 'タグが見つかりません'}, status: 404
    end
  end

  private
  def last_tag
    Tag.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_article
    Article.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_params
    params.permit(:last_object_id)
  end

  def id_params
    params.permit(:id)
  end
end
