class Api::CommentsController < Api::ApiController
  def create
    if Post.exists?(id: create_params[:post_id])
      comment = Comment.where(create_params).first_or_create
      if comment.try(:persisted?)
        render json: {id: comment.id}, status: 201
      else
        render json: {message: '入力内容に不備があります'}, status: 400
      end
    else
      render json: {message: 'Postが見つかりません'}, status: 404
    end
  end

  def destroy
    comment = Comment.find_by(id: destroy_params[:id])
    if comment
      if comment.user_id == @current_user.id
        comment.destroy
        render json: {message: 'コメントを削除しました'}, status: 200
      else
        render json: {message: 'コメントの削除権限がありません'}, status: 401
      end
    else
      render json: {message: 'コメントが見つかりません'}, status: 404
    end
  end

  private
  def create_params
    params.permit(:text, :item_id, :post_id).merge(user_id: @current_user.id)
  end

  def destroy_params
    params.permit(:id)
  end
end
