class Api::FeedbacksController < Api::ApiController
  def create
    if @current_user.customer?
      if Reply.exists?(id: create_params[:reply_id])
        feedback = Feedback.where(create_params).first_or_create
        if feedback.try(:persisted?)
          render json: {id: feedback.id}, status: 201
        else
          render json: {message: '入力内容に不備があります'}, status: 400
        end
      else
        render json: {message: 'Replyが見つかりません'}, status: 404
      end
    else
      render json: {message: 'フィードバックの作成権限がありません'}, status: 401
    end
  end

  private
  def create_params
    params.permit(:rate, :reply_id).merge(user_id: @current_user.id)
  end
end
