class Api::RepliesController < Api::ApiController
  skip_before_action :check_token, only: %i(show)

  def create
    if @current_user.staff?
      item = Item.find_by(id: reply_params[:item_id])
      if item
        if item.shop_id == @current_user.shop.try(:id)
          item.update(item_params)
          @reply = Reply.where(reply_params).first_or_create
          if @reply.try(:persisted?)
            render template: 'api/replies/show'
          else
            render json: {message: '入力内容に不備があります'}, status: 400
          end
        else
          render json: {message: 'Replyの作成権限がありません'}, status: 401
        end
      else
        item = Item.create_with_photo_single(item_params, images_params)
        if item
          @reply = Reply.where(reply_params.merge(item_id: item.id)).first_or_create
          if @reply.try(:persisted?)
            render template: 'api/replies/show'
          else
            render json: {message: '入力内容に不備があります'}, status: 400
          end
        else
          render json: {message: '入力内容に不備があります'}, status: 400
        end
      end
    else
      render json: {message: 'Replyの作成権限がありません'}, status: 401
    end
  end

  def show
    @reply = Reply.find_by(id: id_params[:id])
    if @reply
      render template: 'api/replies/show'
    else
      render json: {message: 'Replyが見つかりません'}, status: 404
    end
  end

  def update
    @reply = Reply.find_by(id: id_params[:id])
    if @reply
      if @reply.user_id == @current_user.id
        @reply.update(reply_params)
        @reply.item.update(item_params)
        render template: 'api/replies/show'
      else
        render json: {message: 'Replyの編集権限がありません'}, status: 401
      end
    else
      render json: {message: 'Replyが見つかりません'}, status: 404
    end
  end

  private
  def id_params
    params.permit(:id)
  end

  def images_params
    params.permit(:photo_1, :photo_2, :photo_3, :photo_4)
  end

  def item_params
    params.permit(:brand, :name, :price, :text, :category_id).merge(shop_id: @current_user.shop.id)
  end

  def reply_params
    params.permit(:post_id, :comment, :item_id).merge(user_id: @current_user.id, shop_id: @current_user.shop.id)
  end
end
