class Api::FollowsController < Api::ApiController  
  def create
    if User.exists?(id: create_params[:staff_id])
      if @current_user.customer?
        @follow = Follow.where(create_params).first_or_create
        if @follow.try(:persisted?)
          render template: 'api/follows/show'
        else
          render json: {message: '入力内容に不備があります'}, status: 400
        end
      else
        render json: {message: 'フォローの作成権限がありません'}, status: 401
      end
    else
      render json: {message: 'ユーザーが見つかりません'}, status: 404
    end
  end

  def destroy
    follow = Follow.find_by(id: destroy_params[:id])
    if follow
      if follow.user_id == @current_user.id
        follow.destroy
        render json: {message: 'フォローを削除しました'}, status: 200
      else
        render json: {message: 'フォローの削除権限がありません'}, status: 401
      end
    else
      render json: {message: 'フォローが見つかりません'}, status: 404
    end
  end

  private
  def create_params
    params.permit(:staff_id).merge(user_id: @current_user.id)
  end

  def destroy_params
    params.permit(:id)
  end
end
