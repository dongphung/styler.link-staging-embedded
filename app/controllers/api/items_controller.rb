class Api::ItemsController < Api::ApiController
  skip_before_action :check_token, only: %i(index show)

  def index
    @items = Item.api_index(last_item)
    render template: 'api/items/index'
  end

  def show
    @item = Item.find_by(id: id_params[:id])
    if @item
      render template: 'api/items/show'
    else
      render json: {message: 'アイテムが見つかりません'}, status: 404
    end
  end

  def update
    item = Item.find_by(id: id_params[:id])
    if item
      if item.shop_id == @current_user.shop.try(:id)
        item.update(item_params)
        render json: {message: 'アイテム情報を更新しました'}, status: 200
      else
        render json: {message: 'アイテムの編集権限がありません'}, status: 401
      end
    else
      render json: {message: 'アイテムが見つかりません'}, status: 404
    end
  end

  private
  def last_item
    Item.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_params
    params.permit(:last_object_id)
  end

  def id_params
    params.permit(:id)
  end

  def item_params
    params.permit(:brand, :name, :price, :text, :category_id)
  end
end
