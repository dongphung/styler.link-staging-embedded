class Api::TopsController < Api::ApiController
  skip_before_action :check_token, except: %i(check_notifications)

  # request application version
  REQUEST_VERSION = '2.1.0'

  # アプリケーションのバージョン確認
  def check_version
    if @current_version
      if dsplit(@current_version, 0) < dsplit(REQUEST_VERSION, 0)
        render json: {message: 'please update your application'}, status: 400
      elsif dsplit(@current_version, 0) == dsplit(REQUEST_VERSION, 0)
        if dsplit(@current_version, 1) < dsplit(REQUEST_VERSION, 1)
          render json: {message: 'please update your application'}, status: 400
        elsif dsplit(@current_version, 1) == dsplit(REQUEST_VERSION, 1)
          if dsplit(@current_version, 2) < dsplit(REQUEST_VERSION, 2)
            render json: {message: 'please update your application'}, status: 400
          else
            render json: {message: 'app version is OK'}, status: 200
          end
        else
          render json: {message: 'app version is OK'}, status: 200
        end
      else
        render json: {message: 'app version is OK'}, status: 200
      end
    else
      render json: {message: 'no app version'}, status: 400
    end
  end

  # ユーザー情報を取得
  def token_reference
    PushNotification.add_endpoint_to_topic(device_params[:device_token], @current_user)
    if @current_user
      @current_session.update(expired_date: Date.today + 6.month)
      @current_session.update(version: @current_version)
      @current_session.update(device_token: device_params[:device_token]) if device_params[:device_token].present?
      @token = @current_session.token
      @user = @current_user
      render template: 'api/tops/token_reference'
    else
      render json: {message: 'guest user'}, status: 200
    end
  end

  # emailとpasswordによるログイン
  def login
    @user = User.find_by(email: login_params[:email])
    if @user.try(:valid_password?, login_params[:password])
      @token = AppSession.create_session(@user).token
      render template: 'api/tops/token_reference'
    else
      render json: {message: 'login failed'}, status: 400
    end
  end

  # emailとpasswordによる新規登録
  def sign_up
    if User.find_by(email: login_params[:email]).blank?
      @user = User.create(email: login_params[:email], password: login_params[:password], uid: User.create_unique_string)
      if @user.persisted?
        @token = AppSession.create_session(@user).token
        render template: 'api/tops/token_reference'
      else
        render json: {message: 'sign up failed'}, status: 400
      end
    elsif login_params[:password].length < 8
      render json: {message: 'this password is too short'}, status: 400
    else
      render json: {message: 'this email has already been registered'}, status: 400
    end
  end

  # Facebookアカウントによるログインまたは新規登録
  def facebook
    if result = api_result(access_token_params[:access_token])
      @user = User.facebook_oauth(result)
      if @user.try(:persisted?)
        @token = AppSession.create_session(@user).token
        render template: 'api/tops/token_reference'
      else
        render json: {message: 'facebook login failed'}, status: 400
      end
    else
      render json: {message: 'facebook login failed'}, status: 400
    end
  end

  # 未確認のnotificationの有無を確認
  def check_notifications
    if @current_user.notifications.unconfirmed.present?
      render json: {unconfirmed_notifications: true}, status: 200
    else
      render json: {unconfirmed_notifications: false}, status: 200
    end
  end

  # 外部アプリケーション向けのAPI
  def kpi
    @user_accounts  = User.where(created_at: date_range).count
    @staff_accounts = ShopUser.where(created_at: date_range).count
    @shop_accounts  = Shop.where(created_at: date_range).count
    @active_users   = Post.where(created_at: date_range).select(:user_id).distinct.count
    @active_staffs  = Reply.where(created_at: date_range).select(:user_id).distinct.count
    @posts          = Post.where(created_at: date_range).count
    @replies        = Reply.where(created_at: date_range).count
    @messages       = Message.where(created_at: date_range).count
    render template: 'api/tops/kpi'
  end

  # クーポン情報を返すAPI
  def coupons
    url = 'https://styler.link/coupon_20161021_20161120.html'
    start_date = Date.parse('2016/10/21')
    end_date = Date.parse('2016/11/20')
    today = Date.today
    if start_date <= today && end_date >= today && url.present?
      render json: {url: url}, status: 200
    else
      render json: {url: nil}, status: 404
    end
  end

  private
  def api_result(access_token)
    get_json("https://graph.facebook.com/v2.2/me?access_token=#{access_token}") if access_token.present?
  end

  def dsplit(text, index)
    text.split('.')[index.to_i].to_i
  end

  def device_params
    params.permit(:device_token)
  end

  def login_params
    params.permit(:email, :password)
  end

  def access_token_params
    params.permit(:access_token)
  end

  def range_params
    params.permit(:begin, :end)
  end

  def date_range
    from = begin Date.parse(range_params[:begin]) rescue Date.yesterday end
    to = begin Date.parse(range_params[:end]) rescue Date.yesterday end
    from..to
  end
end
