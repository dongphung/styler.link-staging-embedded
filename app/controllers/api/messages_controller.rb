class Api::MessagesController < Api::ApiController
  def index
    @conversation = Conversation.search(@current_user.id, destination_params[:destination_user_id])
    if @conversation
      if @conversation.users.find_by(id: @current_user.id)
        @conversation.notifications.by_user_id(@current_user.id).update_all(status: 2)
        @messages = @conversation.messages.api_index(last_message)
        render template: 'api/messages/index'
      else
        render json: {message: 'メッセージの閲覧権限がありません'}, status: 401
      end
    else
      render json: {message: 'メッセージが見つかりません'}, status: 404
    end
  end

  def create
    message = Message.create_with_conversation(create_params[:text], @current_user.id, create_params[:destination_user_id], create_params[:reply_id])
    if message.try(:persisted?)
      render json: {id: message.id}, status: 201
    else
      render json: {message: '入力内容に不備があります'}, status: 400
    end
  end

  private
  def last_message
    Message.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def create_params
    params.permit(:text, :destination_user_id, :reply_id)
  end

  def destination_params
    params.permit(:destination_user_id)
  end

  def last_params
    params.permit(:last_object_id)
  end
end
