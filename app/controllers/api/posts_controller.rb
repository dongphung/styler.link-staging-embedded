class Api::PostsController < Api::ApiController
  skip_before_action :check_token, only: %i(index latest show replies comments)

  def index
    if type_params[:type] == 'latest'
      @posts = Post.api_index(last_post)
    else
      @posts = Post.joins(:replies).uniq.api_index_newest(last_post)
    end
    render template: 'api/posts/index'
  end

  def latest
    @posts = Post.api_index(last_post)
    render template: 'api/posts/index'
  end

  def unreplied
    if @current_user.staff?
      category = Category.find_by(id: category_params[:category_id])
      if category
        @posts = Post.where.not(id: @current_user.replies.pluck(:id)).by_category(category.id).api_index(last_post)
        render template: 'api/posts/index'
      else
        render json: {message: 'カテゴリーが見つかりません'}, status: 404
      end
    else
      render json: {message: 'Postの閲覧権限がありません'}, status: 401
    end
  end

  def create
    if @current_user.customer?
      @post = Post.where(post_params).first_or_create
      if @post.try(:persisted?)
        render template: 'api/posts/show'
      else
        render json: {message: '入力内容に不備があります'}, status: 400
      end
    else
      render json: {message: 'Postの作成権限がありません'}, status: 401
    end
  end

  def show
    @post = Post.find_by(id: id_params[:id])
    if @post
      render template: 'api/posts/show'
    else
      render json: {message: 'Postが見つかりません'}, status: 404
    end
  end

  def replies
    if post = Post.find_by(id: id_params[:id])
      @replies = post.replies.api_index(last_reply)
      render template: 'api/replies/index'
    else
      render json: {message: 'Postが見つかりません'}, status: 404
    end
  end

  def comments
    if post = Post.find_by(id: id_params[:id])
      @comments = post.comments.api_index(last_comment)
      render template: 'api/comments/index'
    else
      render json: {message: 'Postが見つかりません'}, status: 404
    end
  end

  def update
    @post = Post.find_by(id: id_params[:id])
    if @post
      if @post.user_id == @current_user.id
        @post.update(post_params)
        render template: 'api/posts/show'
      else
        render json: {message: 'Postの編集権限がありません'}, status: 401
      end
    else
      render json: {message: 'Postが見つかりません'}, status: 404
    end
  end

  def destroy
    post = Post.find_by(id: id_params[:id])
    if post
      if post.replies.blank?
        if post.user == @current_user
          post.destroy
          render json: {message: 'Postを削除しました'}, status: 200
        else
          render json: {message: 'Postの削除権限がありません'}, status: 401
        end
      else
        render json: {message: 'このPostは削除出来ません'}, status: 401
      end
    else
      render json: {message: 'Postが見つかりません'}, status: 404
    end
  end

  private
  def last_post
    Post.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_reply
    Reply.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_comment
    Comment.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_params
    params.permit(:last_object_id)
  end

  def category_params
    params.permit(:category_id)
  end

  def id_params
    params.permit(:id)
  end

  def post_params
    params.permit(:text, :category_id).merge(user_id: @current_user.id)
  end

  def type_params
    params.permit(:type)
  end
end
