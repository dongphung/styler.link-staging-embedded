class Api::WatchesController < Api::ApiController
  def create
    post = Post.find_by(id: create_params[:post_id])
    if post
      if @current_user.customer? && post.user_id != @current_user.id
        @watch = Watch.where(create_params).first_or_create
        if @watch.try(:persisted?)
          render template: 'api/watches/show'
        else
          render json: {message: '入力内容に不備があります'}, status: 400
        end
      else
        render json: {message: 'Watchの作成権限がありません'}, status: 401
      end
    else
      render json: {message: 'Postが見つかりません'}, status: 404
    end
  end

  def destroy
    watch = Watch.find_by(id: destroy_params[:id])
    if watch
      if watch.user_id == @current_user.id
        watch.destroy
        render json: {message: 'Watchを削除しました'}, status: 200
      else
        render json: {message: 'Watchの削除権限がありません'}, status: 401
      end
    else
      render json: {message: 'Watchが見つかりません'}, status: 404
    end
  end

  private
  def create_params
    params.permit(:post_id).merge(user_id: @current_user.id)
  end

  def destroy_params
    params.permit(:id)
  end
end
