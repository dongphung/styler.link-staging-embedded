class Api::ConversationsController < Api::ApiController
  def index
    ConversationNotification.by_user_id(@current_user.id).unconfirmed.update_all(status: 1)
    @conversations = @current_user.conversations.api_index(last_conversation)
    render template: 'api/conversations/index'
  end

  def messages
    @conversation = Conversation.find_by(id: id_params[:id])
    if @conversation
      if @conversation.users.find_by(id: @current_user.id)
        @conversation.notifications.by_user_id(@current_user.id).update_all(status: 2)
        @messages = @conversation.messages.api_index(last_message)
        render template: 'api/messages/index'
      else
        render json: {message: 'メッセージの閲覧権限がありません'}, status: 401
      end
    else
      render json: {message: 'メッセージが見つかりません'}, status: 404
    end
  end

  private
  def last_conversation
    Conversation.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_message
    Message.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def id_params
    params.permit(:id)
  end

  def last_params
    params.permit(:last_object_id)
  end
end
