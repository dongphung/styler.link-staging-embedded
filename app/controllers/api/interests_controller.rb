class Api::InterestsController < Api::ApiController
  def create
    if Event.exists?(id: create_params[:event_id])
      interest = Interest.where(create_params).first_or_create
      if interest.try(:persisted?)
        render json: {id: interest.id}, status: 201
      else
        render json: {message: '入力内容に不備があります'}, status: 400
      end
    else
      render json: {message: 'イベントが見つかりません'}, status: 404
    end
  end

  def destroy
    interest = Interest.find_by(id: destroy_params[:id])
    if interest
      if interest.user_id == @current_user.id
        interest.destroy
        render json: {message: 'Goingを削除しました'}, status: 200
      else
        render json: {message: 'Goingの削除権限がありません'}, status: 401
      end
    else
      render json: {message: 'Goingが見つかりません'}, status: 404
    end
  end

  private
  def create_params
    params.permit(:event_id).merge(user_id: @current_user.id)
  end

  def destroy_params
    params.permit(:id)
  end
end
