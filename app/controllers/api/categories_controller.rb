class Api::CategoriesController < Api::ApiController
  skip_before_action :check_token, only: %i(index)

  def index
    @categories = Category.all
    render template: 'api/categories/index'
  end

  def articles
    category = Category.find_by(id: id_params[:id])
    if category
      if category.child_name.blank?
        category_ids = Category.where(name: category.name).pluck(:id)
        @articles = Article.categorized.where(category_id: category_ids).api_index(last_article)
      else
        @articles = category.articles.categorized.api_index(last_article)
      end
      render template: 'api/articles/index'
    else
      render json: {message: 'カテゴリーが見つかりません'}, status: 404
    end
  end

  private
  def last_article
    Article.find(last_params[:last_object_id]) if last_params[:last_object_id].present?
  end

  def last_params
    params.permit(:last_object_id)
  end

  def id_params
    params.permit(:id)
  end
end
