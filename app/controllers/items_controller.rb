class ItemsController < ApplicationController
  skip_before_action :authenticate_user!, only: %i(index show)

  def index
    @items = Item.web_index(page_params[:page])
  end

  def new
    redirect_to root_path, alert: 'アイテムの作成権限がありません' unless current_user.staff?
  end

  def create
    if current_user.staff?
      item = Item.create_with_photo_single(item_params, images_params)
      if item.try(:persisted?)
        redirect_to item_path(item), notice: 'アイテムが作成されました'
      else
       redirect_to new_item_path, alert: '入力内容に不備があります'
      end
    else
      redirect_to root_path, alert: 'アイテムの作成権限がありません'
    end
  end

  def show
    @item = Item.find_by(id: id_params[:id])
    if @item
      @interest = @event.interests.find_by(user_id: current_user.id) if current_user
      @interest_users = @event.interest_users.includes(:image).recent
    else
      redirect_to root_path, alert: 'アイテムが見つかりません'
    end
  end

  def edit
    @item = Item.find_by(id: id_params[:id])
    if @item
      unless @item.shop_id == current_user.shop.try(:id)
        redirect_to root_path, alert: 'アイテムの編集権限がありません'
      end
    else
      redirect_to root_path, alert: 'アイテムが見つかりません'
    end
  end

  def update
    item = Item.find_by(id: id_params[:id])
    if item
      if item.shop_id == current_user.shop.try(:id)
        item.update(item_params)
        redirect_to item_path(item), notice: 'アイテム情報を更新しました'
      else
        redirect_to root_path, alert: 'アイテムの編集権限がありません'
      end
    else
      redirect_to root_path, alert: 'アイテムが見つかりません'
    end
  end

  def destroy
    item = Item.find_by(id: id_params[:id])
    if item
      if item.shop_id == current_user.shop.try(:id)
        item.destroy
        redirect_to root_path, notice: 'アイテムを削除しました'
      else
        redirect_to root_path, alert: 'アイテムの削除権限がありません'
      end
    else
      redirect_to root_path, alert: 'アイテムが見つかりません'
    end
  end

  private
  def id_params
    params.permit(:id)
  end

  def images_params
    params.permit(:photo_1, :photo_2, :photo_3, :photo_4)
  end

  def item_params
    params.permit(:brand, :name, :price, :text, :category_id).merge(user_id: current_user.id, shop_id: current_user.shop.id)
  end

  def page_params
    params.permit(:page)
  end
end
