module ViewCache
  extend ActiveSupport::Concern

  def render_query_to_string_with_cache(query, cache_duration)
    Rails.cache.fetch(query.cache_key, expires_in: cache_duration) do
      render_to_string
    end
  end

end
