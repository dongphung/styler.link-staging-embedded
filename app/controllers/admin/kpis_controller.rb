class Admin::KpisController < Admin::AdminController
  def index
    @kpis = Kpi.recent.page(page_params[:page]).per(50)
  end

  private
  def page_params
    params.permit(:page)
  end
end
