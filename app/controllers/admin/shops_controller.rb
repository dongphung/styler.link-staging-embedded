class Admin::ShopsController < Admin::AdminController
  def index
    @objects_count = Shop.count
    @shops = Shop.all.page(page_params[:page]).per(50)
  end

  def new
  end

  def create
    if shop_params[:name].present?
      shop = Shop.where(shop_params).first_or_create
      if shop.persisted?
        Image.create_by_object(shop, image_params[:photo])
        redirect_to admin_shops_path, notice: 'ショップを作成しました'
      else
        redirect_to new_admin_shop_path, alert: 'ショップを作成出来ません'
      end
    else
      redirect_to new_admin_shop_path, alert: 'ショップ名を記入してください'
    end
  end

  def update
    shop = Shop.find_by(id: id_params[:id])
    if shop
      if shop.closed
        shop.update(closed: false)
        redirect_to admin_shops_path, notice: 'ショップの非表示を解除しました'
      else
        shop.update(closed: true)
        redirect_to admin_shops_path, notice: 'ショップを非表示にしました'
      end
    else
      redirect_to admin_shops_path, alert: 'リクエストを処理出来ません'
    end
  end

  private
  def id_params
    params.permit(:id)
  end

  def image_params
    params.permit(:photo)
  end

  def page_params
    params.permit(:page)
  end

  def shop_params
    params.permit(:name, :address, :tel, :website, :profile)
  end
end
