class Admin::ReportsController < Admin::AdminController
  def index
    @reports = Report.uncompleted
  end

  def update
    report = Report.find_by(id: id_params[:id])
    if report
      report.update(complete: true)
      redirect_to admin_reports_path, notice: '報告を確認済みにしました'
    else
      redirect_to admin_reports_path, alert: '報告が見つかりません'
    end
  end

  private
  def id_params
    params.permit(:id)
  end
end
