class Admin::ContactsController < Admin::AdminController
  def index
  end

  def new
    @type = type_params[:type]
    if @type
      @shops = Shop.has_name.alphabetical
    else
      redirect_to admin_contacts_path, alert: '不正なパラメーターです'
    end
  end

  def create
    shop = Shop.find_by(id: shop_id_params[:shop_id])
    if shop && type_params[:type].present?
      case type_params[:type]
      when 'published'
        shop.send_contact(article_params)
        redirect_to admin_contacts_path, notice: '記事掲載の連絡を送信しました'
      when 'reprint'
        shop.send_reprint(article_params)
        redirect_to admin_contacts_path, notice: '記事転載の連絡を送信しました'
      end
    else
      redirect_to new_admin_contact_path, alert: '入力項目に不備があります'
    end
  end

  private
  def article_params
    params.permit(:link1, :link2, :link3, :link4, :link5)
  end

  def shop_id_params
    params.permit(:shop_id)
  end

  def type_params
    params.permit(:type)
  end
end
