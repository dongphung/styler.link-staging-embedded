class Admin::AuthorsController < Admin::AdminController
  def index
    @authors = Author.alphabetical
  end

  def edit
    @author = Author.find_by(id: id_params[:id])
    redirect_to admin_authors_path, alert: 'author not found' unless @author
  end

  def update
    author = Author.find_by(id: id_params[:id])
    if author
      author.update(detail_params)
      Image.create_by_object(author, image_params[:photo])
      redirect_to admin_authors_path, notice: 'プロフィールを更新しました'
    else
      redirect_to admin_authors_path, alert: 'ライターが見つかりません'
    end
  end

  private
  def id_params
    params.permit(:id)
  end

  def image_params
    params.permit(:photo)
  end

  def detail_params
    params.permit(:name, :profile)
  end
end
