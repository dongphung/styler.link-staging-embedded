class Admin::UsersController < Admin::AdminController
  def index
    case search_params[:status]
    when '1'
      users = User.customer
    when '2'
      users = User.staff
    else
      users = User.all
    end
    users = users.where(id: search_params[:id]) if search_params[:id].present?
    users = users.where(email: search_params[:email]) if search_params[:email].present?
    users = users.like_by_name(search_params[:name]) if search_params[:name].present?
    @status = search_params[:status]
    @id = search_params[:id]
    @email = search_params[:email]
    @name = search_params[:name]
    @objects_count = users.count
    @users = users.page(page_params[:page]).per(50)
  end

  def new
    @shops = Shop.has_name.alphabetical
  end

  def create
    if staff_params[:shop_id].present?
      shop = Shop.find_by(id: staff_params[:shop_id])
      shop.add_staffs(staff_params)
      redirect_to new_admin_user_path, notice: 'スタッフアカウントを作成しました'
    else
      redirect_to new_admin_user_path, alert: 'ショップを選択してください'
    end
  end

  private
  def page_params
    params.permit(:page)
  end

  def search_params
    params.permit(:status, :id, :email, :name)
  end

  def staff_params
    params.permit(:shop_id, :email1, :email2, :email3, :email4, :email5)
  end
end
