class Admin::HtmlMailsController < Admin::AdminController
  # before action
  before_action :add_top_to_body, except: %i(index)

  def index
  end

  def welcome_mail
    @user = User.has_name.sample
    render template: 'post_mailer/welcome_mail'
  end

  def comment_mail
    @comment = Comment.all.sample
    render template: 'post_mailer/comment_mail'
  end

  def feedback_mail
    @feedback = Feedback.all.sample
    render template: 'post_mailer/feedback_mail'
  end

  def message_mail
    @message = UserMessage.all.sample
    render template: 'post_mailer/message_mail'
  end

  def reply_mail
    @reply = Reply.all.sample
    render template: 'post_mailer/reply_mail'
  end

  def start_events_mail
    @event = Event.all.sample
    render template: 'post_mailer/start_events_mail'
  end

  def approval_mail
    @registration = ShopRegistration.new
    @staff = User.staff.has_name.sample
    @shop = @staff.shop
    @registration.staff_email = @staff.email
    @registration.staff_name = @staff.name
    @registration.shop_name = @shop.name
    render template: 'staff_mailer/approval_mail'
  end

  def staff_invite_mail
    @staff = User.staff.has_name.sample
    array = User.has_name.pluck(:uid).sample(2) << nil
    @password = array.sample
    render template: 'staff_mailer/staff_invite_mail'
  end
end
