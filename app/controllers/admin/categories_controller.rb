class Admin::CategoriesController < Admin::AdminController
  def index
    @categories = ArticleCategory.where(child_name: '')
  end

  def new
    @categories = ArticleCategory.where(child_name: '')
  end

  def create
    if category_params[:id].present? && category_params[:child_name].present?
      category = ArticleCategory.find_by(id: category_params[:id])
      ArticleCategory.create(name: category.name, child_name: category_params[:child_name])
      redirect_to admin_categories_path, notice: 'カテゴリーを新しく作成しました'
    else
      redirect_to admin_categories_path, alert: '不正なパラメーターです'
    end
  end

  private
  def category_params
    params.permit(:id, :child_name)
  end
end
