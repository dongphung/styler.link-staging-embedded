class Admin::ShopRegistrationsController < Admin::AdminController
  def index
    @registrations = ShopRegistration.unconfirmed
  end

  def show
    @registration = ShopRegistration.find_by(id: id_params[:id])
    redirect_to admin_shop_registrations_path, alert: 'ショップ登録申請が見つかりません' unless @registration
  end

  def update
    registration = ShopRegistration.find_by(id_params)
    if registration && approval_params[:approval].present?
      if approval_params[:approval] == '1'
        if shop_registration = registration.confirm_registration
          shop_registration.destroy
          redirect_to admin_shop_registrations_path, notice: 'リクエストを承認しました'
        else
          redirect_to admin_shop_registrations_path, alert: 'リクエストを処理出来ません'
        end
      elsif approval_params[:approval] == '0'
        registration.update(confirmed: true)
        redirect_to admin_shop_registrations_path, notice: 'リクエストを否認しました'
      end
    else
      redirect_to admin_shop_registrations_path, alert: 'リクエストを処理出来ません'
    end
  end

  private
  def id_params
    params.permit(:id)
  end

  def approval_params
    params.permit(:approval)
  end
end
