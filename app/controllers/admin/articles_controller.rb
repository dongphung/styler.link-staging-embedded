class Admin::ArticlesController < Admin::AdminController
  def index
    @articles = Article.uncategorized
  end

  def edit
    @article = Article.find_by(id: id_params[:id])
    if @article && @article.category_id.nil?
      @tag_names = Tag.all.sample(10).map{|t| t.name}
    else
      redirect_to admin_articles_path, alert: 'article not found'
    end
  end

  def update
    article = Article.find_by(id: id_params[:id])
    if article && category_params[:category_id]
      article.update(category_id: category_params[:category_id])
      tags_params.values.reject(&:blank?).uniq.each do |tag_name|
        tag = Tag.where(name: tag_name).first_or_create
        tag.articles << article
      end
      redirect_to admin_articles_path, notice: '該当記事を公開しました'
    else
      redirect_to admin_articles_path, alert: 'wrong parameter'
    end
  end

  def reload
    Article.create_articles_from_stylermag
    Author.create_authors_from_stylermag
    redirect_to admin_articles_path, notice: '記事を最新の情報に更新しました'
  end

  private
  def id_params
    params.permit(:id)
  end

  def category_params
    params.permit(:category_id)
  end

  def tags_params
    params.permit(:tag1, :tag2, :tag3, :tag4, :tag5, :tag6, :tag7, :tag8, :tag9, :tag10)
  end

  def reload_params
    params.permit(:reload)
  end
end
