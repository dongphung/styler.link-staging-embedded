class Admin::AdminController < ApplicationController
  before_action :redirect_to_root

  private
  def redirect_to_root
    redirect_to root_path, alert: '管理者権限が無いためアクセス出来ません' unless current_user.admin_flag
  end
end
