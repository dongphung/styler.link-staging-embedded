class Admin::PushNotificationsController < Admin::AdminController
  def new
  end

  def create
    if create_params[:push_type].present? && create_params[:message].present?
      case create_params[:push_type]
      when 'all_users', 'all_customers', 'all_staffs'
        PushNotification.create_push(create_params[:push_type], nil, create_params[:message], nil)
        redirect_to new_admin_push_notification_path, notice: 'プッシュを作成しました'
      else
        if create_params[:user_id].present? && create_params[:page_id].present?
          PushNotification.create_push(create_params[:push_type], create_params[:user_id], create_params[:message], create_params[:page_id])
          redirect_to new_admin_push_notification_path, notice: 'プッシュを作成しました'
        else
          redirect_to new_admin_push_notification_path, alert: '入力項目に不備があります'
        end
      end
    else
      redirect_to new_admin_push_notification_path, alert: '入力項目に不備があります'
    end
  end

  private
  def create_params
    params.permit(:user_id, :message, :page_id, :push_type)
  end
end
