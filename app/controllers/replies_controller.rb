class RepliesController < ApplicationController
  skip_before_action :authenticate_user!, only: %i(show embed)

  def new
    if current_user.staff?
      @post = Post.find_by(id: reply_params[:post_id])
      @item = Item.find_by(id: reply_params[:item_id])
      redirect_to root_path, alert: '投稿が見つかりません' unless @post
    else
      redirect_to root_path, alert: 'リプライの作成権限がありません'
    end
  end

  def create
    if current_user.staff?
      @item = Item.find_by(id: reply_params[:item_id])
      if @item
        if @item.shop_id == current_user.shop.try(:id)
          @item.update(item_params)
          @reply = Reply.create(reply_params)
          if @reply.try(:persisted?)
            redirect_to reply_path(@reply), notice: 'Replyを作成しました'
          else
            redirect_to new_reply_path(post_id: reply_params[:post_id]), alert: '入力内容に不備があります'
          end
        else
          redirect_to root_path, alert: '入力内容に不備があります'
        end
      else
        @item = Item.create_with_photo_single(item_params, images_params)
        @reply = Reply.create(reply_params.merge(item_id: @item.id))
        if @reply.try(:persisted?)
          redirect_to reply_path(@reply), notice: 'Replyを作成しました'
        else
          redirect_to new_reply_path(post_id: reply_params[:post_id]), alert: '入力内容に不備があります'
        end
      end
    else
      redirect_to root_path, alert: 'リプライの作成権限がありません'
    end
  end

  def show
    @reply = Reply.cached_find_by_id(id_params[:id], 5.minutes)
    if @reply
      activity_id = 26
      activity_logger(current_user, activity_id, @reply.id, @platform)
      @shop = @reply.shop
      @staff = @reply.user
      @post = @reply.post
      @item = @reply.item
    else
      redirect_to root_path, alert: 'Replyが見つかりません'
    end
  end

  def ex_show
    redirect_to reply_path(id: id_params[:id]), status: 301
  end

  def edit
    if @reply
      if @reply.user_id == current_user.id
        @post = @reply.post
      else
        redirect_to root_path, alert: 'Replyの編集権限がありません'
      end
    else
      redirect_to root_path, alert: 'Replyが見つかりません'
    end
  end

  def update
    reply = Reply.find_by(id: id_params[:id])
    if reply
      if reply.user_id == current_user.id
        reply.update(reply_params)
        reply.item.update(item_params)
        redirect_to reply_path(reply), notice: 'Replyを編集しました'
      else
        redirect_to root_path, alert: 'Replyの編集権限がありません'
      end
    else
      redirect_to root_path, alert: 'Replyが見つかりません'
    end
  end

  def generate_embed_link
    @reply = Reply.find_by(id_params)
    redirect_to anchor: 'embed-link', controller: 'replies', action: 'show', url: "<iframe src='#{replies_embed_url(@reply.id)}' frameborder='0' height='738' scrolling='no' style='background: rgb(255, 255, 255); border: 0px; margin: 1px; max-width: 658px; width: calc(100% - 2px); border-radius: 4px;'></iframe>"
  end

  def embed
      @reply = Reply.find_by(id_params)
      render :layout => false
  end

  private
  def id_params
    params.permit(:id)
  end

  def images_params
    params.permit(:photo_1, :photo_2, :photo_3, :photo_4)
  end

  def item_params
    params.permit(:brand, :name, :price, :text, :category_id).merge(shop_id: current_user.shop.id)
  end

  def reply_params
    params.permit(:post_id, :comment, :item_id).merge(user_id: current_user.id, shop_id: current_user.shop.id)
  end
end
