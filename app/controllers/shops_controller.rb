class ShopsController < ApplicationController
  skip_before_action :authenticate_user!, except: %i(edit update)

  def index
    # activity_id = 21
    # log_activity(current_user, activity_id, @platform)

    if location_params[:location]
      if order_params[:order] == 'name'
        @shops = Shop.web_index_alphabetical(page_params[:page]).by_location(location_params[:location])
      else
        @shops = Shop.web_index(page_params[:page]).by_location(location_params[:location])
      end
    else
      @locations = Settings.prefecture
    end
  end

  def show
    @shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if @shop
      @replies = @shop.replies.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ショップが見つかりません'
    end
  end

  def items
    @shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if @shop
      @items = shop.items.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ショップが見つかりません'
    end

    render raw: render_query_to_string_with_cache(@events, 60.minutes)
  end

  def events
    @shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if @shop
      @events = @shop.events.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ショップが見つかりません'
    end

    render raw: render_query_to_string_with_cache(@events, 60.minutes)
  end

  def staffs
    @shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if @shop
      @staffs = @shop.users.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ショップが見つかりません'
    end

    render raw: render_query_to_string_with_cache(@staffs, 60.minutes)
  end

  def information
    @shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    redirect_to root_path, alert: 'ショップが見つかりません' unless @shop
  end

  def edit
    @shop = Shop.cached_find_by_id(id_params[:id], 60.minutes)
    if @shop
      unless @shop == current_user.shop
        redirect_to root_path, alert: 'ショップ情報の編集権限がありません'
      end
    else
      redirect_to root_path, alert: 'ショップが見つかりません'
    end
  end

  def update
    shop = Shop.find_by(id: id_params[:id])
    if shop
      if shop == current_user.shop
        shop.update(name_params) if name_params[:name].present?
        shop.update(detail_params)
        Image.create_by_object(shop, image_params[:photo])
        redirect_to shop_path(shop), notice: 'ショップ情報を編集しました'
      else
        redirect_to root_path, alert: 'ショップ情報の編集権限がありません'
      end
    else
      redirect_to root_path, alert: 'ショップが見つかりません'
    end
  end

  private
  def detail_params
    params.permit(:profile, :address, :tel, :website, :business_hours)
  end

  def id_params
    params.permit(:id)
  end

  def image_params
    params.permit(:photo)
  end

  def location_params
    params.permit(:location)
  end

  def name_params
    params.permit(:name)
  end

  def order_params
    params.permit(:order)
  end

  def page_params
    params.permit(:page)
  end
end
