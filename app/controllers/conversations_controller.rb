class ConversationsController < ApplicationController
  def index
    ConversationNotification.by_user_id(current_user.id).unconfirmed.update_all(status: 1)
    @conversations = current_user.conversations.web_index(page_params[:page])
  end

  def new
    destination_user_id = new_params[:destination_user_id]
    conversation = Conversation.search(current_user.id, destination_user_id)
    if conversation
      redirect_to conversation_path(conversation)
    else
      destination = User.find(destination_user_id)
      reply = Reply.find_by(id: new_params[:reply_id])
      if valid_params?(destination, reply)
        @conversation = Conversation.new(newest_time: Time.now)
        @destination_user_id = destination_user_id
        @reply_id = new_params[:reply_id]
      else
        redirect_to root_path, alert: '不正なパラメーターです'
      end
    end
  end

  def show
    @conversation = Conversation.find_by(id: id_params[:id])
    if @conversation
      if @conversation.users.find_by(id: current_user.id)
        @conversation.notifications.by_user_id(current_user.id).update_all(status: 2)
        @messages = @conversation.messages.web_index(page_params[:page])
      else
        redirect_to root_path, alert: 'メッセージの閲覧権限がありません'
      end
    else
      redirect_to root_path, alert: 'メッセージが見つかりません'
    end
  end

  private
  def id_params
    params.permit(:id)
  end

  def new_params
    params.permit(:destination_user_id, :reply_id)
  end

  def page_params
    params.permit(:page)
  end

  def valid_params?(destination, reply)
    if reply
      destination.staff? && reply.user = destination
    else
      destination.customer?
    end
  end
end
