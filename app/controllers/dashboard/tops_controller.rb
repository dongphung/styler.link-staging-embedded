class Dashboard::TopsController < Dashboard::DashboardController
  def login
    @user = User.find_by(email: login_params[:email])
    if @user.staff?
      if @user.try(:valid_password?, login_params[:password])
        render template: 'dashboard/tops/login'
      else
        render json: {message: 'login failed'}, status: 400
      end
    else
      render json: {message: 'user is not staff'}, status: 401
    end
  end

  def shop_stats
    @shops = Shop.includes(:users).all
    @range = date_range
    render template: 'dashboard/tops/shop_stats'
  end

  def overall_stats
    @shops = Shop.where(created_at: date_range).count
    @posts = Post.where(created_at: date_range).count
    @replies = Reply.where(created_at: date_range).count
    @messages = Message.where(created_at: date_range).count
    render template: 'dashboard/tops/overall_stats'
  end

  def shop_staffs
    @shops = Shop.includes(users: :image).all
    @range = date_range
    render template: 'dashboard/tops/shop_staffs'
  end

  def shop_replies
    @shops = Shop.includes(:replies).all
    @range = date_range
    render template: 'dashboard/tops/shop_replies'
  end

  private
  def date_range
    date = date_params[:date] ? Date.parse(date_params[:date]) : 1.days.ago
    date.beginning_of_day..date.end_of_day
  end

  def date_params
    params.permit(:date)
  end

  def login_params
    params.permit(:email, :password)
  end
end
