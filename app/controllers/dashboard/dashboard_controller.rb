class Dashboard::DashboardController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  # before action
  before_action :basic_authentication

  # module
  include ApplicationHelper

  private
  def basic_authentication
    if Rails.env.staging?
      authenticate_or_request_with_http_basic do |id, pass|
        setting = Settings.basic_authentication
        id == setting.id && pass == setting.pass
      end
    end
  end
end
