class Users::ConfirmationsController < Devise::ConfirmationsController
  before_action :add_top_to_body

  # GET /resource/confirmation/new
  # def new
  #   @email = email_params[:email]
  #   super
  # end

  # POST /resource/confirmation
  # def create
  #   super
  # end

  # GET /resource/confirmation?confirmation_token=abcdef
  # def show
  #   super
  # end

  # protected

  # The path used after resending confirmation instructions.
  # def after_resending_confirmation_instructions_path_for(resource_name)
  #   super(resource_name)
  # end

  # The path used after confirmation.
  # def after_confirmation_path_for(resource_name, resource)
  #   super(resource_name, resource)
  # end

  # private
  # def email_params
  #   params.permit(:email)
  # end
end
