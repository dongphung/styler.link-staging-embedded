class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]
  before_action :add_top_to_body
  before_action :store_location

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create
    user = User.find_by(email: params[:user][:email]) 
    if user && user.provider.present?
      redirect_to new_user_session_path, alert: 'Facebookアカウントでログインしてください'
    else
      super
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # You can put the params you want to permit in the empty array.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end

  def after_sign_out_path_for(resource)
    new_user_session_path
  end
    
  def after_sign_in_path_for(resource)
    if session[:previous_url]['id']
      reply_path(previous_url) 
    else
      super
    end
  end

  private
  def previous_url
    url = session[:previous_url]['id']
    url.present? ? url : '/'
  end

  def store_location
    should_store_url = request.get? && !request.path.include?('/users/sign_in?id') && !request.xhr?
    session[:previous_url] = request.GET if should_store_url
  end
end
