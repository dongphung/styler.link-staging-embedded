class MessagesController < ApplicationController
  def create
    @message = Message.create_with_conversation(create_params[:text], current_user.id, create_params[:destination_user_id], create_params[:reply_id])
    @conversation = @message.conversation
    if conversation_params[:conversation_id]
      render
    else
      render js: 'window.location.reload();'
    end
  end

  private
  def create_params
    params.permit(:text, :destination_user_id, :reply_id)
  end

  def conversation_params
    params.permit(:conversation_id)
  end
end
