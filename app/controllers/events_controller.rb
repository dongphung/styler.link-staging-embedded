class EventsController < ApplicationController
  skip_before_action :authenticate_user!, only: %i(index show)

  def index
    activity_id = 22
    activity_id = 23 if page_params[:page].to_i > 1
    activity_logger(current_user, activity_id, page_params[:page].to_i, @platform)
    @events = Event.web_index(page_params[:page])
    render raw: render_query_to_string_with_cache(@events, 60.minutes)
  end

  def future_events
    @events = Event.future.web_index(page_params[:page])
  end

  def ongoing_events
    @events = Event.ongoing.web_index(page_params[:page])
  end

  def new
    redirect_to root_path, alert: 'イベントの作成権限がありません' unless current_user.staff?
  end

  def create
    if current_user.staff?
      event = Event.create_with_photo(event_params, image_params)
      if event.try(:persisted?)
        redirect_to event_path(event), notice: 'イベントが作成されました'
      else
       redirect_to new_event_path, alert: '入力内容に不備があります'
      end
    else
      redirect_to root_path, alert: 'イベントの作成権限がありません'
    end
  end

  def show
    @event = Event.find_by(id: id_params[:id])
    if @event
      @interest = @event.interests.find_by(user_id: current_user.id) if current_user
      @interest_users = @event.interest_users.includes(:image).recent
    else
      redirect_to root_path, alert: 'イベントが見つかりません'
    end
  end

  def edit
    @event = Event.find_by(id: id_params[:id])
    if @event
      unless @event.shop_id == current_user.shop.try(:id)
        redirect_to root_path, alert: 'イベントの編集権限がありません'
      end
    else
      redirect_to root_path, alert: 'イベントが見つかりません'
    end
  end

  def update
    event = Event.find_by(id: id_params[:id])
    if event
      if event.shop_id == current_user.shop.try(:id)
        event.update(event_params)
        Image.create_by_object(event, image_params[:photo])
        redirect_to event_path(event), notice: 'イベント情報を更新しました'
      else
        redirect_to root_path, alert: 'イベントの編集権限がありません'
      end
    else
      redirect_to root_path, alert: 'イベントが見つかりません'
    end
  end

  def destroy
    event = Event.find_by(id: id_params[:id])
    if event
      if event.shop_id == current_user.shop.try(:id)
        event.destroy
        redirect_to root_path, notice: 'イベントを削除しました'
      else
        redirect_to root_path, alert: 'イベントの削除権限がありません'
      end
    else
      redirect_to root_path, alert: 'イベントが見つかりません'
    end
  end

  private
  def event_params
    params.permit(:title, :text, :start_date, :finish_date, :location).merge(user_id: current_user.id, shop_id: current_user.shop.id)
  end

  def id_params
    params.permit(:id)
  end

  def image_params
    params.permit(:photo)
  end

  def page_params
    params.permit(:page)
  end
end
