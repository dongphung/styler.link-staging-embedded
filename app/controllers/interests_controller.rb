class InterestsController < ApplicationController
  def create
    @event = Event.find_by(id: create_params[:event_id])
    if @event && current_user.customer?
      @interest = Interest.where(create_params).first_or_create
      render
    else
      render js: 'window.location = "/"'
    end
  end

  def destroy
    interest = Interest.find_by(id: destroy_params[:id])
    if interest && interest.user_id == @current_user.id
      @event = interest.event
      interest.destroy
      render
    else
      render js: 'window.location = "/"'
    end
  end

  private
  def create_params
    params.permit(:event_id).merge(user_id: current_user.id)
  end

  def destroy_params
    params.permit(:id)
  end
end
