class UsersController < ApplicationController
  skip_before_action :authenticate_user!, except: %i(edit update setting)
  skip_before_action :nameless_user, only: %i(edit update)

  def show
    @user = User.find_by(id: id_params[:id])
    if @user.try(:customer?)
      @posts = @user.posts.web_index(page_params[:page])
    elsif @user.try(:staff?)
      @replies = @user.replies.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ユーザーが見つかりません'
    end
  end

  def likes
    @user = User.find_by(id: id_params[:id])
    if @user
      @like_items = @user.like_items.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ユーザーが見つかりません'
    end
  end

  def interests
    @user = User.find_by(id: id_params[:id])
    if @user
      @interest_events = @user.interest_events.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ユーザーが見つかりません'
    end
  end

  def watches
    @user = User.find_by(id: id_params[:id])
    if @user.try(:customer?)
      @watch_posts = @user.watch_posts.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ユーザーが見つかりません'
    end
  end

  def follows
    @user = User.find_by(id: id_params[:id])
    if @user.try(:customer?)
      @follow_users = @user.follow_users.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ユーザーが見つかりません'
    end
  end

  def followers
    @user = User.find_by(id: id_params[:id])
    if @user.try(:staff?)
      @follower_users = @user.follower_users.web_index(page_params[:page])
    else
      redirect_to root_path, alert: 'ユーザーが見つかりません'
    end
  end

  def edit
  end

  def update
    if mail_params[:mail_notification].present?
      current_user.update(mail_params)
      redirect_to root_path, notice: '登録情報を更新しました'
    elsif email_params[:email].present?
      unless User.exists?(email: email_params[:email])
        current_user.update(email_params)
        redirect_to root_path, notice: '登録情報を更新しました'
      else
        redirect_to change_email_users_path, alert: '入力したメールアドレスは既に登録されています'
      end
    else
      current_user.update(name_params) if name_params[:name].present?
      current_user.update(detail_params)
      Image.create_by_object(current_user, image_params[:photo])
      redirect_to root_path, notice: '登録情報を更新しました'
    end
  end

  def setting
  end

  private
  def id_params
    params.permit(:id)
  end

  def name_params
    params.permit(:name)
  end

  def image_params
    params.permit(:photo)
  end

  def detail_params
    params.permit(:profile, :location, :birthday)
  end

  def mail_params
    params.permit(:mail_notification, :mail_magazine)
  end

  def email_params
    params.permit(:email)
  end

  def page_params
    params.permit(:page)
  end
end
