class WatchesController < ApplicationController
  def create
    @post = Post.find_by(id: create_params[:post_id])
    if @post && current_user.customer? && @post.user_id != current_user.id
      @watch = Watch.where(create_params).first_or_create
      render
    else
      render js: 'window.location = "/"'
    end
  end

  def destroy
    watch = Watch.find_by(id: destroy_params[:id])
    if watch && watch.user_id == current_user.id
      @post = watch.post
      watch.destroy
      render
    else
      render js: 'window.location = "/"'
    end
  end

  private
  def create_params
    params.permit(:post_id).merge(user_id: current_user.id)
  end

  def destroy_params
    params.permit(:id)
  end
end
