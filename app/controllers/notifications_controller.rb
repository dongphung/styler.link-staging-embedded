class NotificationsController < ApplicationController
  def index
    if conversation_tab?
      redirect_to conversations_path
    else
      Notification.browse!(current_user.id)
      @notifications = current_user.notifications.web_index(page_params[:page])
    end
  end

  private
  def conversation_tab?
    current_user.notifications.unconfirmed.where.not(type: 'ConversationNotification').blank? && current_user.notifications.unconfirmed.where(type: 'ConversationNotification').present?
  end

  def page_params
    params.permit(:page)
  end
end
