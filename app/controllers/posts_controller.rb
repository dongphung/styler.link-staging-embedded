class PostsController < ApplicationController
  skip_before_action :authenticate_user!, only: %i(index pickup show)

  def index
    activity_id = 15
    activity_id = 16 if page_params[:page].to_i > 1
    activity_logger(current_user, activity_id, page_params[:page].to_i, @platform)
    @posts = Post.joins(:replies).uniq.web_index_newest(page_params[:page])
  end

  def latest
    activity_id = 17
    activity_id = 18 if page_params[:page].to_i > 1
    activity_logger(current_user, activity_id, page_params[:page].to_i, @platform)
    @posts = Post.web_index(page_params[:page])
  end

  def unreplied
    if current_user.staff?
      if category_params[:category_id]
        @posts = Post.where.not(id: current_user.replies.pluck(:id)).by_category(category_params[:category_id]).web_index(page_params[:page])
      else
        redirect_to root_path, alert: '不正なパラメーターです'
      end
    else
      redirect_to root_path, alert: '投稿の閲覧権限がありません'
    end
  end

  def new
    redirect_to root_path, alert: '投稿の作成権限がありません' unless current_user.customer?
  end

  def create
    if current_user.customer?
      post = Post.create(post_params)
      if post.try(:persisted?)
        redirect_to root_path, notice: '投稿を作成しました'
      else
        redirect_to new_post_path, alert: '入力内容に不備があります'
      end
    else
      redirect_to root_path, alert: '投稿の作成権限がありません'
    end
  end

  def show
    @post = Post.cached_find_by_id(id_params[:id], 5.minutes)
    if @post
      activity_id = 24
      activity_logger(current_user, activity_id, @post.id, @platform)
      @watches = @post.watches
      @comments = @post.comments
      @user = @post.user
    else
      redirect_to root_path, alert: '投稿が見つかりません'
    end
  end

  def edit
    if @post
      unless @post.user_id == current_user.id
        redirect_to root_path, alert: '投稿の編集権限がありません'
      end
    else
      redirect_to root_path, alert: '投稿が見つかりません'
    end
  end

  def update
    if post
      if post.user_id == current_user.id
        post.update(post_params)
        redirect_to post_path(post), notice: '投稿を更新しました'
      else
        redirect_to root_path, alert: '投稿の編集権限がありません'
      end
    else
      redirect_to root_path, alert: '投稿が見つかりません'
    end
  end

  def destroy
    if post
      if post.replies.blank? && post.user_id == current_user.id
        post.destroy
        redirect_to root_path, notice: '投稿を削除しました'
      else
        redirect_to root_path, alert: '投稿の削除権限がありません'
      end
    else
      redirect_to root_path, alert: '投稿が見つかりません'
    end
  end

  private
  def category_params
    params.permit(:category_id)
  end

  def id_params
    params.permit(:id)
  end

  def page_params
    params.permit(:page)
  end

  def post_params
    params.permit(:text, :category_id).merge(user_id: current_user.id)
  end
end
