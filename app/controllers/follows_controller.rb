class FollowsController < ApplicationController
  def create
    @staff = User.find_by(id: follow_params[:staff_id])
    if @staff && current_user.customer?
      @follow = Follow.where(follow_params).first_or_create
      render
    else
      render js: 'window.location = "/"'
    end
  end

  def destroy
    follow = Follow.find_by(id: destroy_params[:id])
    if follow && follow.user_id == current_user.id
      @staff = follow.staff
      follow.destroy
      render
    else
      render js: 'window.location = "/"'
    end
  end

  private
  def follow_params
    params.permit(:staff_id).merge(user_id: current_user.id)
  end

  def destroy_params
    params.permit(:id)
  end
end
