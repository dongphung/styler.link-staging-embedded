class LikesController < ApplicationController
  def create
    item = Item.find_by(id: like_params[:item_id])
    if item && item.shop_id != current_user.shop.try(:id)
      like = Like.where(like_params).first_or_create
      Favorite.where(favorite_params).first_or_create if favorite_params[:reply_id]
      render
    else
      render js: 'window.location = "/"'
    end
  end

  def destroy
    like = Like.find_by(id: id_params[:id])
    if like && like.user_id == current_user.id
      like.destroy
      render
    else
      render js: 'window.location = "/"'
    end
  end

  private
  def favorite_params
    params.permit(:reply_id).merge(user_id: current_user.id)
  end

  def id_params
    params.permit(:id)
  end

  def like_params
    params.permit(:item_id).merge(user_id: current_user.id)
  end
end
