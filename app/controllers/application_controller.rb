class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # before action
  before_action :app_webview
  before_action :authenticate_user!
  before_action :basic_authentication
  before_action :detect_browser
  before_action :get_notice_flag
  before_action :nameless_user
  before_action :set_mailer_host
  before_action :store_current_location, unless: :devise_controller?
  before_action :update_login_count

  # module
  include ViewCache

  def add_top_to_body
    @body_class = 'top'
  end

  def append_info_to_payload(payload)
      super
      payload[:uuid] = request.uuid
      payload[:host] = request.host
      payload[:remote_ip] = request.remote_ip
      payload[:user_agent] = request.user_agent
      payload[:os] = request.os
      payload[:os_version] = request.os_version
      payload[:browser] = request.browser
      payload[:browser_version] = request.browser_version
      payload[:user_id] = current_user.try(:id)
  end

  # output activity logs to activity_xxx.log
  def activity_logger(user, activity_id, detail, platform, **extra)
    File.open("#{Rails.root}/log/activity_#{Rails.env}.log", "a") do |f|
      f.puts({
        "user_id" => user.try(:id),
        "activity_id" => activity_id,
        "platform_id" => platform.id,
        "detail" => detail,
        "created_at" => Time.current.to_s
      }.to_json)
    end
  end

  private
  def app_webview
    @app_webview = request.query_parameters.has_key?(:app_webview)
  end

  def basic_authentication
    if Rails.env.staging?
      authenticate_or_request_with_http_basic do |id, pass|
        setting = Settings.basic_authentication
        id == setting.id && pass == setting.pass
      end
    end
  end

  def detect_browser
    @browser = Browser.new(
      request.headers['User-Agent'],
      accept_language: request.headers['Accept-Language']
    )

    @platform = Platform.find_by(category: 'Web', name: 'Other')
    @platform = Platform.find_by(category: 'Web', name: 'iOS') if @browser.platform.ios?
    @platform = Platform.find_by(category: 'Web', name: 'Android') if @browser.platform.android?
  end

  def get_notice_flag
    @notice_flag = current_user && current_user.notifications.unconfirmed.present?
  end

  def nameless_user
    if current_user && current_user.name.blank?
      redirect_to edit_user_path(current_user), notice: 'プロフィール情報を入力してください'
    end
  end

  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end

  def store_current_location
    store_location_for(:user, request.url)
  end

  def update_login_count
    today = Date.today
    if current_user && current_user.last_login_date != today
      current_user.login_count += 1
      current_user.update(last_login_date: today)
    end
  end
end
