class CommentsController < ApplicationController
  def create
    @post = Post.find_by(id: create_params[:post_id])
    if @post
      @comment = Comment.where(create_params).first_or_create
      render
    else
      render js: 'window.location = "/"'
    end
  end

  def destroy
    comment = Comment.find_by(id: destroy_params[:id])
    if comment && comment.user_id == current_user.id
      @post = comment.post
      comment.destroy
      render
    else
      render js: 'window.location = "/"'
    end
  end

  private
  def create_params
    params.permit(:text, :item_id, :post_id).merge(user_id: current_user.id)
  end

  def destroy_params
    params.permit(:id)
  end
end
