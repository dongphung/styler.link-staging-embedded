class ReportsController < ApplicationController
  def create
    Report.create(create_params)
    redirect_to root_path, notice: '投稿を報告しました'
  end

  private
  def create_params
    params.permit(:text, :object, :generic_id).merge(user_id: current_user.id)
  end
end
