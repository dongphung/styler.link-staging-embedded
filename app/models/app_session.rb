class AppSession < ActiveRecord::Base
  # association
  belongs_to :user

  # callback
  after_create :set_expired_date

  # invariable
  SNS_YML = YAML.load_file("#{Rails.root}/config/aws_sns.yml")[Rails.env]

  # validation
  validates :user_id, :token, presence: true

  class << self
    def create_session(user)
      user.update(push_notification: true) if user.app_sessions.blank?
      create(user_id: user.id, token: SecureRandom.uuid)
    end

    def destroy_expired_session
      AppSession.where('expired_date < ?', Date.today).destroy_all
    end
  end

  private
  def set_expired_date
    update(expired_date: Date.today + 6.month)
  end
end
