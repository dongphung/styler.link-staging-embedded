class Message < ActiveRecord::Base
  # association
  belongs_to :conversation
  belongs_to :user

  # callback
  after_create :update_newest_time

  # serialize
  serialize :objects

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :api_pagination, ->(last_object) { 
    where('messages.created_at <= ?', last_object.created_at).
    where('messages.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(:conversation).api_pagination(last_object).recent.limit(Settings.api_limit[:messages]) }
  scope :web_index, ->(page) { includes(:conversation).recent.page(page).per(Settings.api_limit[:messages]) }

  # validation
  validates :conversation_id, presence: true

  class << self
    def create_with_conversation(text, user_id, destination_user_id, item_id=nil)
      conversation = Conversation.search(user_id, destination_user_id)
      unless conversation
        conversation = Conversation.create(newest_time: Time.now)
        conversation.users << User.where(id: [user_id, destination_user_id])
        if item_id.blank? && User.find(user_id).staff?
          message_text = 'フォローしたスタッフがメッセージを送信しました'
        else
          message_text = 'プロフィールを見たユーザーがメッセージを送信しました'
        end
        NotificationMessage.where(text: message_text, conversation_id: conversation.id).first_or_create
      end
      if item_id.present?
        NotificationMessage.where(text: '下記商品についてメッセージを送信しました', conversation_id: conversation.id).first_or_create
        ItemMessage.where(user_id: user_id, conversation_id: conversation.id, objects: item_objects(item_id)).first_or_create
      end
      if reply_id.present?
        NotificationMessage.where(text: "下記商品についてメッセージを送信しました", conversation_id: conversation.id).first_or_create
        ReplyMessage.where(user_id: user_id, conversation_id: conversation.id, objects: reply_objects(reply_id)).first_or_create
      end
      UserMessage.create(user_id: user_id, text: text, conversation_id: conversation.id)
    end

    def item_objects(item_id)
      item = Item.find(item_id)
      { id: item.id,
        brand: item.brand, 
        name: item.name, 
        price: item.price, 
        image_url: item.image.try(:url)}
    end
  end

  def destination
    conversation.users.id_except(user_id).first
  end

  def user_image_url
    user_id ? user.image.try(:url) : nil
  end

  private
  def update_newest_time
    conversation.update(newest_time: created_at)
  end

  def send_mail
    PostMailer.message_mail(self, destination).deliver_now
  end

  def create_link_message
    if url = URI.extract(self.text).first
      agent = Mechanize.new
      page = agent.get(url)
      LinkMessage.create(user_id: user_id, conversation_id: conversation_id, objects: objects_hash(url, page)) if objects_hash(url, page)
    end
  rescue => e
    AdministratorMailer.rescue_notification(e, __method__).deliver_now
    logger.error e
  end

  def create_notification
    notification = ConversationNotification.where(user_id: destination.id, generic_id: conversation.id).first_or_create
    notification.unconfirmed!
    notification.touch
  end

  def create_push_notification
    message = "#{user.name}さんからのメッセージ「#{text}」"
    PushNotification.create_push('message', destination.id, message, conversation.id)
  end

  def objects_hash(url, page)
    if title(page) && image_url(page) && description(page)
      {url: url, title: title(page), image_url: image_url(page), description: description(page)}
    end
  end

  def title(page)
    page.at('meta[property="og:title"]') && page.at('meta[property="og:title"]')['content']
  end

  def image_url(page)
    page.at('meta[property="og:image"]') && page.at('meta[property="og:image"]')['content']
  end

  def description(page)
    page.at('head meta[name="description"]') && page.at('head meta[name="description"]')['content']
  end

  # delayed job
  handle_asynchronously :send_mail
  handle_asynchronously :create_link_message
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
end
