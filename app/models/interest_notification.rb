class InterestNotification < Notification
  # association
  belongs_to :interest, class_name: :Interest, foreign_key: :generic_id
  has_one    :notification_object, source: :event, through: :interest
  has_one    :notification_author, source: :user, through: :interest
end
