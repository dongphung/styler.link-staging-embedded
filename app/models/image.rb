class Image < ActiveRecord::Base
  # callback
  before_destroy :destroy_s3_objects

  # invariable
  S3_YML = YAML.load_file("#{Rails.root}/config/s3.yml")[Rails.env]
  CLOUDFRONT_YML = YAML.load_file("#{Rails.root}/config/cloudfront.yml")[Rails.env]

  # scope
  scope :by_object, ->(object) { where(type: "#{object.class.name}Image", generic_id: object.id) }

  # validation
  validates :generic_id, :type, :photo_content_type, :random_string, presence: true

  class << self
    def create_by_object(object, photo)
      if object.present? && photo.present?
        image = by_object(object).first_or_initialize
        image.photo_file_name = photo.original_filename
        image.photo_content_type = photo.content_type
        image.photo_file_size = photo.size
        image.photo_updated_at = Time.now
        image.random_string = SecureRandom.hex(4)
        image.save
        s3_upload(image, photo)
        upload_completed?(image)
      else
        false
      end
    end

    def s3_upload(image, photo)
      extension = photo.content_type.gsub('image/','')
      bucket.object("photos/#{image.id}/original_#{image.random_string}.#{extension}").upload_file(photo.tempfile)
    end

    def upload_completed?(image)
      extension = image.photo_content_type.gsub('image/','')
      url = "#{S3_YML['url']}/photos/#{image.id}/rectangle_#{image.random_string}.#{extension}"
      i = 0
      until Net::HTTP.get_response(URI.parse(url)).code == '200'
        sleep(1)
        i += 1
        break if i > 15
      end
    end

    def create_by_facebook_user(user)
      image = by_object(user).first_or_initialize
      image.photo_file_name = "#{user.uid}.jpeg"
      image.photo_content_type = 'image/jpeg'
      image.photo_file_size = 999
      image.photo_updated_at = Time.now
      image.random_string = SecureRandom.hex(4)
      image.save
      url = "https://graph.facebook.com/#{user.uid}/picture?width=640"
      s3_upload_by_url(image, url)
      upload_completed?(image)
    end

    def s3_upload_by_url(image, url)
      open(url) do |data|
        File.open("temporary_#{image.random_string}.jpeg","wb"){|f| f << data.read }
        bucket.object("photos/#{image.id}/original_#{image.random_string}.jpeg").upload_file("temporary_#{image.random_string}.jpeg")
        File.delete("temporary_#{image.random_string}.jpeg")
      end
    rescue => e
      AdministratorMailer.rescue_notification(e, __method__).deliver_now
      logger.error e
    end

    def bucket
      Aws::S3::Resource.new(
        region: S3_YML['region'],
        access_key_id: S3_YML['access_key_id'],
        secret_access_key: S3_YML['secret_access_key']
        ).bucket(S3_YML['bucket'])
    end
  end

  def url(size='medium')
    extension = photo_content_type.gsub('image/','')
    "#{CLOUDFRONT_YML['url']}/photos/#{id}/#{size}_#{random_string}.#{extension}"
  end

  private
  def destroy_s3_objects
    bucket = Aws::S3::Resource.new(
          region: S3_YML['region'],
          access_key_id: S3_YML['access_key_id'],
          secret_access_key: S3_YML['secret_access_key']
          ).bucket(S3_YML['bucket'])
    bucket.objects(prefix: "photos/#{id}/").each{|object| object.delete}
  end
end
