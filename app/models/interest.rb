class Interest < ActiveRecord::Base
  # association
  belongs_to :user
  belongs_to :event
  has_many   :notifications, class_name: :InterestNotification, foreign_key: :generic_id, dependent: :destroy

  # callback
  after_create :create_notification
  after_create :create_push_notification

  # counter_cache
  counter_culture :user

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }

  # validation
  validates :user_id, :event_id, presence: true
  validates :user_id, uniqueness: { scope: [:event_id] }

  class << self
    def mine(user_id)
      find_by(user_id: user_id)
    end
  end

  def notice_users
    event.shop.users
  end

  private
  def create_notification
    notice_users.each do |user|
      InterestNotification.where(user_id: user.id, generic_id: id).first_or_create
    end
  end

  def create_push_notification
    message = "#{user.name}がイベントに参加予定です"
    notice_users.each do |user|
      PushNotification.create_push('interest', user.id, message, event.id)
    end
  end

  # delayed job
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
end
