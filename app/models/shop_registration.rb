class ShopRegistration < ActiveRecord::Base
  # association
  has_one :image, class_name: :ShopRegistrationImage, foreign_key: :generic_id

  # callback
  after_create  :send_mail
  after_destroy :send_notice_mail

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :unconfirmed, -> { where(confirmed: false) }

  # validation
  validates :shop_name, :shop_profile, :shop_website, :staff_name, :staff_email, :staff_password, presence: true
  validates_length_of :staff_password, minimum: 8

  class << self
    def create_with_photo(registration_params, image_params)
      if correct_values?(registration_params, image_params)
        registration_params.delete('staff_password_confirmation')
        registration = self.where(registration_params).first_or_create
        Image.create_by_object(registration, image_params[:photo])
        registration
      end
    end

    def correct_values?(registration_params, image_params)
      image_params[:photo] && User.find_by(email: registration_params[:staff_email]).nil? && registration_params[:staff_password] == registration_params[:staff_password_confirmation]
    end
  end

  def confirm_registration
    user = User.create(
      email: staff_email,
      password: staff_password,
      name: staff_name,
      uid: SecureRandom.uuid
    )
    if user
      shop = Shop.create(
        name: shop_name,
        profile: shop_profile,
        website: shop_website
      )
      image.update_columns(type: 'ShopImage', generic_id: shop.id) if shop
    end
    if user && shop
      shop.users << user
      self
    else
      false
    end
  end

  private
  def send_mail
    AdministratorMailer.registration_notification(self).deliver_now
  end

  def send_notice_mail
    StaffMailer.approval_mail(self).deliver_now
  end
end
