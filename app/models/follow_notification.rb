class FollowNotification < Notification
  # association
  belongs_to :follow, class_name: :Follow, foreign_key: :generic_id
  has_one    :notification_object, source: :user, through: :follow
  has_one    :notification_author, source: :user, through: :follow
end
