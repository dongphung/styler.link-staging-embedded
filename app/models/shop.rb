class Shop < ActiveRecord::Base
  # association
  has_many :events, dependent: :destroy
  has_one  :image, class_name: :ShopImage, foreign_key: :generic_id, dependent: :destroy
  has_many :items, dependent: :destroy
  has_many :replies, dependent: :destroy
  has_many :shop_users, dependent: :destroy
  has_many :users, through: :shop_users

  # callback
  after_save   :clear_cache
  after_create :create_sfdc_object
  after_create :set_authenticate_params
  after_create :set_newest_time

  # invariable
  SFDC_YML = YAML.load_file("#{Rails.root}/config/sfdc.yml")[Rails.env]

  # module
  include ModelCache

  # scope
  scope :has_name, -> { where.not(name: nil) }
  scope :is_opened, -> { where(closed: false) }
  scope :by_location, ->(location_id) { where(location: location_id) }
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :newest, -> { order(newest_time: :desc, id: :desc) }
  scope :alphabetical, -> { order(name: :asc, id: :desc) }
  scope :api_pagination, ->(last_object) { 
    where('shops.newest_time <= ?', last_object.newest_time).
    where('shops.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(:image).api_pagination(last_object).has_name.is_opened.newest.limit(Settings.api_limit[:shops]) }
  scope :api_pagination_alphabetical, ->(last_object) { 
    where('shops.name >= ?', last_object.name).
    where('shops.id < ?', last_object.id) if last_object }
  scope :api_index_alphabetical, ->(last_object) { includes(:image).api_pagination_alphabetical(last_object).has_name.is_opened.alphabetical.limit(Settings.api_limit[:shops]) }
  scope :web_index, ->(page) { includes(:image).has_name.is_opened.newest.page(page).per(Settings.api_limit[:shops]) }
  scope :web_index_alphabetical, ->(page) { includes(:image).has_name.is_opened.alphabetical.page(page).per(Settings.api_limit[:shops]) }

  class << self
    def check_inactive_shops
      self.all.each do |shop|
        if shop.replies.present?
          if shop.reply_inactive?
            shop.update_sfdc_inactive_shop_with_reply
          else
            shop.update_sfdc_active_shop
          end
        else
          shop.update_sfdc_inactive_shop_with_no_reply
        end
      end
    rescue => e
      AdministratorMailer.rescue_notification(e, __method__).deliver_now
      logger.error e
    end

    def has_new_shop?(location_id)
      recent_shop = by_location(location_id).recent.first
      recent_shop.created_at > 1.month.ago if recent_shop
    end
  end

  def stats_replies_count(range)
    replies.where(created_at: range).count
  end

  def stats_likes_count(range)
    Like.where(item_id: items.pluck(:id)).where(created_at: range).count
  end

  def stats_messages_count(range)
    array = []
    self.users.each do |staff|
      array << UserMessage.where(conversation_id: User.find(staff.id).conversations.pluck(:id)).where.not(user_id: staff.id).where(created_at: range).count
    end
    array.inject(:+)
  end

  def add_staffs(params)
    ['email1', 'email2', 'email3', 'email4', 'email5'].each do |email|
      if params[email].present?
        user = User.find_by(email: params[email])
        unless user
          password = SecureRandom.hex(4)
          user = User.create(
            email: params[email],
            password: password,
            name: nil,
            uid: SecureRandom.uuid
          )
        end
        if user && user.customer?
          users << user
          send_invite_mail(user, password)
        end
      end
    end
  end

  def send_contact(params)
    users.each do |staff|
      StaffMailer.article_contact_mail(staff.email, name, params).deliver_now
    end
  end

  def send_reprint(params)
    users.each do |staff|
      StaffMailer.reprint_contact_mail(staff.email, name, params).deliver_now
    end
  end

  def reply_inactive?
    replies.recent.first.created_at + 7.days < Time.now
  end

  def update_sfdc_inactive_shop_with_reply
    sfdc_client.upsert(
      'Account', 
      'shop_styid__c',
      shop_styid__c: id,
      Name: name.presence || '-',
      BillingStreet: address,
      Phone: tel,
      Website: website,
      inactive_shop__c: '1週間以上Replyがありません'
      )
  end

  def update_sfdc_inactive_shop_with_no_reply
    sfdc_client.upsert(
      'Account',
      'shop_styid__c',
      shop_styid__c: id,
      Name: name.presence || '-',
      BillingStreet: address,
      Phone: tel,
      Website: website,
      inactive_shop__c: 'まだ1件もReplyがありません'
      )
  end

  def update_sfdc_active_shop
    sfdc_client.upsert(
      'Account',
      'shop_styid__c',
      shop_styid__c: id,
      Name: name.presence || '-',
      BillingStreet: address,
      Phone: tel,
      Website: website,
      inactive_shop__c: ''
      )
  end

  private
  def set_authenticate_params
    update(authenticate_params: SecureRandom.uuid)
  end

  def set_newest_time
    update(newest_time: created_at)
  end

  def send_invite_mail(staff, password=nil)
    StaffMailer.staff_invite_mail(staff, password).deliver_now
  end

  # SFDCにショップ情報を登録
  def create_sfdc_object
    sfdc_client.create(
      'Account',
      shop_styid__c: id,
      Name: name.presence || '-',
      BillingStreet: address,
      Phone: tel,
      Website: website
      )
  rescue => e
    AdministratorMailer.rescue_notification(e, __method__).deliver_now
    logger.error e
  end

  def sfdc_client
    Restforce.new(
      username:        SFDC_YML['username'],
      password:        SFDC_YML['password'],
      security_token:  SFDC_YML['security_token'],
      client_id:       SFDC_YML['client_id'],
      client_secret:   SFDC_YML['client_secret'],
      host:            SFDC_YML['host'] )
  end
end
