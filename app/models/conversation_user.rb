class ConversationUser < ActiveRecord::Base
  # association
  belongs_to :conversation
  belongs_to :user

  # validation
  validates :user_id, :conversation_id, presence: true
end
