class ReplyNotification < Notification
  # association
  belongs_to :reply, class_name: :Reply, foreign_key: :generic_id
  has_one    :notification_object, source: :item, through: :reply
  has_one    :notification_author, source: :user, through: :reply
end
