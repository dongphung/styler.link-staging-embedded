class Comment < ActiveRecord::Base
  # association
  has_many   :notifications, class_name: :CommentNotification, foreign_key: :generic_id, dependent: :destroy
  belongs_to :post
  belongs_to :user
  belongs_to :item

  # callback
  after_create :update_newest_time
  after_create :create_notification
  after_create :create_push_notification
  after_create :send_mail

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :oldest, -> { order(created_at: :asc, id: :asc) }
  scope :api_pagination, ->(last_object) { 
    where('comments.created_at >= ?', last_object.created_at).
    where('comments.id > ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(user: :image).includes(item: :images).
    api_pagination(last_object).oldest.limit(Settings.api_limit[:comments]) }

  # validation
  validates :user_id, :post_id, presence: true

  def notice_users
    [post.user].concat(post.watch_users).concat(post.comment_users).concat(post.reply_users).uniq
  end

  private
  def update_newest_time
    post.update(newest_time: created_at)
  end

  def create_notification
    notice_users.each do |user|
      CommentNotification.where(user_id: user.id, generic_id: id).first_or_create
    end
  end

  def create_push_notification
    message = "#{user.name}さんがコメントを投稿しました"
    notice_users.each do |user|
      PushNotification.create_push('comment', user.id, message, post_id)
    end
  end

  def send_mail
    notice_users.each do |user|
      PostMailer.comment_mail(self, user).deliver_now
    end
  end

  # delayed job
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
  handle_asynchronously :send_mail
end
