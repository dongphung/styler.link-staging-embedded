class Favorite < ActiveRecord::Base
  # association
  belongs_to :user
  belongs_to :reply

  # validation
  validates :user_id, :reply_id, presence: true
  validates :user_id, uniqueness: { scope: [:reply_id] }
end
