class ArticleTag < ActiveRecord::Base
  # association
  belongs_to :article
  belongs_to :tag

  # validation
  validates :article_id, :tag_id, presence: true
  validates :article_id, uniqueness: { scope: [:tag_id] }
end
