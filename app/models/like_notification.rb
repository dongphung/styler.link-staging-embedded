class LikeNotification < Notification
  # association
  belongs_to :like, class_name: :Like, foreign_key: :generic_id
  has_one    :notification_object, source: :item, through: :like
  has_one    :notification_author, source: :user, through: :like
end
