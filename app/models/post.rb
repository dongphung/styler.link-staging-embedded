class Post < ActiveRecord::Base
  # association
  has_many   :comments, dependent: :destroy
  has_many   :comment_users, source: :user, through: :comments
  has_many   :replies, dependent: :destroy
  has_many   :reply_items, source: :item, through: :replies
  has_many   :reply_users, source: :user, through: :replies
  belongs_to :user
  has_many   :watches, dependent: :destroy
  has_many   :watch_users, source: :user, through: :watches

  # callback
  after_create :set_newest_time

  # counter_cache
  counter_culture :user

  # module
  include ModelCache

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :newest, -> { order(newest_time: :desc, id: :desc) }
  scope :web_loading, -> { includes(replies: :image).includes(user: :image) }
  scope :by_category, ->(category_id) { where(category_id: category_id) }
  scope :api_pagination, ->(last_object) { 
    where('posts.created_at <= ?', last_object.created_at).
    where('posts.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(user: :image).
    api_pagination(last_object).recent.limit(Settings.api_limit[:posts]) }
  scope :api_pagination_newest, ->(last_object) { 
    where('posts.newest_time <= ?', last_object.newest_time).
    where('posts.id < ?', last_object.id) if last_object }
  scope :api_index_newest, ->(last_object) { includes(user: :image).
    api_pagination_newest(last_object).newest.limit(Settings.api_limit[:posts]) }
  scope :web_index, ->(page) { includes(user: :image).
    includes(:replies).recent.page(page).per(Settings.api_limit[:posts]) }
  scope :web_index_newest, ->(page) { includes(user: :image).
    includes(replies: :image).newest.page(page).per(Settings.api_limit[:posts]) }

  # validation
  validates :text, :user_id, presence: true

  def user_recommended_item_images_url
    comments.where.not(item_id: nil).includes(item: :images).map{|comment| comment.item.image.try(:url)}
  end

  def json_watch_id(current_user)
    current_user && watches.mine(current_user.id).try(:id)
  end

  def json_reply_id(current_user)
    current_user.try(:staff?) && replies.my_shops(current_user.shop.id).try(:id) || nil
  end

  private
  def set_newest_time
    update(newest_time: created_at)
  end
end
