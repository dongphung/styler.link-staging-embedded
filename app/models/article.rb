class Article < ActiveRecord::Base
  # association
  has_many   :article_tags, dependent: :destroy
  belongs_to :author
  belongs_to :category
  has_many   :tags, through: :article_tags

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :week_latest, -> { where(created_at: (7.days.ago.beginning_of_day)..(Date.today.end_of_day)) }
  scope :api_pagination, ->(last_object) { 
    where('articles.created_at <= ?', last_object.created_at).
    where('articles.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(:tags).
    api_pagination(last_object).recent.categorized.limit(Settings.api_limit[:articles]) }
  scope :categorized, -> { where.not(category_id: nil) }
  scope :uncategorized, -> { where(category_id: nil) }

  # validation
  validates :wp_post_id, presence: true
  validates :wp_post_id, uniqueness: true

  class << self
    # def create_articles_from_stylermag
    #   Stylermagpost.includes(:stylermagpostmetas).publish.each do |stylermag|
    #     article = where(wp_post_id: stylermag.id).first_or_create
    #     article.update(
    #       author_id:   stylermag.post_author,
    #       title:       stylermag.post_title,
    #       description: stylermag.article_description,
    #       keywords:    stylermag.article_keywords,
    #       content:     stylermag.post_content,
    #       thumbnail:   stylermag.article_thumbnail,
    #       created_at:  stylermag.post_date,
    #       updated_at:  stylermag.post_modified)
    #     end
    #   end
    # rescue => e
    #   AdministratorMailer.rescue_notification(e, __method__).deliver_now
    #   logger.error e
    # end

    def create_articles_from_stylermag
      require 'open-uri'
      rss = SimpleRSS.parse open('http://stylermag.link/feed/')
      rss.items.each do |item|
        wp_post_id = item.guid.gsub('http://stylermag.link/?p=', '').to_i
        article = where(wp_post_id: wp_post_id).first_or_create
        article.update(
          author_id:    Author.where(name: item.dc_creator.force_encoding('utf-8')).first_or_create.id,
          title:       item.title.force_encoding('utf-8'),
          description: item.description.force_encoding('utf-8'),
          content:     item.content_encoded.force_encoding('utf-8'),
          thumbnail:   item.content_encoded.force_encoding('utf-8').match(/src(.+)"/)[0].split('"')[1],
          created_at:  item.pubDate.try(:to_datetime),
          updated_at:  item.pubDate.try(:to_datetime),
          wp_url:      item.link.force_encoding('utf-8'))
      end
    rescue => e
      AdministratorMailer.rescue_notification(e, __method__).deliver_now
      logger.error e
    end
  end

  # def url
  #   "https://styler.link/articles/#{id}"
  # end

  def url
    wp_url
  end
end
