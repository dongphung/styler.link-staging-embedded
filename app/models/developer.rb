class Developer < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :recoverable, :registerable
  devise :database_authenticatable, :rememberable, :trackable, :validatable

  class << self
    # <V3に向けて>
    # Post
    # category_idが入力済みのオブジェクトはカテゴリー付けの処理が終了している
    
    # Reply
    # copiedがtrueになり、item_idが入力済みのオブジェクトは既に下記scriptが実行され、ReplyやReplyImageがItemとItemImageとしてコピーされたことを示している
    # ItemとReplyの切り分けが完了すると、commentが入力済みになり、Itemのcompleteがtrueになる

    # Reply→Item, ReplyImage→ItemImageへとコピーする
    # 何回でも実行可能
    def reply_item_copy
      s3_yml = YAML.load_file("#{Rails.root}/config/s3.yml")[Rails.env]
      Aws.config[:region] = s3_yml['region']
      Aws.config[:access_key_id] = s3_yml['access_key_id']
      Aws.config[:secret_access_key] = s3_yml['secret_access_key']
      s3 = Aws::S3::Client.new
      bucket = Aws::S3::Resource.new(
                             region: s3_yml['region'],
                             access_key_id: s3_yml['access_key_id'],
                             secret_access_key: s3_yml['secret_access_key'],
                             ).bucket(s3_yml['bucket'])
      non_image_ids = []
      Reply.where(copied: false).includes(:image).each do |reply|
        extension = reply.image.photo_content_type.gsub('image/','')
        if bucket.object("photos/#{reply.image.id}/medium_#{reply.image.random_string}.#{extension}").exists? && bucket.object("photos/#{reply.image.id}/normal_#{reply.image.random_string}.#{extension}").exists? && bucket.object("photos/#{reply.image.id}/original_#{reply.image.random_string}.#{extension}").exists? && bucket.object("photos/#{reply.image.id}/rectangle_#{reply.image.random_string}.#{extension}").exists?
          item = Item.create(
            shop_id: reply.shop_id,
            brand: reply.item_brand,
            name: reply.item_name,
            price: reply.item_price,
            text: reply.text,
            created_at: reply.created_at,
            updated_at: reply.updated_at)
          reply.update_columns(
            item_id: item.id,
            copied: true)
          item_image = ItemImage.create(
            generic_id: item.id,
            photo_file_name: reply.image.photo_file_name,
            photo_content_type: reply.image.photo_content_type,
            photo_file_size: reply.image.photo_file_size,
            photo_updated_at: reply.image.photo_updated_at,
            created_at: reply.image.created_at,
            updated_at: reply.image.updated_at)
          ['medium', 'normal', 'original', 'rectangle'].each do |name|
            s3.copy_object(
              copy_source: "#{s3_yml['bucket']}/photos/#{reply.image.id}/#{name}_#{reply.image.random_string}.#{extension}",
              bucket: s3_yml['bucket'],
              key: "photos/#{item_image.id}/#{name}_#{reply.image.random_string}.#{extension}")
          end
        else
          non_image_ids <<  reply.image.id
        end
      end
      Message.where(type: 'ReplyMessage').each do |message|
        reply = Reply.find(message.objects[:reply_id])
        if reply.item
          hash = { id: reply.item.id, brand: reply.item.brand, name: reply.item.name, price: reply.item.price, image_url: reply.item.image.try(:url)}
          message.update_columns(type: 'ItemMessage', objects: hash)
        else
          message.destroy
        end
      end
      AdministratorMailer.non_image_notification(non_image_ids).deliver_now
    end

    def after_migration_script
      prefectures = Settings.prefecture.to_h.invert
      User.find_each do |user|
        location_id = prefectures["#{user.location}"].to_s.present? ? prefectures["#{user.location}"].to_s.to_i : 0
        user.update_columns(location: location_id)
        user.update_columns(fb_details: {work: user.work.presence, education: user.education.presence, location: nil, gender: nil})
        user.update_columns(work: nil, education: nil)
      end
    end

  end
end
