class CommentNotification < Notification
  # association
  belongs_to :comment, class_name: :Comment, foreign_key: :generic_id
  belongs_to :notification_object, class_name: :Comment, foreign_key: :generic_id
  has_one    :notification_author, source: :user, through: :comment
end
