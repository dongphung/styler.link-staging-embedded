class Watch < ActiveRecord::Base
  # association
  belongs_to :post
  belongs_to :user

  # callback
  after_create :create_notification

  # counter_cache
  counter_culture :user

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :api_pagination, ->(last_object) { 
    where('watches.created_at <= ?', last_object.created_at).
    where('watches.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(post: [user: :image])
    .api_pagination(last_object).recent.limit(Settings.api_limit[:watches]) }

  # validation
  validates :user_id, :post_id, presence: true
  validates :user_id, uniqueness: { scope: [:post_id] }

  class << self
    def mine(user_id)
      find_by(user_id: user_id)
    end
  end

  def notice_users
    [post.user].concat(post.watch_users).uniq
  end

  private
  def create_notification
    notice_users.each do |user|
      WatchNotification.where(user_id: user.id, generic_id: id).first_or_create
    end
  end

  def create_push_notification
    message = "#{user.name}がPostをWatchしました"
    notice_users.each do |user|
      PushNotification.create_push('watch', user.id, message, post_id)
    end
  end

  # delayed job
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
end
