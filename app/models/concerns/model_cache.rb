module ModelCache
  extend ActiveSupport::Concern

  included do
    after_save :clear_cache
  end

  def clear_cache
    Rails.cache.delete("#{self.class.name}#cached_by_id(#{self.id})")
  rescue => e
    if Rails.env.production?
      AdministratorMailer.rescue_notification(e, __method__).deliver_now
    end
    logger.error e
  end

  module ClassMethods
    def cached_find_by_id(id, cache_duration)
      Rails.cache.fetch("#{self.name}#cached_by_id(#{id})", expires_in: cache_duration) do
        self.find_by(id: id)
      end
    rescue => e
      if Rails.env.production?
        AdministratorMailer.rescue_notification(e, __method__).deliver_now
      end
      logger.error e
      self.find_by(id: id)
    end
  end
end
