class EventNotification < Notification
  # association
  belongs_to :event, class_name: :Event, foreign_key: :generic_id
  belongs_to :notification_object, class_name: :Event, foreign_key: :generic_id
  has_one    :notification_author, source: :shop, through: :event
end
