class Author < ActiveRecord::Base
  # association
  has_many :articles, dependent: :destroy
  has_one  :image, class_name: :AuthorImage, foreign_key: :generic_id, dependent: :destroy

  # scope
  scope :alphabetical, -> { order(name: :asc, id: :desc) }

  # validation
  validates :name, presence: true

  class << self
    def create_authors_from_stylermag
      Stylermaguser.all.each do |stylermaguser|
        self.where(id: stylermaguser.id, name: stylermaguser.display_name).first_or_create
      end
    rescue => e
      AdministratorMailer.rescue_notification(e, __method__).deliver_now
      logger.error e
    end
  end
end
