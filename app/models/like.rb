class Like < ActiveRecord::Base
  # association
  belongs_to :item
  has_many   :notifications, class_name: :LikeNotification, foreign_key: :generic_id, dependent: :destroy
  belongs_to :user

  # counter_cache
  counter_culture :item
  counter_culture :user

  # callback
  after_create :create_notification
  after_create :create_push_notification

  # validation
  validates :user_id, :item_id, presence: true

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :api_pagination, ->(last_object) { 
    where('likes.created_at <= ?', last_object.created_at).
    where('likes.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(item: :replies).includes(item: :images)
    .includes(item: [shop: :image]).api_pagination(last_object)
    .recent.limit(Settings.api_limit[:likes]) }

  class << self
    def mine(user_id)
      find_by(user_id: user_id)
    end
  end

  def notice_users
    item.shop.users
  end

  private
  def create_notification
    notice_users.each do |user|
      LikeNotification.where(user_id: user.id, generic_id: id).first_or_create
    end
  end

  def create_push_notification
    message = "#{user.name}がReplyにLikeしました"
    notice_users.each do |user|
      PushNotification.create_push('like', user.id, message, item.id)
    end
  end

  # delayed job
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
end
