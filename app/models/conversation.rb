class Conversation < ActiveRecord::Base
  # association
  has_many :conversation_users, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :notifications, class_name: :ConversationNotification, foreign_key: :generic_id, dependent: :destroy
  has_many :users, through: :conversation_users

  # scope
  scope :newest, -> { order(newest_time: :desc, id: :desc) }
  scope :api_pagination, ->(last_object) { 
    where('conversations.newest_time < ?', last_object.newest_time).
    where('conversations.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { api_pagination(last_object).newest.
    limit(Settings.api_limit[:conversations]) }
  scope :web_index, ->(page) { newest.page(page).per(Settings.api_limit[:conversations]) }

  # validation
  validates :newest_time, presence: true

  class << self
    def search(user_id, destination_user_id)
      ( self.joins(:users).merge(User.where(id: user_id)) & self.joins(:users).merge(User.where(id: destination_user_id)) ).first
    end
  end

  def destination(current_user_id)
    users.id_except(current_user_id).first
  end

  def newest_text
    messages.recent.find_by(type: :UserMessage).try(:text) || 'NO MESSAGE'
  end

  def has_unread_message?(user_id)
    notifications.by_user_id(user_id).new_notifications.present?
  end
end
