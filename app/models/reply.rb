class Reply < ActiveRecord::Base
  # association
  has_many   :favorites, dependent: :destroy
  has_many   :feedbacks, dependent: :destroy
  belongs_to :item
  has_many   :notifications, class_name: :ReplyNotification, foreign_key: :generic_id, dependent: :destroy
  belongs_to :post
  belongs_to :shop
  belongs_to :user

  has_one    :image, class_name: :ReplyImage, foreign_key: :generic_id, dependent: :destroy

  # callback
  after_create :create_notification
  after_create :create_push_notification
  after_create :send_mail
  after_create :update_newest_time

  # counter_cache
  counter_culture :user

  # module
  include ModelCache

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :api_pagination, ->(last_object) { 
    where('replies.created_at <= ?', last_object.created_at).
    where('replies.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(:image).api_pagination(last_object).recent.limit(Settings.api_limit[:replies]) }
  scope :web_index, ->(page) { includes(:image).recent.page(page).per(Settings.api_limit[:replies]) }

  # validation
  validates :user_id, :post_id, :comment, :item_id, presence: true

  class << self
    def mine(user_id)
      find_by(user_id: user_id)
    end

    def my_shops(shop_id)
      find_by(shop_id: shop_id)
    end
  end

  def item_price=(item_price)
    write_attribute(:item_price, item_price.gsub(',', ''))
  end

  def json_conversation_id(current_user)
    current_user && Conversation.search(current_user.id, user_id).try(:id)
  end

  def json_feedback_flag(current_user)
    current_user.try(:id) == post.user_id && feedbacks.mine(current_user.id).nil?
  end

  def notice_users
    [post.user].concat(post.watch_users).concat(user.follower_users).uniq
  end

  private
  def update_newest_time
    post.update(newest_time: created_at)
    shop.update(newest_time: created_at)
  end

  def create_notification
    notice_users.each do |user|
      ReplyNotification.where(user_id: user.id, generic_id: id).first_or_create
    end
  end

  def create_push_notification
    message = "#{user.name}がReplyを投稿しました"
    notice_users.each do |user|
      PushNotification.create_push('reply', user.id, message, post_id)
    end
  end

  def send_mail
    notice_users.each do |user|
      PostMailer.reply_mail(self, user).deliver_now
    end
  end

  # delayed job
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
  handle_asynchronously :send_mail
end
