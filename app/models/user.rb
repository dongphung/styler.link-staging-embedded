class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable, :confirmable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  # association
  has_many :app_sessions, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :conversation_users, dependent: :destroy
  has_many :conversations, through: :conversation_users, dependent: :destroy
  has_many :events, dependent: :destroy
  has_many :follow_users, source: :staff, through: :follows
  has_many :follows, class_name: :Follow, foreign_key: :user_id, dependent: :destroy
  has_many :follower_users, source: :user, through: :followers
  has_many :followers, class_name: :Follow, foreign_key: :staff_id, dependent: :destroy
  has_one  :image, class_name: :UserImage, foreign_key: :generic_id, dependent: :destroy
  has_many :interest_events, source: :event, through: :interests
  has_many :interests, dependent: :destroy
  has_many :like_items, source: :item, through: :likes
  has_many :likes, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :replies, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :shop_users, dependent: :destroy
  has_many :shops, through: :shop_users
  has_many :watch_posts, source: :post, through: :watches
  has_many :watches, dependent: :destroy

  # callback
  after_create :send_welcome_mail

  # enum
  enum status: { customer: 0, staff: 1 }
  enum gender: { male: 0, female: 1 }

  # serialize
  serialize :fb_details

  # scope
  scope :id_except, ->(id) { where.not(id: id) }
  scope :has_name, -> { where.not(name: nil) }
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :like_by_name, -> (word) { where(['name LIKE ?', "%#{word.gsub(/[\\%_]/){|m| "\\#{m}"}}%"]) }
  scope :api_pagination, ->(last_object) {
    where('users.created_at <= ?', last_object.created_at).
    where('users.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(:image).api_pagination(last_object).recent.limit(Settings.api_limit[:users]) }
  scope :web_index, ->(page) { includes(:image).recent.page(page).per(Settings.api_limit[:users]) }

  class << self
    def facebook_oauth(result)
      if User.exists?(uid: result['id'])
        User.find_by(uid: result['id'])
      elsif result['email'] && User.exists?(email: result['email'])
        user = User.find_by(email: result['email'])
        user.update(uid: result['id'])
        user
      elsif result['email'].present?
        user = User.create(
          name: result['name'].presence,
          uid: result['id'],
          provider: 'facebook',
          email: result['email'],
          password: Devise.friendly_token[0,20],
          birthday: fb_birthday(result),
          gender: fb_gender(result),
          fb_details: fb_details(result))
        Image.create_by_facebook_user(user)
        user
      else
        false
      end
    end

    def new_with_session(params, session)
      super.tap do |user|
        if data = session['devise.facebook_data'] && session['devise.facebook_data']['extra']['raw_info']
          user.email = data['email'] if user.email.blank?
        end
      end
    end

    def create_unique_string
      SecureRandom.uuid
    end

    def fb_birthday(result)
      Date.strptime(result['birthday'],'%m/%d/%Y') if result['birthday'].present?
    end

    def fb_gender(result)
      if result['gender'].present?
        if result['gender'] == 'male'
          0
        elsif result['gender'] == 'female'
          1
        end
      end
    end

    def fb_details(result)
      {work: result['work'], education: result['education'], location: result['location'], gender: result['gender']}
    end
  end

  def shop
    shops.first if self.staff?
  end

  def colleagues
    shop.users.where.not(id: id) if self.staff?
  end

  def api_detail
    d_birthday = "#{(Date.today.try(:strftime, '%Y-%m-%d').to_i - birthday.try(:strftime, '%Y-%m-%d').to_i)}歳・" if birthday
    d_gender = male? ? '男性・' : '女性・' if gender
    d_location = Settings.prefecture[location]
    "#{d_birthday}#{d_gender}#{d_location}"
  end

  def json_follow_id(current_user)
    current_user && followers.mine(current_user.id).try(:id)
  end

  private
  def send_welcome_mail
    PostMailer.welcome_mail(self).deliver_now
  end
end
