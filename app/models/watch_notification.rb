class WatchNotification < Notification
  # association
  belongs_to :watch, class_name: :Watch, foreign_key: :generic_id
  has_one    :notification_object, source: :post, through: :watch
  has_one    :notification_author, source: :user, through: :watch
end
