class Kpi < ActiveRecord::Base
  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }

  class << self
    def get_kpi
      self.create(
        date:             date,
        total_users:      total_users,
        total_customers:  total_customers,
        total_staffs:     total_staffs,
        total_shops:      total_shops,
        total_posts:      total_posts,
        total_watches:    total_watches,
        total_replies:    total_replies,
        total_events:     total_events,
        total_matches:    total_matches)
    end

    def date
      Date.today - 1
    end

    def total_users
      User.count
    end

    def total_customers
      User.count - ShopUser.count
    end

    def total_staffs
      ShopUser.count
    end

    def total_shops
      Shop.count
    end

    def total_posts
      Post.count
    end

    def total_watches
      Watch.count
    end

    def total_replies
      Reply.count
    end

    def total_events
      Event.count
    end
    
    def total_matches
      Post.includes(:watches, :replies).all.map{|p| (p.watches.count + 1) * p.replies.count}.inject{|sum, n| sum + n}
    end
  end
end
