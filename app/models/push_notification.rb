class PushNotification < ActiveRecord::Base
  # association
  has_many :app_sessions, class_name: :AppSession, foreign_key: :user_id, primary_key: :user_id

  # enum
  enum push_type: {
    all_users:       1,
    all_customers:   2,
    all_staffs:      3,
    comment:         4,
    event:           5,
    feedback:        6,
    follow:          7,
    interest:        8,
    like:            9,
    message:         10,
    reply:           11,
    watch:           12,
    event_start:     13
  }

  # invariable
  SNS_YML = YAML.load_file("#{Rails.root}/config/aws_sns.yml")[Rails.env]
  WINDOW_NUM = Settings.window_num

  # scope
  scope :registered_asc, -> { order(registered_at: :asc) }
  scope :by_user_id, ->(user_id) { where(user_id: user_id) }
  scope :by_push_type, ->(push_type) { where(push_type: push_type) }
  scope :passed_reservation, -> { where('reservation_time <= ?', Time.now.to_i) }

  # validation
  validates :push_type, :reservation_time, presence: true

  class << self
    def create_push(push_type, user_id, message, page_id, reservation_time=nil)
      if user_id.blank? || User.find(user_id).push_notification
        self.create(
          identification_id: SecureRandom.base64(32),
          push_type: push_type,
          user_id: user_id.presence,
          message: message.presence,
          page_id: page_id.presence || 0,
          reservation_time: reservation_time.presence || Time.now.to_i,
          registered_at: Time.now.to_i)
      end
    end

    def send_to_sns_per_minute
      send_to_sns('all_users')
      send_to_sns('all_customers')
      send_to_sns('all_staffs')
      send_to_sns('comment')
      send_to_sns('event')
      send_to_sns('feedback')
      send_to_sns('interest')
      send_to_sns('like')
      send_to_sns('message')
      send_to_sns('reply')
      send_to_sns('watch')
      send_to_sns('event_start')
    end

    def send_to_sns_per_fifteen_minutes
      self.send_to_sns('follow')
    end

    def send_to_sns(push_type)
      case push_types[push_type]
      when 1, 2, 3 # 管理画面からのpush通知
        notifications = by_push_type(push_types[push_type]).passed_reservation
        notifications.each do |notification|
          topic_sender(SNS_YML['sns']["apns_topic_#{push_type}_arn"], set_message(notification.message, WINDOW_NUM[push_type], notification.page_id))
        end
        notifications.delete_all
      when 4, 5, 6, 8, 9, 10, 11, 12, 13 # リアルタイム処理
        notifications = by_push_type(push_types[push_type]).includes(:app_sessions).registered_asc
        notifications.each do |notification|
          notification.app_sessions.each do |session|
            sns_sender(session.device_token, set_message(notification.message, WINDOW_NUM[push_type], notification.page_id))
          end
        end
        notifications.delete_all
      when 7 # まとめて処理（今後は5, 8, 9, 12もまとめる）
        follow.includes(:app_sessions).registered_asc.each do |notification|
          notifications = follow.by_user_id(notification.user_id)
          if notifications.size > 1
            message = "#{notifications.size}人にフォローされました。フォロワーリストから確認しましょう！"
          else
            message = "#{notification.message}さんにフォローされました。フォロワーリストから確認しましょう！"
          end
          notification.app_sessions.each do |session|
            sns_sender(session.device_token, set_message(message, WINDOW_NUM[push_type], notification.page_id))
          end
          notifications.delete_all
        end
      end
    rescue => e
      AdministratorMailer.rescue_notification(e, __method__).deliver_now
      logger.error e
    end

    def topic_sender(target_arn, message)
      publish = sns.publish(
        topic_arn:   target_arn,
        message:     message,
        message_structure: 'json')
      if publish
        Rails.logger.info 'publish success'
      else
        Rails.logger.error 'publish failed'
      end
    end

    def sns_sender(device_token, message)
      if device_token
        endpoint_arn = endpoint(device_token).endpoint_arn
        change_endpoint_attribute(endpoint_arn)
        publish = sns.publish(
          target_arn: endpoint_arn,
          message:    message,
          message_structure: 'json')
      end
      if publish
        Rails.logger.info 'publish success'
      else
        Rails.logger.error 'publish failed'
      end
    end

    # push時のメッセージを生成
    def set_message(message, window, page_id)
      data = {aps: {
        alert: message,
        window: window,
        page: page_id,
        badge: 1,
        sound: 'default'}
      }.to_json
      if Rails.env.production?
        {default: message, APNS: data}.to_json
      else
        {default: message, APNS_SANDBOX: data}.to_json
      end
    end

     # topicにdevice_tokenを登録
    def add_endpoint_to_topic(device_token=nil, user=nil)
      if device_token
        sns.subscribe(
          topic_arn:  SNS_YML['sns']['apns_topic_all_users_arn'],
          protocol:   SNS_YML['sns']['protocol'],
          endpoint:   endpoint(device_token).endpoint_arn )
        if user.try(:customer?)
          sns.subscribe(
            topic_arn:  SNS_YML['sns']['apns_topic_all_customers_arn'],
            protocol:   SNS_YML['sns']['protocol'],
            endpoint:   endpoint(device_token).endpoint_arn )
        elsif user.try(:staff?)
          sns.subscribe(
            topic_arn:  SNS_YML['sns']['apns_topic_all_staffs_arn'],
            protocol:   SNS_YML['sns']['protocol'],
            endpoint:   endpoint(device_token).endpoint_arn )
        end
      end
    rescue => e
      AdministratorMailer.rescue_notification(e, __method__).deliver_now
      logger.error e
    end

    # AWS SNSの設定を保持
    def sns
      Aws::SNS::Client.new(
        access_key_id:      SNS_YML['account']['access_key_id'],
        secret_access_key:  SNS_YML['account']['secret_access_key'],
        region:             SNS_YML['account']['region'] )
    end

    # AWS SNSにtokenを保存
    def endpoint(device_token)
      sns.create_platform_endpoint({
        platform_application_arn: SNS_YML['sns']['apple_platform_application_arn'],
        token: device_token })
    end

    def change_endpoint_attribute(endpoint_arn)
      sns.set_endpoint_attributes({
        endpoint_arn: endpoint_arn,
        attributes: { Enabled: 'true' } })
    end
  end
end

