class Tag < ActiveRecord::Base
  # association
  has_many :articles, through: :article_tags
  has_many :article_tags, dependent: :destroy

  # scope
  scope :alphabetical, -> { order(name: :asc, id: :desc) }
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :api_pagination, ->(last_object) { 
    where('tags.name >= ?', last_object.name).
    where('tags.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { api_pagination(last_object).alphabetical.limit(Settings.api_limit[:tags]) }

  class << self
    def weekly_tags
      ids = []
      Article.includes(:tags).week_latest.each do |article|
        ids.concat(article.tags.pluck(:id))
      end
      self.where(id: ids.uniq)
    end
  end
end
