class Feedback < ActiveRecord::Base
  # association
  has_many   :notifications, class_name: :FeedbackNotification, foreign_key: :generic_id, dependent: :destroy
  belongs_to :reply
  belongs_to :user

  # callback
  after_create :create_notification
  after_create :create_push_notification
  after_create :send_mail

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }

  # validation
  validates :user_id, :reply_id, :rate, presence: true

  class << self
    def mine(user_id)
      find_by(user_id: user_id)
    end
  end

  private
  def create_notification
    FeedbackNotification.where(user_id: reply.user_id, generic_id: id).first_or_create
  end

  def create_push_notification
    message = "#{user.name}があなたのReplyに「#{Settings.feedback[rate]}」と言っています"
    PushNotification.create_push('feedback', user_id, message, reply_id)
  end

  def send_mail
    PostMailer.feedback_mail(self, reply.user).deliver_now
  end

  # delayed job
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
  handle_asynchronously :send_mail
end
