class FeedbackNotification < Notification
  # association
  belongs_to :feedback, class_name: :Feedback, foreign_key: :generic_id
  has_one :notification_object, source: :reply, through: :feedback
  has_one :notification_author, source: :user, through: :feedback
end
