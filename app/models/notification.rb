class Notification < ActiveRecord::Base
  # association
  belongs_to :user

  # enum
  enum status: { unconfirmed: 0, browsed: 1, confirmed: 2 }

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :by_user_id, ->(user_id) { where(user_id: user_id) }
  scope :timeline, -> { where.not(type: :ConversationNotification) }
  scope :new_notifications, -> { where.not(status: 2) }
  scope :api_pagination, ->(last_object) { 
    where('notifications.created_at <= ?', last_object.created_at).
    where('notifications.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(:notification_object).includes(notification_author: :image).api_pagination(last_object).timeline.recent.limit(Settings.api_limit[:notifications]) }
  scope :web_index, ->(page) { includes(:notification_object).includes(notification_author: :image).timeline.recent.page(page).per(Settings.api_limit[:notifications]) }

  # validation
  validates :user_id, :generic_id, :type, presence: true
  validates :user_id, uniqueness: { scope: [:generic_id, :type] }

  class << self
    def browse!(user_id)
      by_user_id(user_id).new_notifications.each do |notification|
        notification.confirmed! if notification.browsed?
        notification.browsed! if notification.unconfirmed?
      end
    end
  end
end
