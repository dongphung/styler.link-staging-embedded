class ConversationNotification < Notification
  # association
  belongs_to :conversation, class_name: :Conversation, foreign_key: :generic_id
end
