class Follow < ActiveRecord::Base
  # association
  has_many   :notifications, class_name: :FollowNotification, foreign_key: :generic_id, dependent: :destroy
  belongs_to :staff, class_name: :User, foreign_key: :staff_id
  belongs_to :user, class_name: :User, foreign_key: :user_id

  # callback
  after_create  :create_notification
  after_create  :create_push_notification
  after_destroy :destroy_push_notification

  # counter_cache
  counter_culture :user, column_name: 'follows_count'
  counter_culture :staff, column_name: 'followers_count'

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :by_staff_id, ->(staff_id) { where(staff_id: staff_id) }
  scope :by_user_id, ->(user_id) { where(user_id: user_id) }
  scope :api_pagination, ->(last_object) { 
    where('follows.created_at <= ?', last_object.created_at).
    where('follows.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(user: :image).api_pagination(last_object)
    .recent.limit(Settings.api_limit[:follows]) }

  # validation
  validates :user_id, :staff_id, presence: true
  validates :user_id, uniqueness: { scope: [:staff_id] }

  class << self
    def mine(user_id)
      find_by(user_id: user_id)
    end
  end

  private
  def create_notification
    FollowNotification.where(user_id: staff_id, generic_id: id).first_or_create
  end

  def create_push_notification
    PushNotification.create_push('follow', staff.id, user.name, nil)
  end

  def destroy_push_notification
    PushNotification.where(push_type: 6, user_id: staff_id, message: user.name).delete_all
  end

  # delayed job
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
end
