class Category < ActiveRecord::Base
  # association
  has_many :articles
  has_many :items
  has_many :posts
end
