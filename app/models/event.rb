class Event < ActiveRecord::Base
  # association
  has_one    :image, class_name: :EventImage, foreign_key: :generic_id, dependent: :destroy
  has_many   :interests, dependent: :destroy
  has_many   :interest_users, source: :user, through: :interests
  has_many   :notifications, class_name: :EventNotification, foreign_key: :generic_id, dependent: :destroy
  belongs_to :shop
  belongs_to :user

  # callback
  after_create :create_notification
  after_create :create_push_notification

  # module
  include ModelCache

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :api_pagination, ->(last_object) {
    where('events.created_at <= ?', last_object.created_at).
    where('events.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(:image, [shop: :image]).
    api_pagination(last_object).recent.limit(Settings.api_limit[:events]) }
  scope :web_index, ->(page) { includes(:image, [shop: :image]).recent.page(page).per(Settings.api_limit[:events]) }
  scope :start_today, -> { where(start_date: Date.today) }
  scope :future, -> { where('events.start_date > ?', Date.today) }
  scope :ongoing, -> { no_finish_date.or(Event.within_range) }
  scope :no_finish_date, -> { where('events.start_date <= ?', Date.today).where(finish_date: nil) }
  scope :within_range, -> { where('events.start_date <= ?', Date.today).where.not('events.finish_date < ?', Date.today) }

  # validation
  validates :user_id, :shop_id, :title, :text, :start_date, presence: true

  class << self
    def create_with_photo(event_params, image_params)
      if image_params[:photo]
        event = self.where(event_params).first_or_create
        if event.persisted?
          Image.create_by_object(event, image_params[:photo])
          event
        end
      end
    end

    def start_notifications
      start_today.each do |event|
        message = "#{event.shop.name}の#{event.title}の開催日です"
        event.interest_users.each do |user|
          PostMailer.start_events_mail(event, user).deliver_now
          PushNotification.create_push('event_start', user.id, message, event.id)
        end
      end
    end
  end

  def json_interest_id(current_user)
    current_user && interests.mine(current_user.id).try(:id)
  end

  def notice_users
    User.has_name.id_except(shop.users.pluck(:id))
  end

  private
  def create_notification
    notice_users.find_each do |user|
      EventNotification.where(user_id: user.id, generic_id: id).first_or_create
    end
  end

  def create_push_notification
    message = "#{shop.name}のイベントが公開されました"
    notice_users.each do |user|
      PushNotification.create_push('event', user.id, message, id)
    end
  end

  # delayed job
  handle_asynchronously :create_notification
  handle_asynchronously :create_push_notification
end
