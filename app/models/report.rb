class Report < ActiveRecord::Base
  # association
  belongs_to :user

  # scope
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :uncompleted, -> { where(complete: false) }

  # validation
  validates :object, :generic_id, :user_id, presence: true
end
