class Item < ActiveRecord::Base
  # association
  has_many   :images, class_name: :ItemImage, foreign_key: :generic_id, dependent: :destroy
  has_many   :likes, dependent: :destroy
  has_many   :replies, dependent: :destroy
  belongs_to :shop

  # scope
  scope :completed, -> { where(complete: true) }
  scope :recent, -> { order(created_at: :desc, id: :desc) }
  scope :popular, -> { order(likes_count: :desc, id: :desc) }
  scope :by_category, ->(category_id) { where(category_id: category_id) }
  scope :api_pagination, ->(last_object) { 
    where('items.likes_count <= ?', last_object.likes_count).
    where('items.id < ?', last_object.id) if last_object }
  scope :api_index, ->(last_object) { includes(:images).includes(shop: :image)
    .api_pagination(last_object).popular.limit(Settings.api_limit[:items]) }
  scope :web_index, ->(page) { includes(:images).includes(shop: :image)
    .popular.page(page).per(Settings.api_limit[:items]) }

  # validation
  validates :shop_id, presence: true

  class << self
    def create_with_photo_single(item_params, images_params)
      if images_params[:photo_1]
        item = self.where(item_params).first_or_create
        if item.persisted?
          Image.create_by_object(item, images_params[:photo_1])
          item
        end
      end
    end

    # 複数画像アップロード用のメソッド（v3.1.0に回す）
    # def create_with_photo(item_params, images_params)
    #   if images_params[:photo_1]
    #     item = self.where(item_params).first_or_create
    #     if item.persisted?
    #       Image.create_by_object(item, images_params[:photo_1])
    #       item
    #     end
    #   end
    # end
  end

  def image
    images.first
  end

  def json_like_id(current_user)
    current_user && likes.mine(current_user.id).try(:id)
  end
end
