class ShopUser < ActiveRecord::Base
  # association
  belongs_to :user
  belongs_to :shop

  # callback
  after_create  :update_status_staff
  after_create  :create_sfdc_object
  after_destroy :update_status_customer

  # invariable
  SFDC_YML = YAML.load_file("#{Rails.root}/config/sfdc.yml")[Rails.env]

  # validation
  validates :user_id, :shop_id, presence: true

  class << self
    # SFDCのスタッフ情報を更新
    def update_sfdc_object
      self.all.each do |shop_user|
        shop_user.update_contact_information
      end
    rescue => e
      PostMailer.rescue_notification(e, __method__).deliver
      logger.error e
    end
  end

  def update_contact_information
    account_id = sfdc_client.find('Account', shop_id, 'shop_styid__c').Id
    sfdc_client.upsert(
      'Contact',
      'user_styid__c',
      user_styid__c: user_id,
      LastName: user.name.presence || '-',
      Email: user.email,
      AccountId: account_id
      )
  end

  private
  def update_status_staff
    user.staff!
  end

  def update_status_customer
    user.customer!
  end

  # SFDCにスタッフ情報を登録
  def create_sfdc_object
    account_id = sfdc_client.find('Account', shop_id, 'shop_styid__c').Id
    sfdc_client.create(
      'Contact',
      user_styid__c: user_id,
      LastName: user.name.presence || '-',
      Email: user.email,
      AccountId: account_id
      )
  rescue => e
    AdministratorMailer.rescue_notification(e, __method__).deliver_now
    logger.error e
  end

  def sfdc_client
    Restforce.new(
      username:        SFDC_YML['username'],
      password:        SFDC_YML['password'],
      security_token:  SFDC_YML['security_token'],
      client_id:       SFDC_YML['client_id'],
      client_secret:   SFDC_YML['client_secret'],
      host:            SFDC_YML['host'] )
  end
end
