class Stylermagpost < ActiveRecord::Base
  # association
  has_many   :stylermagpostmetas, class_name: :Stylermagpostmeta, foreign_key: :post_id

  # connection
  establish_connection(Settings.styler_media_editor[Rails.env])

  # scope
  scope :day_latest, -> { where(post_modified: (7.days.ago.beginning_of_day)..(Date.today.end_of_day)) }
  scope :publish, -> { where(post_status: 'publish') }

  def article_description
    stylermagpostmetas.find_by(meta_key: '_aioseop_description').try(:meta_value)
  end

  def article_keywords
    stylermagpostmetas.find_by(meta_key: '_aioseop_keywords').try(:meta_value)
  end

  def article_thumbnail
    thumbnail = Stylermagpostmeta.find_by(meta_key: 'amazonS3_info', post_id: stylermagpostmetas.find_by(meta_key: '_thumbnail_id').try(:meta_value)).try(:meta_value)
    thumbnail_url = "https://styler-media.s3-ap-northeast-1.amazonaws.com/#{thumbnail.split('"')[7]}" if thumbnail.present?
    thumbnail_url
  end
end
