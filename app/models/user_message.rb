class UserMessage < Message
  # callback
  after_create :create_link_message
  after_create :create_notification
  after_create :create_push_notification
  after_create :send_mail
end
