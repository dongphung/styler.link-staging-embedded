Rails.application.routes.draw do
  root 'posts#index'
  devise_for :users, controllers: {
    confirmations: 'users/confirmations',
    omniauth_callbacks: 'users/omniauth_callbacks',
    passwords: 'users/passwords',
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    unlocks: 'users/unlocks'
  }
  devise_for :developers, controllers: {
    sessions: 'developers/sessions'
  }
  mount RailsAdmin::Engine => '/developer', as: 'rails_admin'

  # Publicページ
  get  '/', :to => redirect('/ios-app')
  get  '/', :to => redirect('/press')

  # 旧STYLER MAGからのリダイレクト処理
  get  '/wp_articles/:id'              => 'articles#wp_show'

  # 旧URLからのリダイレクト処理
  get  '/replys/:id'                   => 'replies#ex_show'

  resources :articles, only: %i(index show)
  resources :comments, only: %i(create destroy)
  resources :conversations, only: %i(index new show)
  resources :events
  resources :favorites, only: %i(create destroy)
  resources :feedbacks, only: %i(new create)
  resources :follows, only: %i(create destroy)
  resources :interests, only: %i(create destroy)
  resources :items
  resources :likes, only: %i(create destroy)
  resources :messages, only: %i(create)
  resources :notifications, only: %i(index)
  resources :posts do
    collection do
      get :latest
      get :unreplied
    end
  end
  resources :replies, except: %i(index destroy)
  get  '/replies/:id/embed'            => 'replies#embed', as: 'replies_embed'
  get  '/replies/:id/embed_link'       => 'replies#generate_embed_link', as: 'embed_link'
  resources :reports, only: %i(new create)
  resources :shops, except: %i(new create destroy) do
    member do
      get :items
      get :events
      get :staffs
      get :information
    end
  end
  resources :shop_registrations, only: %i(new create)
  resources :shop_users, only: %i(new create)
  resources :tops, only: %i(), path: '/' do
    collection do
      get :privacy_policy
      get :use_policy
      get :company
      get :help
      get :pending
    end
  end
  resources :users, only: %i(show edit update) do
    member do
      get :watches
      get :likes
      get :interests
      get :follows
      get :followers
    end
    collection do
      get :setting
      get :change_email
      get :mail_setting
    end
  end
  resources :watches, only: %i(create destroy)

  namespace :admin do
    resources :articles, only: %i(index edit update) do
      get :reload, on: :collection
    end
    resources :authors, only: %i(index edit update)
    resources :categories, only: %i(index new create)
    resources :contacts, only: %i(index new create)
    resources :kpis, only: %i(index)
    resources :html_mails, only: %i(index) do
      collection do
        get :welcome_mail
        get :comment_mail
        get :feedback_mail
        get :message_mail
        get :reply_mail
        get :start_events_mail
        get :approval_mail
        get :staff_invite_mail
      end
    end
    resources :push_notifications, only: %i(new create)
    resources :reports, only: %i(index update)
    resources :shops, only: %i(index new create update)
    resources :shop_registrations, only: %i(index show update)
    resources :tops, only: %i(index)
    resources :users, only: %i(index new create)
  end

  namespace :api, { format: 'json' } do
    # KPI
    get    '/kpi'                               => 'tops#kpi'

    # 削除予定
    get    '/articles_top'                      => 'articles#top'
    get    '/features'                          => 'articles#feature'
    get    '/future_events'                     => 'events#future'
    get    '/ongoing_events'                    => 'events#ongoing'
    get    '/latest_posts'                      => 'posts#latest'
    get    '/unreplied_posts'                   => 'posts#unreplied'

    # ios
    resources :articles, only: %i(index show) do
      get :top, on: :collection
    end
    resources :categories, only: %i(index) do
      get :articles, on: :member
    end
    resources :comments, only: %i(create destroy)
    resources :conversations, only: %i(index) do
      get :messages, on: :member
    end
    resources :events, except: %i(new edit)
    resources :feedbacks, only: %i(create)
    resources :follows, only: %i(create destroy)
    resources :interests, only: %i(create destroy)
    resources :items, only: %i(index show update)
    resources :likes, only: %i(create destroy)
    resources :messages, only: %i(index create)
    resources :notifications, only: %i(index)
    resources :posts, except: %i(new edit) do
      member do
        get :replies
        get :comments
      end
      get :unreplied, on: :collection
    end
    resources :replies, only: %i(create show update)
    resources :reports, only: %i(create)
    resources :shops, only: %i(index show update) do
      member do
        get :items
        get :events
        get :staffs
      end
    end
    resources :tags, only: %i(index) do
      get :articles, on: :member
    end
    resources :tops, only: %i(), path: '/' do
      collection do
        get  :check_version
        post :login
        post :sign_up
        post :facebook
        get  :token_reference
        get  :check_notifications
        get  :coupons
      end
    end
    resources :users, only: %i(show) do
      member do
        get :likes
        get :interests
        get :posts
        get :watches
        get :follows
        get :replies
        get :followers
      end
      collection do
        patch :profile
        patch :setting
        patch :email
        patch :password
      end
    end
    resources :watches, only: %i(create destroy)
  end

  namespace :dashboard, { format: 'json' } do
    resources :tops, only: %i(), path: '/' do
      collection do
        post :login
        get  :shop_stats
        get  :overall_stats
        get  :shop_staffs
        get  :shop_replies
      end
    end
  end
end
