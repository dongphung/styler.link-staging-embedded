module CollectionCacheKey
  module CacheKey
    def details_for(collection, timestamp_column)
      sub_query_name = 'sub_query'
      column = "#{connection.quote_table_name(sub_query_name)}.#{connection.quote_column_name(timestamp_column)}"
      query = collection.dup
      attrs = connection.select_all("SELECT COUNT(*) AS size, MAX(#{column}) AS timestamp FROM (#{query.to_sql}) AS #{sub_query_name}").first

      [query_key(collection), attrs['size'], parsed_timestamp(attrs['timestamp'])]
    end
  end
end
