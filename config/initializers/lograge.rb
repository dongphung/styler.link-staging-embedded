Rails.application.configure do
  config.lograge.logger = ActiveSupport::Logger.new("#{Rails.root}/log/lograge_#{Rails.env}.log")
  config.lograge.enabled = true
  config.lograge.keep_original_rails_log = true
  config.lograge.formatter = Lograge::Formatters::Json.new

  config.lograge.custom_options = lambda do |event| {
    host:            event.payload[:host],
    remote_ip:       event.payload[:remote_ip],
    user_agent:      event.payload[:user_agent],
    os:              event.payload[:os],
    os_version:      event.payload[:os_version],
    browser:         event.payload[:browser],
    browser_version: event.payload[:browser_version],
    user_id:         event.payload[:user_id],
    timestamp:       event.time
  }
  end
end
