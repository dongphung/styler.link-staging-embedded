# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'https://styler.link/'
 
SitemapGenerator::Sitemap.create do
  add posts_path
  add users_path
  add new_user_session_path
  add shop_registrations_path
  add help_tops_path
  add company_tops_path
end

