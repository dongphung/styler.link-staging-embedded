require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Styler
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    config.time_zone = 'Asia/Tokyo'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :ja

    # For redis
    redis_yml = YAML.load_file("#{Rails.root}/config/redis.yml")[Rails.env]
    config.cache_store = :readthis_store, {
      expires_in: 5.minutes.to_i,
      namespace: redis_yml['cache_store_prefix'],
      redis: { host: redis_yml['host'], port: redis_yml['port'], db: 0 },
      driver: :hiredis
    }
    
    config.action_dispatch.default_headers = {
      'X-Frame-Options' => 'ALLOWALL'
    }

    config.track_ids = YAML.load_file("#{Rails.root}/config/track.yml")[Rails.env]
  end
end
