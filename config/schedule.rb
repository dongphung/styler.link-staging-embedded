set :output, 'log/crontab.log'
set :environment, ENV['RAILS_ENV'] || :production

every 1.minute do
  runner 'PushNotification.send_to_sns_per_minute'
end

every 15.minute do
  runner 'PushNotification.send_to_sns_per_fifteen_minutes'
end

every 1.day, at: '0:00 am' do
  runner 'Kpi.get_kpi'
  runner 'AppSession.destroy_expired_session'
  runner 'Shop.check_inactive_shops'
  runner 'ShopUser.update_sfdc_object'
end

every 1.day, at: '2:00 am' do
  # runner 'Article.create_articles_from_stylermag'
  # runner 'Author.create_authors_from_stylermag'
end

every 1.day, at: '10:00 am' do
  runner 'Event.start_notifications'
end

every :sunday, at: '10:00 am' do
  # runner 'PostMailer.customer_weekly_mail.deliver_now'
  # runner 'PostMailer.staff_weekly_mail.deliver_now'
end

every :tuesday, at: '9:00 am' do
  # runner 'PostMailer.weekly_popular_posts_mail.deliver_now'
end

every :friday, at: '9:00 am' do
  # runner 'PostMailer.weekly_popular_items_mail.deliver_now'
end
